/*
TASK: BANDOMASIS
LANG: C++
*/

#include <cstdlib>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
    ifstream fin("bandomasis.in");
    int K,S,L;
    fin >> K >> S;
    fin.close();
    if (S<K) L=-1;
        else if ( (S-K) % 19 != 0 ) L=-1;
             else L=(S-K)/19;
    if ( L>K ) L=-1;           
    ofstream fout("bandomasis.out");
    fout << L;
    fout.close();
    return 0;
}
