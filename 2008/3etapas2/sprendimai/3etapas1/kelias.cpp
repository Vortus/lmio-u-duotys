/* Lino sprendimas uždaviniui PAPRASČIAUSIAS KELIAS. (Komentarai utf-8)
 * Sprendimas panaudojant steką.
 */

#include <iostream>
#include <cstdio>
#include <string>
#include <stack>

using namespace std;

int main()
{
    freopen("kelias.in", "r", stdin);
    freopen("kelias.out", "w", stdout);

    string line;
    getline(cin, line);
    line += "/";
        
    stack<string> dirs;
    string dir = "";

    for (int i = 1; i < line.length(); i++) {
        if (line[i] == '/') {
            // jei jau sulipdėme katalogo vardą
            if (dir == ".." && !dirs.empty()) 
                dirs.pop();
            if (dir != "."  && dir != "..")
                dirs.push(dir);

            dir = "";
        }
        else
            // lipdome
            dir += line[i];
    }

    string path = "";
    if (dirs.empty())
        path = "/";
    else
        while (!dirs.empty()) {
            path = "/" + dirs.top() + path;
            dirs.pop();
        }
    
    cout << path << endl;

    return 0;
}
