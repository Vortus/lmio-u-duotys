program MIESTO_TVARKYMAS;
  
  const MAX_DARBU = 500;
  const MAX_RIKIAVIMU = 1000;
  
  var
      N : 1..MAX_DARBU; {darbu skaicius}
      R : 1..MAX_RIKIAVIMU; {pasiulytu rikiavimu skaicius}

      {Priklausomybes tarp darbu NEANKSCIAU matrica}
      neanksciau : array [1..MAX_DARBU, 1..MAX_DARBU] of boolean;

      ri : 0..MAX_RIKIAVIMU;
      k, i, j : 0..MAX_DARBU;
      d, d1, d2 : 1..MAX_DARBU;

      {Isiminsime viena i� daru rikiavimu; ji panaudosime grupiu tvarkai nustatyti}
      rikiuote : array [1..MAX_DARBU] of 1..MAX_DARBU;

      {Kiekvienam darbui priskirtos grupes numeriai; jei 0, grupe nepriskirta}
      darboGrupesNr : array [1..MAX_DARBU] of 0..MAX_DARBU;

      {"Lygiu" darbu grupes. Grupiu numeriai atitinka darbu atlikimo tvarka}
      yraGrupiu : 0..MAX_DARBU;
      grupes : array [1..MAX_DARBU] of record 
                                         dydis : 0..MAX_DARBU;
                                         darbai : array [1..MAX_DARBU] of 1..MAX_DARBU;
                                       end;
      g : 0..MAX_DARBU;

begin
  readln(N, R);

  {I� prad�iu nera apribojimu darbu tvarkai. 
   Patogumo delei laikome, kad darbas negali eiti anksciau u� save.}
  for i := 1 to N do begin
    for j := 1 to N do begin
      if (i = j) then neanksciau[i,j] := true
                 else neanksciau[i,j] := false;
    end;
  end;

  {Nuskaitome pasiulytus rikiavimus. Kiekvienam rikiavimui uztenka
   fiksuoti tvarka tik tarp dvieju gretimu darbu. Kitos prklausomybes
   bus apskaiciuotos. 
   Pirmaji rikiavima isimename}
  for ri := 1 to R do begin
    read(d1);
    if (ri = 1) then rikiuote[1] := d1;
    for k := 2 to N do begin
      read(d2);
      if (ri = 1) then rikiuote[k] := d2;
      neanksciau[d2, d1] := true;
      d1 := d2;
    end;
    readln;
  end;

  {Randam priklausomybes "neanksciau" tranzityvu uzdarini}
  for k:= 1 to N do begin
    {Randam visas priklausomybes, kurios apskaiciuojamos 
     tarpiniu darbu 1,2,...,k pagalba}
    for i:= 1 to N do begin
      for j:= 1 to N do begin
        if (neanksciau[i,k] and neanksciau[k,j]) then
          neanksciau[i,j] := true; 
      end;
    end;
  end;

  {Darbams grupes nepriskirtos}
  for k := 1 to N do begin
    darboGrupesNr[k] := 0;
  end;

  {Visos grupes tu�cios}
  for g := 1 to N do begin
    grupes[g].dydis := 0;
  end;

  {Suskirstome darbus i "lygumo" grupes. 
   Pirmiausia formuojame grupes tiems darbams, kurie turi eiti pirmesni}
  yraGrupiu := 0;
  for k := 1 to N do begin
    d := rikiuote[k];
    if (darboGrupesNr[d] = 0) then begin
      {Radome darba, kuriam dar nepriskirta grupe. Pradesime nauja grupe
       ir i ja sudesime visus darbus lygius d}
      yraGrupiu := yraGrupiu + 1;
      for i := 1 to N do begin
        {Tikriname, ar darbas d lygus darbui i}
        if (neanksciau[d, i] and neanksciau[i, d]) then begin
           {Pastebesime, kad neanksciau[d,d]=true, todel darbas d pateks i grupe}
           darboGrupesNr[i] := yraGrupiu;
           with grupes[yraGrupiu] do begin
             dydis := dydis + 1;
             darbai[dydis] := i;
           end;
        end;
      end;
    end;
  end;
  
  {I�vedame rezultatus}
  writeln(yraGrupiu);
  for g := 1 to yraGrupiu do begin
    with grupes[g] do begin
      write(dydis:1);
      for i := 1 to dydis do
        write(' ', darbai[i]:1);
    end;
    writeln;
  end;                        

end.

