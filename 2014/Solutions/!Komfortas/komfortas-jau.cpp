/**
 * Sprendimo aprašymas:
 *
 * Pirmiausia verta pastebėti, kad keleivių grupės, kurias skiria tuščių kėdžių
 * eilė, turi būti nagrinėjamos atskirai. Jei du keleiviai sėdi kartu, tai
 * kažkas negalės atlenkti savo kėdės, nebent abi gretimos eilės tuščios. Grupė,
 * susidedanti iš keleivių, sėdinčių po vieną eilėje, gali susėsti tik dviem
 * skirtingais būdais, tad lengva patikrinti, kuriuo atveju bus mažiau
 * persėdimų. Tiksliau, jei grupė susideda iš K eilių, ir reikia X persėdimų,
 * kad visi keleiviai galėtų atsilenkti savo kėdę, tai kitu atveju prireiks
 * lygiai (K - X) persėdimų.
 *
 * Autorius: Linas Petrauskas
 */

#include <algorithm>
#include <cstdio>
#include <iostream>

using namespace std;

int main() {
    // Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis iš
    // failo.
     freopen ("komfortas.in", "r", stdin);
     freopen ("komfortas-jau.out", "w", stdout);

    int N;      // Eilių skaičius.
    int S = 0;  // Uždavinio atsakymas.

    int a1 = 0, b1 = 0;  // Nagrinėjama eilė.
    int a2 = 0, b2 = 0;  // "Praeita" eilė.

    int K = 0;  // Kiek eilių yra nagrinėjamoje grupėje.
    int X = 0;  // Kiek keleivių persodinome grupėje.

    cin >> N;

    for (int i = 0; i <= N; i++) {
        if (i < N) {
            cin >> a1 >> b1;
        } else {
            // Pridedame dirbtinę paskutinę eilę, kad būtų paprasčiau skaičiuoti.
            a1 = 0;
            b1 = 0;
        }

        if (a1 + b1 > 0) {
            K++;
        }

        if (a1 + b1 + a2 + b2 > 2) {
            // Daugiau nei du keleiviai negalės patogiai susėsti gretimose eilėse.
            S = -1; break;
        }
        if (a1 + b1 == 0) {
            // Baigėme nagrinėti grupę -- padidiname atsakymą ir pradedam iš naujo.
            S += min(X, K - X);
            K = 0;
            X = 0;
        } else if (a1 + b1 == 1) {
            // Nagrinėjame, ar kas sėdi priešais šį vieną keleivį.
            if (a2 + b2 == 1) {
                // Mandras būdas padidinti pasislinkusių skaičių.
                X += a1*a2 + b1*b2;
                a1 = b2;
                b1 = a2;
            }
        }

        a2 = a1;
        b2 = b1;
    }

    cout << S << endl;
    return 0;
}
