#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

    struct Point{
        int x, y;
        Point(int xx, int yy) : x(xx), y(yy){ } // konstruktorius
    };

   vector<Point> exploredMap; // iseksplorintas mapas

   inline bool notExplored(int x, int y){ // ar yra mape
        for(int i = 0; i < exploredMap.size(); i++)
            if(exploredMap[i].x == x && exploredMap[i].y == y)
                return false;
        return true;
   }

   inline void moveTurtle(char C, int &x, int &y){
        if(C == 'S') y--;
        else if(C == 'R') x++;
        else if(C == 'V') x--;
        else if(C == 'P') y++;
        if(notExplored(x, y)) // jei nera mape
            exploredMap.push_back(Point(x, y));
    }

 int main(){

    int posX = 0, posY = 0, N;
    ifstream fi("vezlys.in");
    fi >> N; fi.ignore(80, '\n'); // nereikia to net
    for(int i = 0; i < N; i++){
        char c; fi.get(c);
        if(c == 'C'){
            fi.get(c); int j = c - '0'; fi.get(c);
            for(int k = 0; k < j; k++)
                moveTurtle(c, posX, posY);
            i += 2;
        } else moveTurtle(c, posX, posY);
    }
    fi.close();

    ofstream fo("vezlys.out");
    fo << exploredMap.size();
    fo.close();

 return 0; }
