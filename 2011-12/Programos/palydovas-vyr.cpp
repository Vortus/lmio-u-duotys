#include<cstdio>
#include<map>
#include<vector>
#include<utility>

using namespace std;

#define MY_NULL -1
#define MAX_N 2001
#define HASH_SIZE 5000000
#define ATOMS MAX_N*MAX_N

typedef pair<int, int> pii;

struct Atom {
public:
	int x, y, count, nxt;
};

pii ptA[MAX_N], ptB[MAX_N];
Atom universe[ATOMS];
int bucket[HASH_SIZE];
int N, M, usize = 0;

int key(int x, int y) {
	unsigned int hash;
	int h1 = 0x12345678;
	int h2 = 0x87654321;
	hash = (x*h1)^(y*h2);
	hash += (hash >> 2) * h1;
	hash ^= (hash >> 10) * h2;
	return hash % HASH_SIZE;
}

int newAtom(int x, int y) {
	int k = key(x, y);
	int pnt = bucket[k];
	while(pnt != MY_NULL) {
		if(universe[pnt].x == x && universe[pnt].y == y) return ++(universe[pnt].count);
		pnt = universe[pnt].nxt;
	}
	universe[usize].x = x;
	universe[usize].y = y;
	universe[usize].count = 1;
	universe[usize].nxt = bucket[k];
	bucket[k] = usize;
	++usize;
	return 1;
}

int main() {
	freopen("palydovas-vyr.in", "r", stdin);
	freopen("palydovas-vyr.out", "w", stdout);

	for(int k = 0; k < HASH_SIZE; ++k)
		bucket[k] = MY_NULL;
	scanf("%d\n", &N);
	for(int i = 0; i < N; ++i)
		scanf("%d%d", &ptA[i].first, &ptA[i].second);
	scanf("%d\n", &M);
	for(int i = 0; i < M; ++i)
		scanf("%d%d", &ptB[i].first, &ptB[i].second);

	int most = 0;
	for(int i = 0; i < N; ++i)
		for(int j = 0; j < M; ++j)
			most = max(most, newAtom(ptA[i].first - ptB[j].first, ptA[i].second - ptB[j].second));

	printf("%d\n", most);
	return 0;
}

