#include <iostream>
#include <fstream>
#define TASK "kas-teisus-jau"
using namespace std;

int main()
{
	ifstream fin(TASK".in");
	ofstream fout(TASK".out");
	char c;
	int n = 0;
	int cnt[] = {0, 0};
	for (; fin >> c; ++n)
	{
		if (c == '\n') break;
		cnt[n % 2] += c - '0';
	}
	if (n % 2) swap(cnt[0], cnt[1]);
	if (cnt[0] < cnt[1])
	{
		fout << "BEBRAS" << endl << cnt[1] - cnt[0] << endl;
	}
	else if (cnt[1] < cnt[0])
	{
		fout << "ZUIKIS" << endl << cnt[0] - cnt[1] << endl;
	}
	else
	{
		fout << "LYGIOSIOS" << endl << 0 << endl;
	}
	return 0;
}
