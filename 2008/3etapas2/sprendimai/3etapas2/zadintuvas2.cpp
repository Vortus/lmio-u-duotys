/*
TASK:zadintuvas
LANG:C++
*/


#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include "zadintuvas_bib.h"
using namespace std;

#define MAXN 210
#define B 1000

int n, m;
bool zadint[MAXN][MAXN];
int pl = 0;
int empty[MAXN*MAXN];
int ne = 0;
bool flipped = 0;

int abs(int x) { return x < 0 ? -x : x; }

void ask(int, int);

bool baldas(int e, int s) {
	return yraBaldas(e+1, flipped ? (m-s) : (s+1));
}

void flip() {
	for (int i = 0; i < n; ++i)
		reverse(zadint[i], zadint[i]+m);
	for (int i = 0; i < ne; ++i)
		empty[i] = empty[i]/B*B + m-1-empty[i]%B;
	flipped = !flipped;
}

void input() {
	n = gautiN();
	m = gautiM();
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			zadint[i][j] = baldas(i, j);
			pl += zadint[i][j];
			if (!zadint[i][j]) {
				empty[ne++] = i*B + j;
			}
		}
	}
}

void get_one() {
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j)
			if (zadint[i][j])
				zadintuvas(i+1, flipped ? m-j : j+1);
}

void get_two() {
	int x1 = -1, x2, y1, y2;
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j)
			if (zadint[i][j]) {
				if (x1 < 0) {
					x1 = i;
					y1 = j;
				}
				else {
					x2 = i;
					y2 = j;
				}
			}
	for (int i = 0; i < ne; ++i) {
		int ex = empty[i]/B, ey = empty[i]%B;
		if (abs(x1 - ex) + abs(y1 - ey) != abs(x2 - ex) + abs(y2 - ey))
			ask(ex, ey);
	}
	printf("Neišsprendžiamas kambarys\n");
	exit(1);
}

void ask(int x, int y) {
	int r = atstumas(x+1, flipped ? m-y : y+1);
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j)
			if (zadint[i][j] && abs(i-x) + abs(j-y) != r) {
				zadint[i][j] = 0;
				--pl;
			}
	if (pl == 1)
		get_one();
	if (pl == 2)
		get_two();
}

int main() {
	input();
	/*while (ne) {
		int i = rand()%ne;
		int x = empty[i];
		empty[i] = empty[--ne];
		ask(x/B, x%B);
	}*/
	int topf = -1, topl;
	for (int i = 0; i < m; ++i)
		if (!baldas(0, i)) {
			if (topf < 0)
				topf = i;
			topl = i;
		}
	if (topf < 0) {
		printf("Viršutinėje eilutėje nėra laisvos vietos\n");
		exit(1);
	}
	int botf = -1, botl;
	for (int i = 0; i < m; ++i)
		if (!baldas(n-1, i)) {
			if (botf < 0)
				botf = i;
			botl = i;
		}
	if (botf < 0) {
		printf("Apatinėje eilutėje nėra laisvos vietos\n");
		exit(1);
	}
	if (topf <= botf && topl >= botf) {
		ask(0, topf);
		ask(n-1, botf);
		ask(0, topl);
	}
	if (botf <= topf && botl >= topf) {
		ask(0, topf);
		ask(n-1, botf);
		ask(n-1, botl);
	}

	int leftf = -1, leftl;
	for (int i = 0; i < n; ++i)
		if (!baldas(i, 0)) {
			if (leftf < 0)
				leftf = i;
			leftl = i;
		}
	if (leftf < 0) {
		printf("Kairiame stulpelyje nėra laisvos vietos\n");
		exit(1);
	}
	int rightf = -1, rightl;
	for (int i = 0; i < n; ++i)
		if (!baldas(i, m-1)) {
			if (rightf < 0)
				rightf = i;
			rightl = i;
		}
	if (leftf < 0) {
		printf("Dešiniame stulpelyje nėra laisvos vietos\n");
		exit(1);
	}
	if (leftf <= rightf && leftl >= rightf) {
		ask(leftf, 0);
		ask(rightf, m-1);
		ask(leftl, 0);
	}
	if (rightf <= leftf && rightl >= leftf) {
		ask(leftf, 0);
		ask(rightf, m-1);
		ask(rightl, m-1);
	}

	if (topf > botl) {
		ask(0, topf);
		ask(n-1, botl);
	}
	else {
		ask(0, topl);
		ask(n-1, botf);
	}
	if (leftf > rightl) {
		ask(leftf, 0);
		ask(rightl, m-1);
	}
	else {
		ask(leftl, 0);
		ask(rightf, m-1);
	}

	int x1 = -1, x2 = -1, y1, y2;
	for (int i = 0; i < n && x2 < 0; ++i)
		for (int j = 0; j < m && x2 < 0; ++j)
			if (zadint[i][j]) {
				if (x1 < 0) {
					x1 = i;
					y1 = j;
				}
				else {
					x2 = i;
					y2 = j;
				}
			}
	if (x1 + y1 == x2 + y2) {
		flip();
		y1 = m-1-y1;
		y2 = m-1-y2;
	}
	int diff = x1 - y1;
	int minx = x1;
	int maxx = x2;
	for (int i = x2 + 1; i < n; ++i)
		if (zadint[i][i-diff])
			maxx = i;
	while (1) {
		int nne = 0;
		for (int i = 0; i < ne; ++i) {
			if (empty[i]/B <= minx && empty[i]%B <= minx-diff || empty[i]/B >= maxx && empty[i]%B >= maxx-diff)
				ask(empty[i]/B, empty[i]%B);
			if (empty[i]/B <= minx && empty[i]%B >= maxx-diff || empty[i]/B >= maxx && empty[i]%B <= minx-diff)
				continue;
			empty[nne++] = empty[i];
		}
		ne = nne;
		if (ne == 0) {
			printf("Neišsprendžiamas kambarys\n");
			exit(1);
		}
		int b = -1, bw = maxx - minx + 1;
		for (int i = 0 ; i < ne; ++i) {
			int w;
			if (empty[i]/B - empty[i]%B < diff)
				w = min(maxx, diff + empty[i]%B) - max(minx, empty[i]/B) + 1;
			else
				w = min(maxx, empty[i]/B) - max(minx, diff + empty[i]%B) + 1;
			if (w < bw) {
				bw = w;
				b = i;
			}
		}
		if (b < 0) {
			printf("Neišsprendžiamas kambarys\n");
			exit(1);
		}
		ask(empty[b]/B, empty[b]%B);
		while (!zadint[minx][minx-diff])
			++minx;
		while (!zadint[maxx][maxx-diff])
			--maxx;
	}
}
