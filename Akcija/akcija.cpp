#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

    vector<double> differentItems;
    int differentMax, N;

    void readFile(){
        ifstream fi("akcija.in");
        fi >> differentMax >> N;

        for(int i = 0; i < N; i++){
            double f; fi >> f;
                differentItems.push_back(f);
        }
        sort(differentItems.begin(), differentItems.end());
        fi.close();
    }

 int main(){

    readFile();
    double result = -1;

    for(int i = 0; i < differentItems.size(); i++){
        bool b = true;
        double sum = 0, mins = differentItems[i];
        for(int j = i; j < differentMax + i; j++){
            if(j >= N){ b = false; break; }
            if(differentItems[j] < mins) mins = differentItems[j];
            sum += differentItems[j];
            cout << differentItems[j] << " ";
        }
        double tmp = mins / sum;
        cout << endl;
        if(b && tmp > result) result = tmp;
    }

    ofstream fo("akcija.out");
    fo << fixed << result * 100.0 << endl;
    fo.close();

 return 0; }
