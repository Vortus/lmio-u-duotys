#include <cstdio>
#include <queue>
#include <algorithm>
#include "grader.h"
using namespace std;

const int Maxn = 1000005;
const bool prpath = false;

int n, m, k;
int flag;
vector <int> neigh[Maxn];
bool wolf[Maxn], final[Maxn];
int dist[Maxn], par[Maxn];
int V[Maxn], vlen;
int U[Maxn], ulen;
bool inplace[Maxn];

void BFS(int v, bool final[], int seq[], int &slen, bool filpar = false)
{
	fill(dist + 1, dist + n + 1, Maxn); dist[v] = 0;
	seq[slen++] = v;
	for (int i = 0; i < slen; i++) {
		v = seq[i];
		for (int i = 0; i < neigh[v].size(); i++) {
			int u = neigh[v][i];
			if (final[u] && dist[v] + 1 < dist[u]) {
				dist[u] = dist[v] + 1;
				seq[slen++] = u;
			}
		}
	}
}

void BFS2(int v)
{
	fill(dist + 1, dist + n + 1, Maxn); dist[v] = 0;
	queue <int> Q; Q.push(v);
	while (!Q.empty()) {
		v = Q.front(); Q.pop();
		for (int i = 0; i < neigh[v].size(); i++) {
			int u = neigh[v][i];
			if (dist[v] + 1 < dist[u]) {
				dist[u] = dist[v] + 1;
				par[u] = v;
				Q.push(u);
			}
		}
	}
}

void Medziok(int N, int M, int K, int a[], int b[], int v[], int u[], bool nurodyti_kelia)
{
	n = N; m = M; k = K;
	for (int i = 0; i < m; i++) {
		neigh[a[i]].push_back(b[i]);
		neigh[b[i]].push_back(a[i]);
	}
	for (int i = 0; i < k; i++) {
		wolf[v[i]] = true;
		final[u[i]] = true;
	}
	fill(dist + 1, dist + n + 1, Maxn);
	queue <int> Q;
	for (int i = 0; i < k; i++) {
		dist[v[i]] = 0; par[v[i]] = v[i]; Q.push(v[i]);
	}
	int near, furt;
	while (!Q.empty()) {
		int cur = Q.front(); Q.pop();
		if (final[cur]) { near = cur; break; }
		for (int i = 0; i < neigh[cur].size(); i++) {
			int u = neigh[cur][i];
			if (dist[cur] + 1 < dist[u]) {
				dist[u] = dist[cur] + 1;
				par[u] = par[cur];
				Q.push(u); 
			}
		}
	}
	furt = par[near];
	int res = k - 1 + dist[near]; Kelias(res);
	if (nurodyti_kelia) {
		BFS(near, final, U, ulen); BFS(furt, wolf, V, vlen);
		BFS2(near);
		int i = 0; // V rodyklė
		int j = 0; // U rodyklė
		int wh = V[i]; // artimiausia viršūnė iki 'near'
		while (res--) {
			do i = (i - 1 + vlen) % vlen;
			while (inplace[i]);
			if (wh != near) {
				wh = par[wh];
				Eik(V[i], wh);
				V[i] = wh;
				if (final[V[i]]) { final[V[i]] = false; inplace[i] = true; }
			} else {
				while (!final[U[j]]) j++;
				Eik(V[i], U[j]);
				final[U[j]] = false; inplace[i] = true;
			}
		}
	}
}