#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int digitCount(int n){
        int result = 0;
        while(n > 0){
            n /= 10;
            result++;
        }
        return result;
    }

 int main(){

    ifstream fi("skait.in");
    int N; fi >> N;
    fi.close();

    int c = 0;
    for(int i = 1; i <= N; i++)
        c += digitCount(i);

    ofstream fo("skait.out");
    fo << c;
    fo.close();

 return 0; }
