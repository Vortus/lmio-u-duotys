/**
 * Sprendimo idėja:
 *
 * Greitai gendančius mandarinus reikia bandyti valgyti kiek įmanoma anksčiau.
 * Tad išsirikiuojame mandarinus pagal jų sugedimo dieną (nuo mažiausios iki
 * didžiausios) ir valgome iš eilės.
 *
 * Valgymą galima simuliuoti paprasčiausiai pereinant per dienas pradedant nuo
 * pirmosios, kadangi paskutinė diena niekada neviršys d_i (pagal sąlygą maksimali
 * d_i reikšmės bus ganėtinai maža).
 *
 * Uždavinio autorius: Linas Petrauskas
 * Pavyzdinio sprendimo autorius: Artūras Lapinskas
 */

#include      <iostream>
#include      <algorithm>
using namespace std;

// Šis masyvas nurodo, kurią dieną sugenda i'tasis mandarinas.
int sugenda[100000];

int main(int argc, char **argv) {
	// Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis
	// iš failo.
	// freopen("mandarinai-jau.in", "r", stdin);
	// freopen("mandarinai-jau.out", "w", stdout);

	// Nusiskaitome duomenis.
	int mandarinu, suvalgoma_per_diena;
	cin >> mandarinu >> suvalgoma_per_diena;

	for (int i = 0; i < mandarinu; i += 1) {
		cin >> sugenda[i];
	}

	// Mandarinus, kurie sugenda greičiau, bandysime valgyti kiek įmanoma anksčiau.
	// Tad, prieš pradedant valgyti, išrikiuojame mandarinus pagal jų sugedimo
	// laiką.
	sort(sugenda, sugenda + mandarinu);

	// Galutinis atsakymas bus saugomas šiame kintamajame.
	int suvalgyta = 0;

	// Nurodo kurį mandariną bandysime valgyti pirmiausiai.
	int valgomas_mandarinas = 0;

	// Simuliuojame dienas.
	for (int diena = 0; valgomas_mandarinas < mandarinu; diena += 1) {
		// Pirmiausiai išmetame mandarinus, kurie supuvo.
		while (valgomas_mandarinas < mandarinu && diena >= sugenda[valgomas_mandarinas]) {
			valgomas_mandarinas += 1;
		}

		// Valgome kiek įmanoma daugiau.
		int sia_diena_suvalgyta = 0;
		while (
				valgomas_mandarinas < mandarinu
				&& diena < sugenda[valgomas_mandarinas]
				&& sia_diena_suvalgyta < suvalgoma_per_diena) {
			sia_diena_suvalgyta += 1;
			valgomas_mandarinas += 1;
		}

		suvalgyta += sia_diena_suvalgyta;
	}

	cout << suvalgyta << endl;

	return 0;
}
