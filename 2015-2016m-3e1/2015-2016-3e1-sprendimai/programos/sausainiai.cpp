/**
 * Sprendimo idėja:
 *
 * Surikiuojame sausainius a+b sumos mažėjimo tvarka ir tuomet juos dalijame
 * broliams paeiliui: sausainis, kurio a+b vertė didžiausia, atiteks Antanui,
 * antras po jo atiteks Broniui, ir t. t.
 *
 * Uždavinio autorius: Vytautas Gruslys
 * Pavyzdinio sprendimo autorius: Marijonas Petrauskas
 */

#include <algorithm>
#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

struct Sausainis {
    int a, b;

    bool operator<(const Sausainis &x) const {
        return (a + b) > (x.a + x.b);
    }
};

int main() {
    freopen("sausainiai-vyr.in", "r", stdin);
    freopen("sausainiai-vyr.out", "w", stdout);

    int N;
    cin >> N;
    vector<Sausainis> sausainiai(N);
    for (int i = 0; i < N; i++) {
        cin >> sausainiai[i].a >> sausainiai[i].b;
    }

    // Rikiuojame sausainius a+b mažėjimo tvarka.
    sort(sausainiai.begin(), sausainiai.end());

    // Sausainius su lyginiais indeksais (0, 2, 4, ...) priskiriame Antanui,
    // o su nelyginiais – Broniui.
    int A = 0, B = 0;
    for (int i = 0; i < N; i++) {
        if (i % 2 == 0) {
            A += sausainiai[i].a;
        } else {
            B += sausainiai[i].b;
        }
    }
    cout << A - B << endl;

    return 0;
}
