/**
 * Sprendimo idėja:
 *
 * Šiame uždavinyje reikėjo rasti visus iškirptus kvadratėlius ir atvaizduoti juos
 * simetriškai (priklausomai nuo lenkimų kiekio) į kitas snaigės vietas:
 *
 * Atlikus vieną lenkimą, kvadratėlis gali atsivaizduoti į dar vieną vietą;
 * Atlikus du - į ne daugiau kaip tris kitas vietas;
 * Atlikus tris - į ne daugiau kaip septynias kitas vietas.
 *
 * Visas šias naujas vietas galima apibrėžti matematiškai.
 *
 * Autorius: Artūras Lapinskas
 */

#include      <iostream>
using namespace std;

bool lakstas[25][25];
int N;

void atvaizduoti(int y, int x, int lenkimu) {
	// Horizontalus lenkimas.
	lakstas[y][x] = lakstas[N - 1 - y][x] = true;

	// Horizontalus ir vertikalus.
	if (lenkimu >= 2) {
		atvaizduoti(y, N - 1 - x, 1);
	}

	// Horizontalus, vertikalus ir per įstrižainę.
	if (lenkimu >= 3) {
		atvaizduoti(x, y, 2);
	}
}

int main(int argc, char **argv) {
	// Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis
	// iš failo.
	// freopen("snaige-jau.in", "r", stdin);
	// freopen("snaige-jau.out", "w", stdout);

	// Nusiskaitome apšalusią snaigę.
	int lenkimu;
	cin >> N >> lenkimu;

	cin.ignore();
	for (int i = 0; i < N; i += 1) {
		for (int j = 0; j < N; j += 1) {
			char kvadratelis;
			cin >> kvadratelis;
			if (kvadratelis != '.') {
				lakstas[i][j] = true;
			} else {
				lakstas[i][j] = false;
			}
		}
	}

	// Pereiname per visus snaigės kvadratėlius.
	for (int i = 0; i < N; i += 1) {
		for (int j = 0; j < N; j += 1) {
			// Jei randame iškirptą kvadratėlį.
			if (lakstas[i][j]) {
				// Atvaizduojame jį simetriškai (priklausomai nuo lenkimų kiekio).
				atvaizduoti(i, j, lenkimu);
			}
		}
	}

	// Atspausdiname gautą snaigę.
	for (int i = 0; i < N; i += 1) {
		for (int j = 0; j < N; j += 1) {
			cout << (lakstas[i][j] ? "x" : ".");
		}
		cout << endl;
	}

	return 0;
}
