#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

    #define MAXN 100010
    #define MAXH 1010
    #define MAXR 10010
    #define INF 2000000000

    struct Block{
        int a, b;
        bool operator <(const Block &bl) const{
            return b < bl.b;
        }
        void initialize(int aa, int bb){ a = aa; b = bb; }
    };

    Block blocks[MAXN]; // visi blokai
    int N, i, a, b;

    void readFile(){
        ifstream fi("senis.in"); fi >> N;
        for(i = 0; i < N; i++){
            fi >> a >> b;
            blocks[i].initialize(a, b);
        }
        fi.close();
        sort(blocks, blocks + N);
        blocks[N].b = blocks[N - 1].b;
    }


 int main(){

    readFile();
    int sum = 0, maxs = -1;

    for(i = 0; i < N; i++){
        maxs = blocks[i].a;
        while(blocks[i].b == blocks[i + 1].b){
            maxs = max(blocks[i + 1].a, maxs);
            i++;
        }
        sum += maxs;
    }
    cout << sum << endl;

 return 0; }
