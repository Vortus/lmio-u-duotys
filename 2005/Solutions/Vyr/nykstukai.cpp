#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <queue>
using namespace std;

    struct Room{
        int floor, ID;
        Room(int a, int b) : floor(a), ID(b){}
        Room(){}
    };

    struct Vertex{
        Room head;
        int COST = -1;
        vector<Room> link;
    };

    int N, S, P; //aukstu, nikskt, viso k
    Room EXIT_ROOM; // isejimo kambaris
    int t, COST_DOOR, COST_STAIRS; // limitas, duris, laiptai
    Room playerPos[3000]; // nikstuku pozicijos
    Vertex V[200000]; // visos sajungos

    inline int flatID(int floor, int ID){
        return (floor - 1) * 50 + ID;
    }

    void readFile(){
        int tmp;
        ifstream fi("NAMAS.DAT");
        fi >> N >> S >> P;
        for(int i = 1; i <= N; i++)
            fi >> tmp;
        int a, b, c, d; fi >> a >> b; // exitromas
        EXIT_ROOM = Room(a, b);
        fi >> t >> COST_DOOR >> COST_STAIRS; // visi costai
        for(int i = 1; i <= S; i++){ // nikstuku pozicijos
            fi >> a >> b; playerPos[i] = Room(a, b);
        }
        for(int i = 0; i < P; i++){ // visi linkai
            fi >> a >> b >> c >> d;
            Room aa(a, b), bb(c, d);
            ////
            int fl = flatID(a, b);
            V[fl].head = aa;
            V[fl].link.push_back(bb);
            fl = flatID(c, d);
            V[fl].head = bb;
            V[fl].link.push_back(aa);
            ////
        }
        V[flatID(EXIT_ROOM.floor, EXIT_ROOM.ID)].head.floor = EXIT_ROOM.floor;
        V[flatID(EXIT_ROOM.floor, EXIT_ROOM.ID)].head.ID = EXIT_ROOM.ID;
        fi.close();
    }

    void exploreBFS(Vertex v){
        queue<Vertex> q; q.push(v);
        V[flatID(v.head.floor, v.head.ID)].COST = COST_DOOR; // pradinis taskas
        while(!q.empty()){
            v = q.front();
            q.pop();
            for(int i = 0; i < v.link.size(); i++){
                int fl = flatID(v.link[i].floor, v.link[i].ID);
                int c = v.head.floor != v.link[i].floor ? COST_STAIRS : COST_DOOR;
                if(V[fl].COST + c <= t && V[fl].COST == -1){ // jei nepraeksplorinom
                    q.push(V[fl]);
                    V[fl].COST = V[flatID(v.head.floor, v.head.ID)].COST;
                    V[fl].COST += c;
                }
            }
        }
    }

 int main(){

    readFile();
    Vertex v = V[flatID(EXIT_ROOM.floor, EXIT_ROOM.ID)];
    exploreBFS(v);


    ofstream fo("NAMAS.out");
    int result = 0, fl;
    for(int i = 1; i <= S; i++){
        fl = flatID(playerPos[i].floor, playerPos[i].ID);
        if(V[fl].COST != -1 && V[fl].COST <= t) result++;
    }
    fo << result << endl;
        for(int i = 1; i <= S; i++){
        fl = flatID(playerPos[i].floor, playerPos[i].ID);
        if(V[fl].COST != -1 && V[fl].COST <= t) fo << i << endl;
    }
    fo.close();

 return 0; }
