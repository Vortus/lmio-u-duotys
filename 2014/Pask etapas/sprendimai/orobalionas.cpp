#include "orobalionas.h"
#include <queue>
using namespace std;

struct iii {
	int x, y, h;
	iii(int xx, int yy, int hh) {x = xx, y = yy, h = hh;}
};

const int MAX_X = 150;
const int MAX_Y = 150;
const int MAX_H = 150;

int dst[MAX_X][MAX_Y][MAX_H];


int kiekUztruks(int X, int Y, int H, int Xa, int Ya, int Xb, int Yb) {
	queue <iii> qs;
	qs.push(iii(Xa, Ya, 0));
	dst[Xa][Ya][0] = 1;
	while (!qs.empty() && dst[Xb][Yb][0] == 0) {
		iii cur = qs.front(); qs.pop();
		for (int dh = -1; dh <= 1; dh++) {
			int nh = cur.h + dh;
			if (nh >= 0 && nh < H) {
				iii nxt = iii(cur.x + greitisX(nh), cur.y + greitisY(nh), nh);
				if (nxt.x >= 0 && nxt.x < X && nxt.y >= 0 && nxt.y < Y &&
					dst[nxt.x][nxt.y][nxt.h] == 0) {
					qs.push(nxt);
					dst[nxt.x][nxt.y][nxt.h] = dst[cur.x][cur.y][cur.h] + 2;
				}
			}
		}
	}

	int ans = dst[Xb][Yb][0] - 2;
	return (ans > 0) ? ans : -1;
}