#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

    struct Stairs{
        int min, max;
        bool operator<(const Stairs &s) const {
            return min < s.min;
        }
    };

    vector<Stairs> allStairs(1010); // visi laipta
    int N, H; // laiptu kiekis, maksimalus aukstis

    void readFile(string s){
        ifstream fi(s);
        fi >> N >> H;
        for(int i = 0; i < N; i++)
            fi >> allStairs[i].min >> allStairs[i].max;
        fi.close();
        sort(allStairs.begin(), allStairs.begin() + N);
    }

 int main(){

    readFile("laiptai.in");

    bool fullHouse = true; // ar visa nama galima
    int maxs = allStairs[0].max;
    for(int i = 0; i < N - 1; i++){
        if(allStairs[i].max - allStairs[i + 1].min < 0){
            fullHouse = false;
            break;
        } else maxs = allStairs[i + 1].max;
    }

    ofstream fo("laiptai.out");
    if(fullHouse) fo << H;
    else fo << maxs;
    fo.close();

 return 0; }
