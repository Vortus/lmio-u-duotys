#include <cstdio>
#include <queue>


using namespace std;

const int MAX_N = 1000;
const int LAPO_KRASTINE = MAX_N + 2;

const char*  FIN = "atkarpos-jau.in";
const char*  FOUT = "atkarpos-jau.out";
const int NE = 0;
const int TAIP = 1;
 
 
// Langelių baziniai tipai:
const int NETIKRINTAS = 0;
const int SIENA_KAIREJE = 1;
const int SIENA_DESINEJE = 2;
const int SIENA_APACIOJE = 4;
const int SIENA_VIRSUJE = 8;
const int UZ_KRASTO = 16;
const int TIKRINTAS = 32;

// Lapas:
int lapas[ LAPO_KRASTINE ][ LAPO_KRASTINE ];
int realusN;

// Langelio tipas:
struct TLangelis {
    int x;
    int y;
};

// Tikrinimo eilė:
queue<TLangelis> eile;


// Pagalibinės funkcijos:
int min(int a, int b){
   if (a<b) return a;
   else return b;
}

int max(int a, int b){
   if (a>b) return a;
   else return b;
}

bool pozymis(int koks, int x, int y){

   return (koks & lapas[x][y]) != 0;

}


void pridekJeiNetikrintas(
              int salyga, int esamox, int esamoy,
              int langeliox, int langelioy){   
   if ( !pozymis(salyga, esamox, esamoy) && 
        !pozymis(TIKRINTAS, langeliox, langelioy)) {
      eile.push({langeliox, langelioy});
      lapas[langeliox][langelioy] |= TIKRINTAS; 
      //printf("%d %d\n", langeliox, langelioy);
   }   
}
//-----------------------------------

void inicializacija(){
    freopen(FIN, "r",stdin);
    freopen(FOUT, "w",stdout);
    
    scanf("%d", &realusN);
    for(int i=0;i <= realusN+1; i++)            // Pradedame pildyti lapą
       for(int j=0;j <= realusN+1; j++){
           lapas[i][j] = NETIKRINTAS;
           if ( ( i % (realusN+1) == 0) ||
                ( j % (realusN+1) == 0) ) 
                lapas[i][j] |= UZ_KRASTO;
        }
                
    int x,y;                                    // Pradinis langelis
    scanf("%d %d", &x, &y);
   // lapas[x][y] |= TIKRINTAS;
    eile.push({x,y});
    
    int atkarpuSk;                              // Atkarpų skaičius    
    scanf("%d", &atkarpuSk);  
    for(int i=1;i<=atkarpuSk;i++){
        int x1,y1, x2,y2;
        scanf("%d %d %d %d", &x1,&y1,&x2, &y2);
        if(x1 == x2) {                          // Vertikali atkarpa?
           int nuo = min(y1,y2);
           int iki = max(y1,y2)-1;
            
           for(int yy = nuo; yy <= iki; yy++){
              lapas[x1][yy] |= SIENA_KAIREJE; 
              lapas[x1 - 1][yy] |= SIENA_DESINEJE; 
           }
        }   
        else {                                   // Horizontali atkarpa?
           int nuo = min(x1,x2);
           int iki = max(x1,x2)-1;
            
           for(int xx = nuo; xx <= iki; xx++){
              lapas[xx][y1] |= SIENA_APACIOJE; 
              lapas[xx][y1-1] |= SIENA_VIRSUJE; 
           }
        }   
    }
    
}
 
//--------------------------------------------------------------------
int sprendimas() {
    while ( ! eile.empty() ){
        TLangelis dabartinis = eile.front();
        int x = dabartinis.x;
        int y = dabartinis.y;
        
        if ( pozymis(UZ_KRASTO, x, y) )          // ar jau išeiname iš krašto?
           return NE;   
    
        pridekJeiNetikrintas(SIENA_KAIREJE, x,y, x-1,y);
        pridekJeiNetikrintas(SIENA_DESINEJE, x,y, x+1,y);    
        pridekJeiNetikrintas(SIENA_APACIOJE, x,y, x,y-1);
        pridekJeiNetikrintas(SIENA_VIRSUJE, x,y, x,y+1);
    
        lapas[x][y] |= TIKRINTAS;
        eile.pop();
    
    }

    return TAIP;

} 

int main(){
    inicializacija();
    printf("%d\n",sprendimas());
    return 0;
}




