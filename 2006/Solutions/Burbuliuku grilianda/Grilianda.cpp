#include <iostream>
#include <iomanip>
#include <fstream>
#include <queue>
using namespace std;

    struct Node{
        int headID, ID, layer;
        bool explored; vector<int> connections;
        Node(int a, int b, int c) : headID(a), ID(b), layer(c) {}
        Node(){}
    };

    int N, // burbuliu kiekis
        M, // ant kurio kabinam
        L, // kiek lygiu yra
        maxL = -1, // maximalus ilgis
        maxD = -1; // max gylis

    vector<Node> nodes(100010);

    void removeConnection(Node &n, int ID){
        for(int i = 0; i < n.connections.size(); i++)
            if(nodes[n.connections[i]].ID == ID)
                n.connections.erase(n.connections.begin() + i);
    }

    void bfs(int nodeID){
        nodes[nodeID].explored = true;
        nodes[nodeID].layer = 1;
        queue<int> q; q.push(nodeID);
        while(!q.empty()){
            nodeID = q.front(); q.pop();
            if(!nodes[nodes[nodeID].headID].explored){
                nodes[nodes[nodeID].headID].headID = nodeID; // sukeicia headus
                nodes[nodes[nodeID].headID].layer = nodes[nodeID].layer + 1;
                q.push(nodes[nodeID].headID);
                cout << nodes[nodeID].headID << endl;
                nodes[nodes[nodeID].headID].explored = true;
                removeConnection(nodes[nodes[nodeID].headID], nodeID); // nuima headerio
            }
            for(int i = 0; i < nodes[nodeID].connections.size(); i++){
                if(!nodes[nodes[nodeID].connections[i]].explored){
                    nodes[nodes[nodeID].connections[i]].headID = nodeID;
                    nodes[nodes[nodeID].connections[i]].layer = nodes[nodeID].layer + 1;
                    nodes[nodes[nodeID].connections[i]].explored = true;
                    q.push(nodes[nodeID].connections[i]);
                }
            }
        }
    }

    void readFile(){
        ifstream fi("t/girlianda.g10.in");
        fi >> N >> M >> L; int j, a, b;
        for(int i = 0; i < L; i++){
            fi >> j;
            for(int k = 0; k < j; k++){
                fi >> a >> b;
                if(b != 0) nodes[b].connections.push_back(a);
                nodes[a] = Node(b, a, i + 1);
            }
        }
        fi.close();
    }

 int main(){

    readFile();
    bfs(M); nodes[M].headID = 0;

 return 0; }
