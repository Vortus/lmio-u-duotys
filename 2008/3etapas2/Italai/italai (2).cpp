/*
TASK: italai
LANG: C++
*/

#include <cstdio>
#include <algorithm>

#define MAXN 100000

int L[2*MAXN];

int main() {
	freopen("italai.in", "r", stdin);
	freopen("italai.out", "w", stdout);
	int n;
	scanf("%*d%d", &n);
	for (int i = 0; i < n; ++i) {
		scanf("%d%d", &L[2*i], &L[2*i+1]);
		++L[2*i+1];
	}
	std::sort(L, L+2*n);
	int c = 0;
	for (int i = 0; i < n; ++i)
		c += L[2*i+1] - L[2*i];
	printf("%d\n", c);
	return 0;
}
