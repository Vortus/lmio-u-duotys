/*
TASK: KLAUSIMYNAS
LANG: C++
*/

#include <fstream>
#include <vector>
#include <iostream>

// Konstantos
const int MAX_N=5000;
const int MAX_M=500;
const int MAX_K=3000;

char* bylaIn = "klausimynas.in"; 
char* bylaOut = "klausimynas.out"; 
//-------------------------------------------------

int N,M,K;

//-------------------------------------------------
using namespace std;

class Persokimas {
       public:
          Persokimas();
          Persokimas(int klausimas, int nuo, int iki); 
          Persokimas(istream&  finp);
          void print(ostream& fout); 	 
          bool skirtingi(Persokimas* kitas);
          Persokimas*  pridekKlausima(int nr);
	  int klausimas;
          int nuo;
          int iki;
          
};

Persokimas::Persokimas(int klausimas, int nuo, int iki){
   this->klausimas = klausimas;
   this->nuo = nuo;
   this->iki = iki;
}

Persokimas::Persokimas(istream& fin){
   fin >> this->klausimas >> this->nuo >> this->iki;
}

void Persokimas::print(ostream& fout){
   fout << klausimas << " " << nuo << " " << iki << endl;
} 

bool Persokimas::skirtingi(Persokimas* kitas){
	
	return   ((this->klausimas)!=(kitas->klausimas)) ||
	            ((this->nuo)!=(kitas->nuo)) ||
	     	    ((this->iki)!=(kitas->iki));
	
	}
Persokimas*  Persokimas::pridekKlausima(int nr){
       if( nr < this->klausimas) this->klausimas++; 
       if(nr >= this->iki) return NULL;
        else if (nr >= this->nuo ) {
	   int iki1 =  iki+1;	
           this-> iki = nr;		
           return new Persokimas(this->klausimas, nr+2, iki1);
           }
                else {
                          this->nuo++; this->iki++;  return NULL;                       
                       }
        }

//
vector<Persokimas*> klausimai;

void skaitykIrPildyk(){
    ifstream fin(bylaIn);
    fin >> N >> M >> K;
    
    for(int i=0;i<M;i++)
      klausimai.push_back(new Persokimas(fin));
    
    for(int i=0;i<K;i++) {
	int kiek =klausimai.size(); 	    
	int nr;
        fin >> nr;
      for(int j=0; j<kiek ; j++)
        { 
           Persokimas* p = klausimai[j]->pridekKlausima(nr); 
           if( p != NULL) klausimai.push_back(p);
        } 
    }
  fin.close();

}
//--------------------------
void rasyk(){

   ofstream fout(bylaOut);
   int kiek_skirt=0;
   for(int j=0; j< klausimai.size(); j++){
	bool nera_sut = true;
//	klausimai[j]->print(fout); 
	   
	for(int i=j-1; i>=0; i--)
		if(! klausimai[j]->skirtingi(klausimai[i]) ){
			nera_sut=false;
			break;
			}
 	 if(nera_sut) { 
		 kiek_skirt++; 
		 
		 }
   }
   fout << // klausimai.size() << " " << 
                kiek_skirt << endl;         
   while( ! klausimai.empty() ){
       //delete klausimai[  klausimai.size() ]; 
       klausimai.pop_back();   
   }   
      
   fout.close();
}
//------------------------------------

int main(int argc, char* argv[] ){
	skaitykIrPildyk();
	rasyk();
 }
 