#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <queue>
using namespace std;

    struct Vertex{
        int ID;
        vector<int> link;
    };

    vector<Vertex> V(1000); // visi sarysiai
    vector<int> buffer(1000, -1); // ilgiu bufferis
    int playerNR[1000]; // nykstuku numeriai
    int S, D; //nykstuku skaicius, duru skaicius
    int K; // kambariu skaicius
    int EXIT_NR; // isejimo nr
    int t, TIME_COST; // laiko limitas, kainuoja perejimas

    void addLink(int nr, int which){
        V[nr].ID = nr;
        V[nr].link.push_back(which);
    }

    void readFile(){
        ifstream fi("NAMAS.DAT");
        fi >> S >> D >> K >> EXIT_NR;
        fi >> t >> TIME_COST;
        for(int i = 1; i <= S; i++)
            fi >> playerNR[i];

        for(int i = 0; i < D; i++){
            int a, b; fi >> a >> b;
            addLink(a, b);
            addLink(b, a);
        }

        fi.close();
    }

    void fillBFS(Vertex v){
        queue<Vertex> q;
        q.push(v);
        buffer[v.ID] = TIME_COST;
        while(!q.empty()){
            v = q.front();
            q.pop(); // nuima
            for(int i = 0; i < v.link.size(); i++){
                int j = V[v.link[i]].ID;
                if(buffer[j] == -1){ // jei nebuvom
                    q.push(V[j]);
                    buffer[j] = buffer[v.ID] + TIME_COST;
                }
            }
        }
    }

 int main(){

    readFile();
    fillBFS(V[EXIT_NR]);
    ofstream fo("nykstukai.out");
    int c = 0;
    for(int i = 1; i <= S; i++) if(buffer[playerNR[i]] <= t && buffer[playerNR[i]] != -1) c++;
    fo << c << endl;
    for(int i = 1; i <= S; i++) if(buffer[playerNR[i]] <= t && buffer[playerNR[i]] != -1) fo << i << endl; // ispauzdin
    fo.close();

 return 0; }
