/*
TASK: MAZAKAMPIAI
LANG: C++
*/

#include <vector>
#include <ctime>
#define pb push_back
#define rep(i,n) for(i=0;i<n;++i)
using namespace std;

int main(){
	freopen("mazakampiai.in", "r", stdin);
	freopen("mazakampiai.out", "w", stdout);
	vector<int> x, d, g;
	int n, k, i, j, e, d1;
	scanf("%i %i", &n, &k);
	vector<bool> y;
	rep(i, n) y.pb(0);
	rep(i, k){
		scanf("%i", &j);
		d.pb(j);
		y[j]=1;
	}
	for (d1=3; d1<n; ++d1) if (!y[d1]){
		x.clear();
		rep(i, 4) x.pb(0);
		d.pb(d1);
		for(i=4; i<=n; ++i){
			x.pb(-1);
			rep(j, d.size()) if (d[j]<i){
				e=max(x[d[j]], x[i-d[j]+2])+1;
				if (x[i]==-1||(!(x[i]%2))&&(e%2||x[i]>e)
					||x[i]%2&&e>x[i]&&e%2) x[i]=e;
			}
			if (x[i]==-1) x[i]=0;
		}
		d.pop_back();
		if (!(x[n]%2)) g.pb(d1);
	}
	printf ("%i\n", g.size());
	rep(i, g.size()) printf ("%i ", g[i]);
	printf ("\n");
}
