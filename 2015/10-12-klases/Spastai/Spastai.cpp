#include <iostream>
#include <iomanip>
#include <fstream>
#include <queue>
#include <vector>
using namespace std;

    #define INFINITY 200000000
    #define x first //pairui
    #define y second
    #define MAXW 1010 //maks plotis
    #define MAXH 1010//maks aukstis
    #define MAXD 4 //maks direkciju
    typedef pair<int, int> Point;
    const Point DIR[] = { Point(0, -1), Point(1, 0), Point(0, 1), Point(-1, 0) };

    struct Block{
        char tile; Point p;
        int minLength, hpUsed;
    };

    Point startPoint;
    Block map[MAXH][MAXW]; // mapas
    int WIDTH, HEIGHT, HP, MINHP = INFINITY, MINL = INFINITY ;

    void readFile(string s){
        ifstream fi(s); fi >> WIDTH >> HEIGHT >> HP;
        for(int i = 0; i < HEIGHT; i++){
            for(int j = 0; j < WIDTH; j++){
                fi >> map[i][j].tile; map[i][j].hpUsed = INFINITY; // bloko nuskaitymas
                map[i][j].p = Point(j, i);
                if(map[i][j].tile == 'P') startPoint = Point(j, i); // start pointas
            }
        }
        fi.close();
    }

    inline bool onEdge(int x, int y){
        return x == 0 || y == 0 || x == WIDTH - 1 || y == HEIGHT - 1;
    }

    inline bool canAdvance(int x, int y, Point *p){
        return x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT && // jei sienos
        map[y][x].hpUsed > map[p -> y][p -> x].hpUsed && // jei geresnis kelias
        map[y][x].tile != '#';
    }

    void exploreBFS(Point p){
        queue<Point> s; s.push(p);
        map[p.y][p.x].hpUsed = 0;
        int nx, ny;
        while(!s.empty()){
            p = s.front(); s.pop();
            if(onEdge(p.x, p.y)){
                if(map[p.y][p.x].hpUsed < MINHP || (map[p.y][p.x].hpUsed == MINHP && map[p.y][p.x].minLength < MINL)){
                    MINHP = map[p.y][p.x].hpUsed;
                    MINL = map[p.y][p.x].minLength;
                }
            }else{
                for(int i = 0; i < MAXD; i++){
                    ny = p.y + DIR[i].y; nx = p.x + DIR[i].x; // naujos kordinates
                    if(canAdvance(nx, ny, &p)){ // ar galim eit i kordinate
                        map[ny][nx].hpUsed = map[p.y][p.x].hpUsed;
                        if(map[ny][nx].tile == '!') map[ny][nx].hpUsed++; // jei lipam ant spasto
                        map[ny][nx].minLength = map[p.y][p.x].minLength + 1;
                        s.push(Point(nx, ny));
                    }
                }
            }
        }
    }

 int main(){

    readFile("spastai.in");
    exploreBFS(startPoint);
    cout << HP - MINHP << " " << MINL;

 return 0; }
