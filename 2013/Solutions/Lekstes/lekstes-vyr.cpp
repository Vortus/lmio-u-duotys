#include <cstdio>
#include <algorithm>
#include <map>

using namespace std;

#define INFTY 1000000001
#define MAX_N 1000000

typedef pair<int, int> pii;

pii plate[MAX_N];
map<int,int> step;
int N, ans = 0;

int main() {
    freopen("lekstes-vyr.in", "r", stdin);
    freopen("lekstes-vyr.out", "w", stdout);

    scanf("%d", &N);
    for(int i = 0; i < N; ++i) {
        scanf("%d%d", &plate[i].first, &plate[i].second);
        if(plate[i].first < plate[i].second) // Apsimoka pasukti l�k�tes, kad a >= b
            swap(plate[i].first, plate[i].second); 
    }
    sort(plate, plate+N);
    
    // Med�io step bet kuris elementas (b, h) rei�kia, kad yra auk��io h bok�tas,
    // kurio vir�utin�s l�k�t�s antras matmuo yra b
    step.insert(pii(INFTY, 0)); 
    step.insert(pii(-1, INFTY)); // D�l patogumo
    for(int j = N-1; j >= 0; --j) {
        map<int,int> :: iterator it;
        it = step.lower_bound(plate[j].second);
        int h = it->second + 1;
        it = step.upper_bound(plate[j].second);
        --it;
        while(it->second <= h) {
            step.erase(it);
            it = step.upper_bound(plate[j].second);
            --it;
        }
        step.insert(pii(plate[j].second, h));
        if(h > ans) ans = h;
    }
    printf("%d\n", ans);
    
    return 0;
}
