/*
TASK: NYKSTUKAIV
LANG: C
*/

#include <stdio.h>

    typedef struct vertex {
        int   ejimai;
        int   nkaim;
        int   kaim[150];
    } vertex;

    vertex V[2500];

    void add_kaim(int n, int k)
    {
        V[n].kaim[V[n].nkaim++] = k;
    }

    int kod(int aukstas, int numeris)
    {
        return (aukstas-1)*50+(numeris-1);
    }

    int N,  /* aukstai */
        S,  /* smurfai */
        P;  /* perejimai */

    int ISEJIMAS;
    int t,  /* deadline'as */
        t1, /* perejimo per duris laikas */
        t2; /* perejimo laiptais laikas */

    int NYKST[2500];    /* nykstuku buvimo vietos */
    int NLAIMINGUJU = 0;

int read_data(void)
{
    int i, x, y, dummy;
    int a1, b1, a2, b2;

    FILE *f = fopen("nykstukaiv.g11.dat", "r");
    fscanf(f, "%d %d %d", &N, &S, &P);

    for (i = 0; i < N; i++)
        fscanf(f, "%d", &dummy);
    /* isejimas */
    fscanf(f, "%d %d", &y, &x);
    ISEJIMAS = kod(y, x);
    /* laikai */
    fscanf(f, "%d %d %d", &t, &t1, &t2);
    /* nykstukai */
    for (i = 0; i < S; i++) {
        fscanf(f, "%d %d", &y, &x);
        NYKST[i] = kod(y, x);
    }
    /* perejimai */
    for (i = 0; i < P; i++) {
        fscanf(f, "%d %d %d %d", &a1, &b1, &a2, &b2);
        add_kaim(kod(a1, b1), kod(a2, b2));
        add_kaim(kod(a2, b2), kod(a1, b1));
    }

    fclose(f);
}

void write_data(void)
{
    FILE *f = fopen("NAMAS.REZ", "w");
    int   i;

    fprintf(f, "%d\n", NLAIMINGUJU);

    for (i = 0; i < S; i++)
       if (V[NYKST[i]].ejimai <= t)
           fprintf(f, "%d\n", i+1);

    fclose(f);
}


    int QUEUE[125000];
    int Q = 0;

    void add(int n)
    {
        QUEUE[Q++] = n;
    }

void baltijos_banga(void)
{
    int q, n, i, k, l;

    add(ISEJIMAS);
    for (q = 0; q < Q; q++) {
        n = QUEUE[q];
        for (i = 0; i < V[n].nkaim; i++) {
            k = V[n].kaim[i];
            l = (n/50 == k/50) ? t1 : t2;
            if (V[n].ejimai + l <= t) /* deadline atkirtimas */
                if (V[n].ejimai + l <  V[k].ejimai) {
                    V[k].ejimai = V[n].ejimai + l;
                    add(k);
                }
        }   /* for i... */
    }   /* for q... */
}

int main(void)
{
    int i;

    read_data();
    for (i = 0; i < 2500; i++)
        V[i].ejimai = 66666666;

    V[ISEJIMAS].ejimai = t1;

    baltijos_banga();

    for (i = 0; i < S; i++)
        if (V[NYKST[i]].ejimai <= t)
            NLAIMINGUJU++;

    write_data();

    return 0;
}

