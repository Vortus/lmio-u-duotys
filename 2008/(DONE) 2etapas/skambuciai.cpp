#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("skambuciai.in");
    int N; fi >> N;
    fi.close();

    int c = 0, n = 1;
    while(n < N){
        n *= 2;
        c++;
    }

    ofstream fo("skambuciai.out");
    fo << c;
    fo.close();

 return 0; }
