#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Box{
        int ID;
        bool banana;
    } boxes[12];

    int getIndex(int N, int n){
        for(int i = 1; i <= N; i++)
            if(boxes[i].ID == n) return i;
        return -1;
    }

 int main(){

    ifstream fi("band.in");
    int N = 0, M = 0; fi >> N >> M;

    for(int i = 1; i < 12; i++){
        boxes[i].ID = i;
        boxes[i].banana = true;
    }
    int result = 0;

    for(int i = 0; i < M; i++){
        int a, b, c; fi >> a >> b >> c;
        if(boxes[a].banana) result++;
        boxes[a].banana = false;
        int ai = getIndex(N, b);
        int bi = getIndex(N, c);
        swap(boxes[ai], boxes[bi]);
    }
    fi.close();

    ofstream fo("band.out");
    fo << result;
    fo.close();

 return 0; }
