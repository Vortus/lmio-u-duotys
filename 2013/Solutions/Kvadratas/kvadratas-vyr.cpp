#include <list>
#include <algorithm>
#include <vector>

#include <iostream>
#include <cstdio>


using namespace std;


void skaityk(vector<int>& skaicius){

   int K;
   cin >> K;
   for(int i=0; i<K; i++){
      int sk;
      cin >> sk;
      skaicius.push_back(sk);
   }
}


void kvadratas(vector<int>& skaicius, vector<int>& atsakymas){
      // kv. suma:
      for(int i=0; i<skaicius.size(); i++){

         atsakymas.push_back( skaicius[i] * 2 );

      }

     // dvigubos sandaugos:
     for (int i=0; i<skaicius.size(); i++)
        for(int j=i+1; j<skaicius.size(); j++){

           int laipsnis = skaicius[i] + skaicius[j] + 1;

           atsakymas.push_back(laipsnis);

     }

   sort(atsakymas.begin(), atsakymas.end(), less<int>());

 }

#define PAB -1

int kitasLaipsnis(int poz, vector<int>&  atsakymas){

     if (poz < atsakymas.size())
        return atsakymas[poz];
     else
        return PAB;
}


void rasyk(vector<int>& atsakymas){
   int btc = 0;
   int poz = 0;
   int pab = atsakymas.size() - 1;
   int bituSuma = 0;
   int kitas = kitasLaipsnis(poz,atsakymas);
  
   int veiksmai = 0;
  
   vector<int> spausdinimui; 
   
   while (kitas != PAB){

     int vienodi = poz;
     int laipsnis = atsakymas[ poz ];
     int pleciamas =  kitas;
     veiksmai++;
     do {
        bituSuma++;
        vienodi++;
        kitas = kitasLaipsnis(vienodi,atsakymas);
        veiksmai++;
     } while ( kitas == laipsnis );

     do {
        veiksmai++;
        if ((bituSuma % 2) != 0)   {
           btc++;
           spausdinimui.push_back(pleciamas);
        }

        bituSuma >>= 1;

        pleciamas++;

     } while ( ( pleciamas != kitas ) && ( bituSuma != 0 ));

     poz = vienodi;

   }
  // cout << veiksmai << endl << endl;
   
   int V = spausdinimui.size();
   cout << V << endl;
   for(int i=0; i < V; i++)
      cout << spausdinimui[ i ] << endl;
}

int main()
{
    
    freopen("kvadratas-vyr.in","r", stdin);
    freopen("kvadratas-vyr.out","w", stdout);

    vector<int> skaicius;
    vector<int> atsakymas;

    skaityk(skaicius);
    sort(skaicius.begin(), skaicius.end(), less<int>());

    kvadratas(skaicius, atsakymas);

    rasyk(atsakymas);

    return 0;
}
