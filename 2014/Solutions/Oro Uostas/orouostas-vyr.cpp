#include <algorithm>
#include <cstdio>
#include <vector>

using namespace std;

const int MaxV = 10010;

struct Lektuvas {
	int t, v, indeksas;
	
	bool operator<(const Lektuvas &l) const {
		return v < l.v;
	}
};

/*
Kodas žemiau parašytas pagal šią funkciją:

int daugiausiai_pakils(int i, int laikas) {
	if (i == N)
		return 0;
	int ans = daugiausiai_pakils(i + 1, laikas);
	if (laikas + L[i].t <= L[i].v)
		ans = max(ans, 1 + daugiausiai_pakils(i + 1, laikas + L[i].t));
	return ans;
}

Uždavinio atsakymas: daugiausiai_pakils(0, 0)
*/

int main() {
	freopen("orouostas-vyr.in", "r", stdin);
	freopen("orouostas-vyr.out", "w", stdout);
	int N;
	scanf("%d", &N);
	vector<Lektuvas> L(N);
	for (int i = 0; i < N; i++) {
		L[i].indeksas = i + 1;
		scanf("%d%d", &L[i].t, &L[i].v);
	}
	
	sort(L.begin(), L.end());
	
	vector<int> max_kils_i(MaxV, 0), max_kils_i1(MaxV, 0);
	
	for (int i = N - 1; i >= 0; i--) {
		swap(max_kils_i, max_kils_i1);
		for (int v = 0; v < MaxV; v++) {
			int jei_nekils = max_kils_i1[v];
			int jei_kils = -1;
			if (v + L[i].t <= L[i].v)
				jei_kils = 1 + max_kils_i1[v + L[i].t];
			max_kils_i[v] = max(jei_nekils, jei_kils);
		}
	}
	
	printf("%d\n", max_kils_i[0]);
	return 0;
}
