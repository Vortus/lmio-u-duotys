/**
 * Sprendimo idėja:
 *
 * Surandame du trumpiausius kelius nuo Biržų iki Prienų – vieną, kuris būtinai
 * yra lyginio ilgio, ir vieną, kuris būtinai yra nelyginio ilgio. Tuomet,
 * į Prienus važiuosime trumpiausiu lyginiu arba nelyginiu keliu, ir tada tiesiog
 * važinėsime iš Prienų į bet kurį gretimą miestą ir atgal iki tol, kol bus galima
 * papietauti Prienuose.
 *
 * Uždavinio autorius: Vytautas Gruslys
 * Pavyzdinio sprendimo autorius: Martynas Budriūnas
 * Pavyzdinio sprendimo aprašymo autorius: Marijonas Petrauskas
 */

#include      <vector>
#include      <queue>
#include      <iostream>
#include      <limits>
#include      <cassert>
using namespace std;

enum {
	LYGINIS = 0,
	NELYGINIS = 1
};

// Grafo viršūnė.
struct Miestas {
	vector<int> jungiasi_su;
	// Nurodo trumpiausio lyginio ir nelyginio kelio nuo Biržų iki šios
	// viršūnės ilgius. Jei ilgis yra lygus -1, šios viršūnės pasiekti
	// nepavyksta.
	int atstumai[2];

	Miestas() {
		atstumai[LYGINIS] = -1;
		atstumai[NELYGINIS] = -1;
	}
};

Miestas miestai[1000001];
const int BIRZAI = 1;
int PRIENAI;

int dalyba_apvalinant_i_virsu(int dalinys, int daliklis) {
	return (dalinys / daliklis) + ((dalinys % daliklis) != 0);
}

int gauti_atstuma(int nukeliauta, int valgyt_kas, bool lyginis_kartotinis) {
	if (nukeliauta == -1) {
		return numeric_limits<int>::max();
	}

	int artimiausias_kartotinis = dalyba_apvalinant_i_virsu(nukeliauta, valgyt_kas) * valgyt_kas;

	// Bandome pataisyti kartotinius kurie yra netinkamo lyginumo.
	if ((lyginis_kartotinis && artimiausias_kartotinis % 2 == NELYGINIS)
		 || (!lyginis_kartotinis && artimiausias_kartotinis % 2 == LYGINIS)) {
		// Lyginumą galima pakeisti tik su nelyginiu `valgyt_kas`.
		assert(valgyt_kas % 2 == NELYGINIS);
		artimiausias_kartotinis += valgyt_kas;
	}

	const int dar_reikia_aplankyti = artimiausias_kartotinis - nukeliauta;

	return dar_reikia_aplankyti + nukeliauta;
}

int main() {
	// Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis
	// iš failo.
	// freopen("turistai-vyr.in", "r", stdin);
	// freopen("turistai-vyr.out", "w", stdout);

	// Nuskaitome duomenis.
	int miestu_sk, keliu_sk, valgyt_kas;
	cin >> miestu_sk >> keliu_sk >> valgyt_kas;

	PRIENAI = miestu_sk;

	for (int i = 0; i < keliu_sk; i += 1) {
		int miestas1, miestas2;
		cin >> miestas1 >> miestas2;
		miestai[miestas1].jungiasi_su.push_back(miestas2);
		miestai[miestas2].jungiasi_su.push_back(miestas1);
	}

	// Randame trumpiausius atstumus (lyginius ir nelyginius) iki visų viršūnių,
	// atlikdami paiešką platyn.

	queue<pair<int, int> > eile; // eilėje laikysime miesto numerį ir atstumą iki miesto.

	// Biržus galime aplankyti neatlikę nei vieno žingsnio (vienetą pridėsime sprendimo
	// pabaigoje).
	miestai[BIRZAI].atstumai[LYGINIS] = 0;
	eile.push(make_pair(BIRZAI, 0));

	while (!eile.empty()) {
		const Miestas &dabar_mieste = miestai[eile.front().first];
		const int nukeliautas_atstumas = eile.front().second;
		eile.pop();

		for (int i = 0; i < dabar_mieste.jungiasi_su.size(); i += 1) {
			Miestas &keliaujam_i = miestai[dabar_mieste.jungiasi_su[i]];
			const bool atstumo_lyginumas = (nukeliautas_atstumas + 1) % 2;
			// Jeigu miestas dar neaplankytas su atitinkamu lyginumu
			if (keliaujam_i.atstumai[atstumo_lyginumas] == -1) {
				// Aplankome
				keliaujam_i.atstumai[atstumo_lyginumas] = nukeliautas_atstumas + 1;
				eile.push(make_pair(dabar_mieste.jungiasi_su[i], nukeliautas_atstumas + 1));
			}
		}
	}

	// Atliekam šokinėjimą iš/į Prienus, bandydami rasti pirmą kelią kurio
	// ilgis yra `valgyt_kas` kartotinis.

	int rezultatas = numeric_limits<int>::max();

	// Nagrinėjame atvejus.
	if (valgyt_kas % 2 == LYGINIS) {
		// Šokinėjimas iš/į gali padidinti nueitą atstumą tik lyginiu skaičiumi,
		// tad lygini kartotinį galime išgauti pradėdami tik nuo lyginio atstumo.
		rezultatas = gauti_atstuma(miestai[PRIENAI].atstumai[LYGINIS], valgyt_kas, true);
	} else {
		// Bandome keliauti iki artimiausio lyginio kartotinio.
		rezultatas = min(rezultatas, gauti_atstuma(miestai[PRIENAI].atstumai[LYGINIS], valgyt_kas, true));
		// Bandome keliauti iki artimiausio nelyginio kartotinio.
		rezultatas = min(rezultatas, gauti_atstuma(miestai[PRIENAI].atstumai[NELYGINIS], valgyt_kas, false));
	}

	if (rezultatas == numeric_limits<int>::max()) {
		cout << -1 << endl;
	} else {
		cout << rezultatas + 1 << endl;
	}
	
	return 0;
}
