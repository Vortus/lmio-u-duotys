#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int buffer[256];

    void refresh(){
        fill(buffer, buffer + 256, 0);
    }

 int main(){

    ifstream fi("puslapiavimas.in");
    int N, minRes = 2000000000, maxRes = -1, pageSize;
    fi >> N >> pageSize; fi.ignore(80, '\n');

    while(!fi.eof()){
        refresh();
        for(int i = 0; i < pageSize; i++){
            if(fi.eof()) break;
            char c; fi.get(c); fi.ignore(80, '\n');
            buffer[(int)c]++;
        }
        int c = 0;
        for(int i = 1; i <= 255; i++)
            if(buffer[i] != 0) c++;
        if(c < minRes) minRes = c;
        if(c > maxRes) maxRes = c;
    }
    fi.close();

    ofstream fo("puslapiavimas.out");
    fo << minRes << endl << maxRes;
    fo.close();

 return 0; }
