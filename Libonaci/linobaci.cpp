#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

    ifstream fi("linobaci.in");
	int N; fi >> N;
	int number = 0, result = 0, tmp = 0; fi >> number;

    if(number == 1){
        tmp = number; result++;
        for(int i = 1; i < N; i++){
            int j; fi >> j;
            if(j > tmp) break;
            if(tmp <= 1000000000) tmp += j;
            result++;
        }
    }

    fi.close();

    ofstream fo("linobaci.out");
    fo << result;
    fo.close();

 return 0; }
