#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define MAXK 100010

    struct Weather{
        int t, v;
        bool operator <(const Weather &w) const{
            return v < t.v;
        }
    };

    int N, K; // piratu kiekis, oru
    Weather W[MAXK];

    void readFile(string p){
        ifstream fi(p);
        fi >> N >> K;
        for(int i = 0; i < K; i++)
            fi >> W[i].t >> W[i].v;
        fi.close();
    }

 int main(){

    readFile("piratai.txt");

 return 0; }
