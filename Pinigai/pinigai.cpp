#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

 int main(){

    int result = 0;
    ifstream fi("pinigai.in");
    int n, s; fi >> n >> s;

    int arr[11];
    for(int i = 0; i < n; i++)
        fi >> arr[i];

    int p = s;
    for(int i = n - 1; i >= 0; i--){
        int missing = p - arr[i];
        result += missing;
        p = arr[i] * s;
    }

    ofstream fo("pinigai.out");
    fo << result;
    fi.close();
    fo.close();

 return 0; }
