#include <stdio.h>
#include <string.h>

int main(void)
{
    #define MAXSKAITMENU 1010

    char skaitmenys[MAXSKAITMENU];
    int  i;
    
    freopen("SKAICIUS.IN", "r", stdin);
    freopen("SKAICIUS.OUT", "w", stdout);
    
    scanf("%s", skaitmenys);
    i = strlen(skaitmenys) - 1;

    /* praleidžiame nulius */    
    while (i > 0 && skaitmenys[i] == '0')
        --i;
    
    /* atspausdiname likusius skaitmenis */
    while (i >= 0)
        putchar(skaitmenys[i--]);
    putchar('\n');
    
    return 0;
}

