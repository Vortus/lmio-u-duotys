{
TASK: LINOBACI
LANG: PASCAL
}
program Libonaci;
const
  fin =  'linobaci.in';
  fout = 'linobaci.out';
var
  f, f2: text;
  i, n, suma, a: int64;
  ok: boolean;
begin
  assign (f, fin);
  assign (f2, fout);
  reset (f);
  rewrite (f2);
  readln (f, n);
  read (f, a);
  if (a = 1) then begin
  suma := a; ok := true; i := 1;
  while (ok) and (i < n) do begin
    read (f, a);
    //if a > 1000000 then writeln (f2, 'ERROR');
    if (a > suma)
      then ok := false
      else begin
        suma := suma + a;
        inc (i);
      end;
  end;
  end
  else begin
    ok := false;
    i := 0;
  end;

  if (ok)
    then writeln (f2, n)
    else writeln (f2, i);
  close (f);
  close (f2);
end.
