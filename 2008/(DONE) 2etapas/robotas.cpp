#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    bool board[30][30];

    void explore(int x, int y, int xx, int yy){
        while(y != yy){
            if(yy > y){
                board[y][x] = true;
                y++;
            }else{
                board[y - 1][x - 1] = true;
                y--;
            }
        }

        while(x != xx){
            if(xx > x){
                board[y - 1][x] = true;
                x++;
            }else {
                board[y][x - 1] = true;
                x--;
            }
        }
    }

 int main(){

    double result = 0;
    ifstream fi("robotas.in");
    int endX, endY, N; fi >> endX >> endY >> N;
    int x = 0, y = 0;

    for(int i = 0; i < N; i++){
        int xx, yy; fi >> xx >> yy;
        explore(x, y, xx, yy);
        x = xx; y = yy;
    }
    fi.close();
    if(x != endX || y != endY) explore(x, y, endX, endY);

    for(int i = 0; i < endY; i++){
        for(int j = 0; j < endX; j++){
           //cout << board[i][j] << " ";
            if(board[i][j]) result++;
        }
       // cout << endl;
    }

    ofstream fo("robotas.out");
    fo << result;
    fo.close();

 return 0; }
