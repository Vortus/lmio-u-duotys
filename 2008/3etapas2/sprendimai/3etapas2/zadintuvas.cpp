#include "zadintuvas_bib.h"
#include <cstdio>
#include <vector>
#define pb push_back
#define rep(i,n) for(i=0;i<n;++i)
using namespace std;
typedef vector<char> vc;
int main(){
	pasiruosti();
	freopen("zadintuvas.in", "r", stdin);
	int N, M, i, j, a, d, m, i2=-1, j2;
	char c;
	vector<vc> k;
	vector<int> x, y, x2, y2, v;
	scanf("%i%i ", &N, &M);
	rep(j, N){
		k.pb(vc());
		rep(i, M){
			scanf("%c ", &c);
			k[j].pb(c);
			if (i2==-1&&k[j][i]=='.') i2=i;
		}
	}
	a=atstumas(1, i2+1);
	for (i=max(0,i2-a); i<min(a+i2+1,M); ++i){
		j=a-abs(i-i2);
		if (j<N){x.pb(i); y.pb(j);}
	}
	rep(i, M+N) v.pb(0);
	while (x.size()>1){
		m=x.size();
		rep(j, N) rep(i, M)	if (k[j][i]=='.'){
				rep(a, M+N) v[a]=0;
				rep(a, x.size()) ++v[abs(x[a]-i)+abs(y[a]-j)];
				d=0;
				rep(a, M+N) d=max(d, v[a]);
				if (d<m){
					m=d;
					i2=i; j2=j;
				}
			}
		a=atstumas(j2+1, i2+1);
		x2=x; y2=y;
		x.clear(); y.clear();
		rep(i, x2.size())
			if (abs(i2-x[i])+abs(j2-y[i])==a){
				x.pb(x2[i]);
				y.pb(y2[i]);
			}
	}
	zadintuvas(y[0]+1, x[0]+1);	
}
