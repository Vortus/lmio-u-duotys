#include <iostream>
#include <vector>
#include <queue>
#include <cstdio>
#define MAX_N  1000000
#define MAX_BS 100


using namespace std;

int min(int a, int b){
      if ( a < b ) return a;
      else return b;
}

int fun(int a, int b, int f){
   if ( !f) return min(a,b);
   else return -min(-a,-b);
}


int main ()
{
  queue<int> eile;

  int N;
  freopen("zaidimas-vyr.in","rt", stdin);
  freopen("zaidimas-vyr.out","w", stdout);

  vector<int> ivertinimai[2];

  cin >> N;

  int N1 = N+1;

  int ribos[] = {N1, -1};
  for(int i=0; i <= N; i++)
    for (int j=0; j < 2; j++ )
       ivertinimai[j].push_back(ribos[j]);

  ivertinimai[0][1] = ivertinimai[1][1] = 1;

  for(int i=1; i<=(N-1); i++){
      int S,B;
      cin >> S >> B;
      for (int k=0; k<2; k++)
         if (ivertinimai[k][i] != ribos[k]){
            for(int j=S; j<=B; j++) {
               int v = min(i+j,N);
               int c1 = 1 + ivertinimai[k][i];
               ivertinimai[k][v] = fun(c1, ivertinimai[k][v], k);
            }
         }
   }

  cout << ivertinimai[0][ N ] << " " <<
          ivertinimai[1][ N ]  << endl;;

  return 0;

}
