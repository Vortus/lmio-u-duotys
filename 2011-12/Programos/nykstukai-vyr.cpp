/**
 * Laikas: O(MN^2)
 * Atmintis: O(N)
 *
 * Stengiausi susilaikyti nuo optimizacijų aiškumo vardan.
 */

#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Grąžina didėjimo tvarka išrikiuotą pozicijų sąrašą, kuriose stovi nykštukai,
 * kai reikšmė value.
 */
vector<int> dwarf_positions(long long value)
{
    vector<int> result;
    for (int position = 0; (1LL << position) <= value; ++position)
    {
        if ((1LL << position) & value) result.push_back(position);
    }
    return result;
}

/**
 * Pagal didėjimo tvarka išrikiuotus pozicijų sąrašus grąžina nykštukų
 * nueitų atstumų sumą perėjimui iš positionsBefore į positionsAfter N bitų
 * registre, jei iš jaunesniųjų bitų pusės ateina come nykštukų (jei come
 * neneigiamas) arba išeina -come (jei come neigiamas).
 */
int distance(vector<int>& positionsBefore, vector<int>& positionsAfter, int N,
             int come)
{
    int result = 0, before = 0, after = 0;
    while (before < -come)
    { // išvedami nykštukai per mažesnių adresų pusę
        result += positionsBefore[before++] + 1;
    }
    while (after < come)
    { // įvedami nykštukai per mažesnių adresų pusę
        result += positionsAfter[after++] + 1;
    }
    while (after < positionsAfter.size() && before < positionsBefore.size())
    { // perkeliami esami nykštukai
        result += abs(positionsAfter[after++] - positionsBefore[before++]);
    }
    while (before < positionsBefore.size())
    { // išvedami pertekliniai nykštukai per didesnių adresų pusę
        result += N - positionsBefore[before++];
    }
    while (after < positionsAfter.size())
    { // atvedami trūkstami nykštukai iš didesnių adresų pusės
        result += N - positionsAfter[after++];
    }
    return result;
}

/**
 * Pagal didėjimo tvarka išrikiuotus pozicijų sąrašus grąžina minimalią nykštukų
 * nueitų atstumų sumą perėjimui iš positionsBefore į positionsAfter N bitų
 * registre.
 */
int distance(vector<int>& positionsBefore, vector<int>& positionsAfter, int N)
{
    int result = N * N; // didžiausias įmanomas galutinis rezultatas:
                        // (N / 2 + 1) * (N / 2) + N % 2 * (N + 1) / 2 < N * N
    for (int i = -(int)positionsBefore.size(); i <= (int)positionsAfter.size();
         ++i)
    {
        result = min(result, distance(positionsBefore, positionsAfter, N, i));
    }
    return result;
}

int main()
{
    int N, M, result = 0;
    freopen("nykstukai-vyr.in", "r", stdin);
    freopen("nykstukai-vyr.out", "w", stdout);
    vector<int> positionsBefore, positionsAfter;
    scanf("%d%d", &N, &M);
    for (int i = 0; i < M; ++i)
    {
        long long value;
        scanf("%lld", &value);
        positionsAfter = dwarf_positions(value);
        result += distance(positionsBefore, positionsAfter, N);
        positionsBefore = positionsAfter;
    }
    printf("%d\n", result);
    return 0;
}
