#include <cstdio>

#define MaxN 1000010
#define MaxP 1000010

char map[MaxN] = {0};
bool dega[MaxN] = {0};
int N, P, p, ans;

int main() {
    freopen("milzinas-vyr.in", "r", stdin);
    freopen("milzinas-vyr.out", "w", stdout);
    scanf("%d%d\n", &N, &P);
    for (int i = 1; i <= N; i++)
        scanf("%c", &map[i]);
    
    for (int i = 1; i <= N; i++) {
        if (map[i] == 'L') {
            int j = i; dega[j] = true;
            while (map[j-1] == 'D') {
                j--;
                dega[j] = true;
            } 
            j = i;
            while (map[j+1] == 'K') {
                j++; dega[j] = true;   
            }
        }    
    }
    
    for (int i = 0; i < P; i++) {
        scanf("%d", &p);
        ans += (!dega[p]);
    }
    printf("%d\n", ans);
    return 0;
}
