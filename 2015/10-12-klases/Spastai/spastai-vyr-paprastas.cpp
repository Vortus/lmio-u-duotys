 /**
 * Sprendimo aprašymas:
 *
 * Uždavinys sprendžiamas paieškos gilyn algoritmu specialiame grafe,
 * kuris sudaromas pagal tokias taisykles:
 *
 *   - kiekvienai pozicijai labirinte (x, y) ir skirtingam gyvybių
 *     skaičiui k grafe sukuriama po viršūnę (x, y, k). Išimtis
 *     galioja langeliams, kurie pažymėti kaip siena.
 *
 *   - iš (x1, y1, k) galima eiti į (x2, y2, k), jeigu (x1, y1) ir
 *     (x2, y2) yra gretimi langeliai, ir juose nėra spąstų.
 *
 *   - iš (x1, y1, k) galima eiti į (x2, y2, k - 1), jeigu (x1, y1) ir
 *     (x2, y2) yra gretimi langeliai, ir (x2, y2) yra spąstai.
 *
 * Pastebėkime, kad iš kraštinio langelio kaimyninių langelių nagrinėti
 * nebūtina, kadangi visi tolimesni sprendimai būtų blogesni už esamą.
 *
 * Autorius: Linas Petrauskas
 */

#include <cstdio>
#include <utility>
#include <vector>

using namespace std;

const int MAXN = 1000; // Maksimali kraštinė.
const int MAXK = 10;   // Maksimalus gyvybių skaičius.
const int INFINITY = MAXN*MAXN + 1;

char map[MAXN][MAXN];  // Labirinto žemėlapis.
int D[MAXN][MAXN][MAXK + 1];  // Atstumų iki kiekvienos pozicijos masyvas.

struct Position {
    Position(int x, int y, int k) : x(x), y(y), k(k) {}
    int x;
    int y;
    int k;
};

int main() {
    freopen("spastai.in", "r", stdin);
    freopen("spastai-vyr.out", "w", stdout);

    int N, M, K;
    vector<Position> queue;

    // Duomenų perskaitymas.
    scanf("%d%d%d\n", &N, &M, &K);
    for (int y = 0; y < M; y++) {
        for (int x = 0; x < N; x++) {
            scanf("%c", &map[y][x]);
            if (map[y][x] == 'P') {
                queue.push_back(Position(x, y, K));
            }
        }
        scanf("\n");
    }

    // Geriausio kelio paieška.
    int best_k = 0;
    int best_d = 0;
    for (int q = 0; q < queue.size(); q++) {
        const Position p = queue[q];
        if (p.x == 0 || p.x == N - 1 || p.y == 0 || p.y == M - 1) {
            if (best_k < p.k || (best_k == p.k && best_d > D[p.y][p.x][p.k])) {
                best_k = p.k;
                best_d = D[p.y][p.x][p.k];
            }
        } else {
            for (int dx = -1; dx <= 1; dx++) {
                for (int dy = -1; dy <= 1; dy++) {
                    if ((dx == 0) == (dy == 0))
                        continue;
                    Position r = Position(p.x + dx, p.y + dy, p.k);
                    if (map[r.y][r.x] == '#')
                        continue;
                    if (map[r.y][r.x] == '!')
                        r.k -= 1;
                    if (r.k == 0)
                        continue;
                    if (D[r.y][r.x][r.k] == 0) {
                        D[r.y][r.x][r.k] = D[p.y][p.x][p.k] + 1;
                        queue.push_back(r);
                    }
                }
            }
        }
    }

    printf("%d %d\n", best_k, best_d);

    return 0;
}
