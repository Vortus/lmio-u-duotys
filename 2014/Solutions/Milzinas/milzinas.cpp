#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

    vector<char> board(1000002);
    vector<int> buffer(1000002, -1);
    int mapSize;

    int notBurnExplore(int startPos){
        if(buffer[startPos] != -1)
            return buffer[startPos];

        int position = startPos;
        //neapskaiciavom kaire desine
        if(board[position] == 'K'){
            while(buffer[position] == -1)
                position--;
            int j = buffer[position]; position++;
            while(buffer[position] == -1){
                buffer[position] = j;
                position++;
            }
            return j;
        }else if(board[position] == 'D'){
           while(buffer[position] == -1)
                position++;
            int j = buffer[position]; position--;
            while(buffer[position] == -1){
                buffer[position] = j;
                position--;
            }
            return j;
        }
    }

 int main(){

    /////Failo nuskaitymas
    ifstream fi("milzinasbig.in");
    int pC; fi >> mapSize >> pC;
    for(int i = 1; i <= mapSize; i++){ // boarda nuskaito
        fi >> board[i];
        if(board[i] == 'L') buffer[i] = 0;
        if(board[i] == 'S') buffer[i] = 1; // nesudega
    }
    buffer[0] = 1;
    buffer[mapSize + 1] = 1;
    ////Sprendimas
    int result = 0;
    for(int i = 0; i < pC; i++){
        int j; fi >> j;
        result += notBurnExplore(j);
    }
    fi.close();

    ofstream fo("milzinas.out");
    fo << result;
    fo.close();

 return 0; }
