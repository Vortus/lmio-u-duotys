﻿/*
TASK: italai
LANG: C++
*/
/*
Autorius: Vytis

Šis sprendimas pateikiamas smalsiems skaitytojams, norintiems patyrinėti Radix sort algoritmą.
Sudėtingumas O(n)
*/
#include <cstdio>
#include <algorithm>

#define MAXN 100000

#define NDIG 8
#define MDIG 16

int n;
int L[2*MAXN];
int T[2*MAXN];

struct digit {
	int d;
	digit(int d): d(d) {}
	int operator()(int x) { return (x >> (d*4)) & 0xf; }
};

void bucket_sort(int A[], int n, int R[], digit dig) {
	int cnt[MDIG] = {0};
	for (int i = 0; i < n; ++i)
		++cnt[dig(A[i])];
	int st[MDIG] = {0};
	for (int i = 1; i < MDIG; ++i)
		st[i] = st[i-1] + cnt[i-1];
	for (int i = 0; i < n; ++i)
		R[st[dig(A[i])]++] = A[i];
}

void radix_sort() {
	for (int i = 0; i < NDIG; ++i)
		bucket_sort(i % 2 ? T : L, n, i % 2 ? L : T, digit(i));
}

int main() {
	freopen("italai.in", "r", stdin);
	freopen("italai.out", "w", stdout);
	scanf("%*d%d", &n);
	for (int i = 0; i < n; ++i) {
		scanf("%d%d", &L[2*i], &L[2*i+1]);
		++L[2*i+1];
	}
	radix_sort();
	int c = 0;
	for (int i = 0; i < n; ++i)
		c += L[2*i+1] - L[2*i];
	printf("%d\n", c);
	return 0;
}
