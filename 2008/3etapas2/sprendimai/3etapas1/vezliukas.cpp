/*
TASK: vezliukas
LANG: C++
*/

#include <cstdio>
#include <set>
#include <algorithm>
#include <map>
using namespace std;

typedef pair<int, int> pii;

int dx[] = {0, 1, 0, -1};
int dy[] = {1, 0, -1, 0};

int main() {
	freopen("vezliukas.in", "r", stdin);
	freopen("vezliukas.out", "w", stdout);
	map<char, int> dir;
	dir['S'] = 2;
	dir['R'] = 1;
	dir['V'] = 3;
	dir['P'] = 0;
	int n;
	char s[300];
	scanf("%d %s", &n, s);
	set<pii> color;
	for (int i = 0, x = 0, y = 0; i < n; ++i) {
		int cnt = 1;
		if (s[i] == 'C') {
			cnt = s[i+1]-'0';
			i += 2;
		}
		int d = dir[s[i]];
		for (int q = 0; q < cnt; ++q) {
			x += dx[d];
			y += dy[d];
			color.insert(pii(x, y));
		}
	}
	printf("%d\n", color.size());
	return 0;
}
