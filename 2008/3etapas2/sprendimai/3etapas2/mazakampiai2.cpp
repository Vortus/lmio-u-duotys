/*
TASK: mazakampiai
LANG: C++
*/

#include <cstdio>
#include <set>
#include <algorithm>
#include <vector>
using namespace std;

#define MAXN 810

int N;
int res[MAXN];
bool allowed[MAXN];

inline bool compare(int a, int b) {
	if (a % 2) {
		if (b % 2)
			return a > b;
		return 1;
	}
	if (b % 2)
		return 0;
	return a < b;
}

void read() {
	fill(allowed, allowed+MAXN, 0);
	int k;
	scanf("%d%d", &N, &k);
	for (int i = 0, x; i < k; ++i) {
		scanf("%d", &x);
		allowed[x] = 1;
	}
}

bool test() {
	for (int i = 0; i < 4; ++i)
		res[i] = 0;
	for (int i = 4; i <= N; ++i) {
		res[i] = 1000;
		for (int j = 3; j <= i+2-j; ++j)
			if (allowed[j] || allowed[i+2-j]) {
				int r = 1+max(res[j], res[i+2-j]);
				if (compare(r, res[i]))
					res[i] = r;
			}
		if (res[i] == 1000)
			res[i] = 0;
	}
	return res[N] % 2;
}

int main() {
	freopen("mazakampiai.in", "r", stdin);
	freopen("mazakampiai.out", "w", stdout);
	read();
	vector<int> vi;
	for (int i = 3; i < N; ++i)
		if (!allowed[i]) {
			allowed[i] = 1;
			if (!test())
				vi.push_back(i);
			allowed[i] = 0;
		}
	printf("%u\n", vi.size());
	for (int i = 0; i < vi.size(); ++i) {
		if (i)
			printf(" ");
		printf("%d", vi[i]);
	}
	printf("\n");
	return 0;
}
