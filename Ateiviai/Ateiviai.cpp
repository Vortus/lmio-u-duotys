#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define MAXSIZE 2010
    #define INFINITY 2000000000

    int HEIGHT, WIDTH, V; // aukstis, plotis V
    int map[MAXSIZE][MAXSIZE]; // mapas

    void readFile(string p){
        ifstream fi(p);
        fi >> HEIGHT >> WIDTH >> V;
        for(int i = 0; i < HEIGHT; i++)
            for(int j = 0; j < WIDTH; j++)
                fi >> map[i][j];
        fi.close();
    }

    int getSum(int x, int y, int size){ // gauna suma is matricos
        int endx = x + size - 1, endy = y + size - 1;
        if(endx >= WIDTH || endy >= HEIGHT) return INFINITY;
        int result = 0;
        for(int i = y; i <= endy; i++)
            for(int j = x; j <= endx; j++)
                result += map[i][j];
        return result;
    }

 int main(){

    readFile("ateiviai.txt");
    int bestX, bestY, maxsum = -1, bestSize = INFINITY;
    ////Radimas
    for(int i = 0; i < HEIGHT; i++){
        for(int j = 0; j < WIDTH; j++){
            int sum = getSum(j, i, 1), sz = 1;
            while(sum < V){ sz++; sum = getSum(j, i, sz); }
            if(sum >= V && sum != INFINITY){ // jei yra galimas varijantas
                if(sz <= bestSize && sum > maxsum){ bestSize = sz; maxsum = sum; bestX = j; bestY = i; }
            }
        }
    }
    cout << bestY << " " << bestX << " " << bestSize << " " << maxsum;
 return 0; }
