#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

    const int MAXN = 110, MAXB = 1010;
    int N, B, C;
    vector<int> packs(MAXN);
    int buffer[MAXB][MAXN]; //memo

 int main(){

    ifstream fi("kava.in");
    fi >> B >> C >> N;
    for(int i = 1; i <= N; i++)
        fi >> packs[i];
    fi.close();
    fill(buffer[0], buffer[MAXB], -1); buffer[0][0] = 0;
    int dd = 0;
    for(int i = 1; i <= N; i++){ // per visus packus
        for(int j = B; j >= 0; j--){
            for(int k = C; k >= 0; k--){
                if(buffer[j][k] < 0 && k && j >= packs[i] &&
                   buffer[j - packs[i]][k - 1] >= 0)
                   buffer[j][k] = i;
            }
        }
    }

    ofstream fo("kava.out");
    if(buffer[B][C] < 0){ // negalima sudaryti
        fo << 0;
    }else{
        while(C){
            fo << buffer[B][C] << endl;
            B -= packs[buffer[B][C]];
            C--;
        }
    }
    fo.close();

 return 0; }
