//Sprendimas, surenkantis 99 taškus

#include <cstdlib>
#include "korteles.h"

static const int MAX_N = 1000;
static const int MAX_K = 2131;
int AA[MAX_N+1][MAX_N];

void pasiruosti()
{
    int K, i, N;
    for (N = 1; N <= MAX_N; N++)
    {
        bool jau[MAX_K] = {false};
        for (i = 0; i < N; i++)
        {
            do {K = rand() % MAX_K;} while (jau[K]);
            jau[K] = true;
        }
        i = 0;
        for (K = 0; K < MAX_K; K++)
            if (jau[K])
            {
                AA[N][i] = K + 1;
                i++;
            }
    }
}

void korteliuRinkinys(int N, int A[])
{
    for (int i = 0; i < N; i++) {
        A[i] = AA[N][i];
    }
}

