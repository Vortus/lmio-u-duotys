#include <vector>
#define pb push_back
#define rep(i,k) for(i=0;i<k;++i)
#include <string>
using namespace std;

int main(){
	freopen("nuliai.in", "r", stdin);
	freopen("nuliai.out", "w", stdout);
	int k, i, a, j, n;
	scanf("%i", &n);
	vector<int> x;
	vector<string> r;
	++n;
	rep(k, n) {
		x.pb(-1);
		r.pb("");
	}
	x[0]=1;
	r[0]="0";
	rep(k, n){
		a=1;
		j=1;
		while(j<=k&&a<n) {
			a*=j;
			++j;
		}
		if (a<n&&(x[a]==-1||x[a]>x[k])){
			x[a]=x[k];
			if (k==0) r[a]="0!";
			else {
				r[a]="(";
				r[a].append(r[k]);
				r[a].append(")!");
			}
		}
		rep(i, k+1){
			a=i+k;
			if (a<n&&(x[a]==-1||x[a]>x[k]+x[i])){
				x[a]=x[k]+x[i];
				r[a]=r[k];
				r[a].append("+");
				r[a].append(r[i]);
			}
		}
		rep(i, k+1){
			a=i*k;
			if (a<n&&(x[a]==-1||x[a]>x[k]+x[i])){
				x[a]=x[k]+x[i];
				r[a]="(";
				r[a].append(r[k]);
				r[a].append(")*(");
				r[a].append(r[i]);
				r[a].append(")");
			}
		}
		a=1;
		i=1;
		while(i<=k&&a<n){
			a*=k;
			if (a<n&&(x[a]==-1||x[a]>x[k]+x[i])){
				x[a]=x[k]+x[i];
				r[a]="(";
				r[a].append(r[k]);
				r[a].append(")^(");
				r[a].append(r[i]);
				r[a].append(")");
			}
			++i;
		}
		i=2;
		a=1;
		while(i<=k&&a<n){
			a=1;
			j=0;
			while(j<k&&a<n) {
				a*=i;
				++j;
			}
			if (a<n&&(x[a]==-1||x[a]>x[k]+x[i])){
				x[a]=x[k]+x[i];
				r[a]="(";
				r[a].append(r[i]);
				r[a].append(")^(");
				r[a].append(r[k]);
				r[a].append(")");
			}
			++i;
		}
	}
	printf ("%i\n%s\n", x[n-1], r[n-1].c_str());
}
