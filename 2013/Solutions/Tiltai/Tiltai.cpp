#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#define MAXN 100010
#define FILE "tiltai.in"
using namespace std;

    int N, M;
    vector<int> links[MAXN];
    bool explored[MAXN];

    void readFile(){
        int a, b;
        ifstream fi(FILE); fi >> N >> M;
        for(int i = 0; i < N; i++) fi >> a >> b;
        for(int i = 0; i < M; i++){
            fi >> a >> b;
            links[a].push_back(b);
            links[b].push_back(a);
        }
        fi.close();
    }

    void DFS(int v){
        if(!explored[v]){
            explored[v] = true;
            for(int i = 0; i < links[v].size(); i++)
                DFS(links[v][i]);
        }
    }

 int main(){

    readFile(); int c = 0;
    for(int i = 1; i <= N; i++){
        if(!explored[i]){
            c++;
            DFS(i);
        }
    }
    cout << M - (N - c);

 return 0; }
