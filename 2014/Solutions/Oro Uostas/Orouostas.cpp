#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

    #define MAXV 10010
    #define FILE "orouostas.in"

    struct Plane {
        int t, v;
        bool operator <(const Plane &p) const{
            return v < p.v;
        }
    };

    int N;
    vector<Plane> P(MAXV);

    void readFile(){
        ifstream FI(FILE); FI >> N;
        int a, b, i = 0;
        while(FI >> a >> b){
            P[i].t = a; P[i].v = b;
            i++;
        }
        FI.close();
        sort(P.begin(), P.begin() + N);
    }

 int main(){

    readFile();
    vector<int> buffer(MAXV, 0), buffer1(MAXV, 0);

    for(int i = N - 1; i >= 0; i--){
        swap(buffer, buffer1);
        for(int v = 0; v < MAXV; v++){
            int a = -1;
            if(v + P[i].t <= P[i].v)
                a = 1 + buffer1[v + P[i].t];
            buffer[v] = max(buffer1[v], a);
        }
    }

    cout << buffer[0];

 return 0; }
