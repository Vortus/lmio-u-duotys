#include <cstdio>
#include <algorithm>
#define MAX_N 1001
#define TASK "kroviniai-jau"
using namespace std;

long long A[MAX_N];

int main()
{
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out","w",stdout);
	int N;
    scanf("%d", &N);
    
    long long weight = 0, maxWeight = 0;
    for (int i = 1; i <= N; i++)
	{
        weight -= A[i];
		int M;
        scanf("%d", &M);
        for (int j = 0; j < M; j++)
		{
			int S;
			long long P;
            scanf("%d %lld", &S, &P);
            A[S] += P;
            weight += P;
        }
		if (weight > maxWeight)
		{
			maxWeight = weight;
		}
    }
    printf("%lld\n", maxWeight);
    return 0;   
}
