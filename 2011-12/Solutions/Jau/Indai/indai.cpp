#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define MAXN 1010
    #define MAXLT 110
    int N, // dienu skaicius
        L, // turima viso leksciu
        T, // tableciu
        M; // maksimaliai kiek isplauna

    int buffer[MAXN][MAXLT][MAXLT]; //
    int K[MAXN]; //leksciu reikalingu

    void readFile(){
        ifstream fi("indai.in");
        fi >> N >> L >> T >> M;
        for(int i = 0; i < N; i++)
            fi >> K[i];
        fi.close();
    }

    int explore(int i, int j, int k){
        if(i == N - 1) return 0;
        if(buffer[i][j][k] != -1) return buffer[i][j][k];
        int need = K[i + 1] - j;
        int result = max(0, need) + explore(i + 1, max(0, -need), k);
        if(k > 0){
            int nj = min(j + M, L);
            int nneed = K[i + 1] - nj;
            result = min(result, max(0, nneed) + explore(i + 1, max(0, -nneed), k - 1));
        }
        return buffer[i][j][k] = result;
    }

 int main(){

    readFile();
    fill(buffer[0][0], buffer[N][0], -1);
    cout << explore(0, L - K[0], T);

 return 0; }
