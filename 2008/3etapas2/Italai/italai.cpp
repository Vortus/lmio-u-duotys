#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
using namespace std;

    const int MAXN = 2 * 100010;
    int buffer[MAXN]; // bufferis
    int T, N, c = 0;

    void readFile(){
        ifstream fi("italai.in");
        fi >> T >> N;
        for(int i = 0; i < N; i++){
            fi >> buffer[i * 2] >> buffer[i * 2 + 1];
            buffer[i * 2 + 1]++;
        }
        sort(buffer, buffer + N * 2);
        fi.close();
    }

 int main(){

    readFile();
	for (int i = 0; i < N; i++)
        c += buffer[i * 2 + 1] - buffer[i * 2];
    cout << c;

 return 0; }
