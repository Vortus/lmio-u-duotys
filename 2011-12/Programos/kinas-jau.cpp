#include <algorithm>
#include <cstdio>
#define rep(i,n) for(i=0;i<n;++i)
#define maxN 1001
using namespace std;

int u[maxN][maxN], i, j, ok, N;

int main(){
    freopen("kinas-jau.in", "r", stdin);
    freopen("kinas-jau.out", "w", stdout);
    ok=1;
    scanf("%i", &N);
    rep(j,N) if (ok){
        rep(i,N) scanf("%i", &u[j][i]);
        sort(&u[j][0], &u[j][N]);
        if (j) rep(i,N) 
            if (u[j][i]<u[j-1][i]-j-1) ok=0;
    }
    if (ok){
        printf ("TAIP\n");
        rep(j,N) {
            rep(i,N-1) printf("%i ", u[j][i]);
            printf ("%i\n", u[j][N-1]);
        }
    } else printf ("NE\n");
}
