// Irus Grinis
#include <iostream>
#include <cstdio>

using namespace std;

const int MAX_N = 100000;

int main() {
	freopen("faktorialai-jau.in", "r", stdin);
	freopen("faktorialai-jau.out", "w", stdout);
        
   int N;                 // skaičiaus M ir M+1 skaitmenų skaičius 
   int M[MAX_N + 1];      // Naudojame tą patį masyvą skaičiui M, o po to ir M+1 saugoti

   ////////////// Skaitome:
   cin >> N;

	M[ N ] = 0;
   for (int i = N - 1; i >= 0; i--)       
	   cin >> M[ i ];
   
   ///////////////////////////////////////////////////////////////   
   ////////////// Skaičiuojame M + 1:

   int i = 1, pernesame = 1;
   while (pernesame > 0){        
	M[ i ] = (M[ i ] + pernesame) % (i + 1);
      pernesame = (M[ i ] == 0) ? 1 : 0;
      i++;
   }

   
   if(M[ N ] != 0) N++;
   ///////////////////////////////////////////////////////////////   
   ////////////// Išvedame:
     
   cout << N << endl;
	for (int i = N - 1; i >= 0; i--)
	   cout << M[ i ] << endl;

	return 0;
}
