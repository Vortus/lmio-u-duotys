#include <cstdio>
#include <algorithm>
#include <deque>
using namespace std;

#define D 4
// top, right, bottom, left
int dx[D] = {0, 1, 0, -1};
int dy[D] = {1, 0, -1, 0};

#define MAXN 1024

bool wall[MAXN][MAXN][D];
bool been[MAXN][MAXN];

int main() {
	freopen("atkarpos-jau.in", "r", stdin);
	freopen("atkarpos-jau.out", "w", stdout);
	int n, sx, sy, l;
	scanf("%d%d%d%d\n", &n, &sx, &sy, &l);
	fill(wall[0][0], wall[MAXN][0], false);
	fill(been[0], been[MAXN], false);
	for (int i = 0; i < l; ++i) {
		int x1, y1, x2, y2;
		scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
		if (x1 == x2) {
			if (y1 > y2)
				swap(y1, y2);
			for (int y = y1; y < y2; ++y) {
				wall[x1-1][y][1] = true;
				wall[x1][y][3] = true;
			}
		} else { // y1 == y2
			if (x1 > x2)
				swap(x1, x2);
			for (int x = x1; x < x2; ++x) {
				wall[x][y1-1][0] = true;
				wall[x][y1][2] = true;
			}
		}
	}
	
	deque<int> Q;
	Q.push_back(sx * MAXN + sy);
	been[sx][sy] = true;
	bool freedom = false;
	while (!Q.empty() && !freedom) {
		int x = Q.front() / MAXN;
		int y = Q.front() % MAXN;
		Q.pop_front();
		for (int i = 0; i < D; ++i) {
			if (wall[x][y][i])
				continue;
			int xx = x + dx[i];
			int yy = y + dy[i];
			if (xx < 1 || xx > n || yy < 1 || yy > n)
				freedom = true;
			if (been[xx][yy])
				continue;
			Q.push_back(xx * MAXN + yy);
			been[xx][yy] = true;
		}
	}
	printf("%d\n", !freedom);
	return 0;
}
