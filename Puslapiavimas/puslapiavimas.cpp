#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

    int buffer[256];
    int result = 0;

    void refreshBuffer(){
        for(int i = 0; i < 256; i++)
            buffer[i] = 0;
    }

    int getCount(vector<string> strings){
        int c = 0;
        refreshBuffer();

        for(int i = 0; i < strings.size(); i++)
               buffer[strings.at(i)[0]]++;

        for(int i = 1; i < 256; i++)
            if(buffer[i] != 0) c++;

        return c;
    }

 int main(){

    int n, pageSize;
    ifstream fi("puslapiavimas.in");
    fi >> n >> pageSize;

    while(!fi.eof()){
        vector<string> tmp;
        for(int i = 0; i < pageSize; i++){
            string s; fi >> s;
            tmp.push_back(s);
        }
       int j = getCount(tmp);
       if(result < j) result = j;
    }

    fi.close();

    ofstream fo("puslapiavimas.out");
    fo << result;
    fo.close();

 return 0; }
