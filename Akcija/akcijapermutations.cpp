#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

    vector<double> differentItems;
    int differentMax, NMAX;
    double result = -1;

    bool exists(double f){
        for(int i = 0; i < differentItems.size(); i++)
            if(differentItems[i] == f) return true;
        return false;
    }

    void readFile(){
        ifstream fi("akcija.in");
        fi >> differentMax >> NMAX;

        for(int i = 0; i < NMAX; i++){
            double f; fi >> f;
            if(!exists(f)) differentItems.push_back(f);
        }
        sort(differentItems.begin(), differentItems.end());
        fi.close();
    }

    void comb(int N, int K)
    {
        string bitmask(K, 1);
        bitmask.resize(N, 0);
        do {
            double sum = 0;
            double mins = 9999999999999;
            for (int i = 0; i < N; ++i)
            {
                if (bitmask[i]){
                        if(mins > differentItems[i]) mins = differentItems[i];
                        sum += differentItems[i];
                }
            }
            double f = mins / sum;
            if(result < f) result = f;
        } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
    }

 int main(){

    readFile();
    comb(NMAX, differentMax);

    ofstream fo("akcija.out");
    fo << fixed << result * 100.0 << endl;
    fo.close();

 return 0; }
