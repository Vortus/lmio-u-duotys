#include <cstdio>
#include <vector>
#include <queue>
#include <algorithm>
using namespace std;

typedef long long ll;

const int MaxB = 1000000;  // Maksimalus besmegenių skaičius

vector <vector <int> > D;  // i-tojo besmegenio j-tojo rutulio skersmuo
// Pokytis: kiek i-tojo besmegenio aukštis gali būti didesnis / mažesnis už (i-1)-tojo
// pok[0] lygus pirmojo besmegenio aukščiui (aukščiai skaičiuojami tik iš kilnojamų rutulių)
ll pok[MaxB];
ll ats;                    // Uždavinio sprendinys - didžiausias įmanomas aukštis

// Skaitomas duomenų failas
void Skaityti()
{
    freopen("besmegeniai-vyr.in", "r", stdin);
    int B;                 // Besmegenių skaičius
    scanf("%d", &B);
    D.resize(B);
    for (int i = 0; i < D.size(); i++) {
        int R;             // Rutulių, sudarančių besmegenį skaičius.
        scanf("%d", &R);
        D[i].resize(R);
        for (int j = 0; j < D[i].size(); j++) scanf("%d", &D[i][j]);
    }
}

// Uždavinio sprendimas: Kiekvienam kilnojamam rutuliui apskaičiuojami kraštiniai besmegeniai, kurie
// gali būti pasiekiami. Naudojant prioritetinę eilę, kurioje yra kilnojami rutuliai, dar neturintys kraštinio
// besmegenio, gaunamas sudėtingumas pagal laiką - O(N log N), N - rutulių skaičius.
void Spresti()
{
    // Kilnojamų rutulių, dar neturinčių kraštinio pasiekiamo besmegenio, skersmenys.
    priority_queue <int> Kiln;
    // Kiekvienam kilnojamam rutuliui ieškomi pasiekiami besmegeniai iš kairės
    for (int i = D.size() - 1; i >= 0; i--) {
        while (!Kiln.empty() && D[i][0] < Kiln.top()) {
            pok[i + 1] += Kiln.top();
            Kiln.pop();
        }
        for (int j = 1; j < D[i].size(); j++) Kiln.push(D[i][j]);
    }
    while (!Kiln.empty()) {
        pok[0] += Kiln.top();
        Kiln.pop();
    }
    // Kiekvienam kilnojamam rutuliui ieškomi pasiekiami besmegeniai iš dešinės
    for (int i = 0; i < D.size(); i++) {
        while (!Kiln.empty() && D[i][0] < Kiln.top()) {
            pok[i] -= Kiln.top();
            Kiln.pop();
        }
        for (int j = 1; j < D[i].size(); j++) Kiln.push(D[i][j]);
    }
    // Išrenkamas geriausias variantas
    ll h = 0;    // Aukštis, sudaromas iš kilnojamų rutulių
    for (int i = 0; i < D.size(); i++) {
        h += pok[i];
        ats = max(ats, h + D[i][0]);
    }
}

// Rašomas rezultatų failas
void Spausdinti()
{
    freopen("besmegeniai-vyr.out", "w", stdout);
    printf("%lld\n", ats);
}

int main()
{
    Skaityti();
    Spresti();
    Spausdinti();
    return 0;
}
