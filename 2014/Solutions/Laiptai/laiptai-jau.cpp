/**
 * Sprendimo idėja:
 *
 * Tarkime, kad jau žinome, jog Aloyzas gali pakilti iki aukšto X. Vadinasi,
 * bet kur tarp aukštų 1 .. X jis gali persėsti į liftą, kuris ten važiuoja,
 * t.y. į bet kurį liftą, kuris važiuoja žemiau arba iki X.
 *
 * Dabar išsirikiuokime liftus pagal žemiausią aukštą, iki kurio jie važiuoja.
 * Nagrinėdami juos iš eilės, įsimename iki kur gali pakilti Aloyzas, ir vis
 * tikriname, ar jis gali pasiekti sekantį liftą sąraše. Jei negali -- čia
 * sustojame, nes nepasieks ir jokių tolimesnių liftų.
 *
 * Patogumo dėlei, sprendime naudosime standartinį C++ tipą "pair", kuris turi
 * apibrėžtą palyginimo operatorių, tad gali būti lengvai panaudotas rikiuojant.
 *
 * Autorius: Linas Petrauskas
 */

#include <algorithm>
#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

int main() {
    // Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis
    // iš failo.
    // freopen ("laiptai-jau.in", "r", stdin);
    // freopen ("laiptai-jau.out", "w", stdout);
    
    int N;  // Liftų skaičius.
    int H;  // Namo aukštis (nenaudojamas).
    
    cin >> N >> H;

    vector<pair<int, int> > liftai(N);  // Litfų aprašymai (aukštas - aukštas); 
    for (int i = 0; i < N; i++) {
        cin >> liftai[i].first >> liftai[i].second;
    }
    
    int X = 1;  // Aukščiausias aukštas, iki kurio gali pakilti Aloyzas.
    
    sort(liftai.begin(), liftai.end());
    
    for (int i = 0; i < N; i++) {
        if (X >= liftai[i].first) {
            X  = max(X, liftai[i].second);
        } else {
            // Čia Aloyzui teks sustoti.
            break;
        }
    }
    
    cout << X << endl;
    
    return 0;
}