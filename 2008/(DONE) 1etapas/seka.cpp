#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void checkNumber(int n, int NUMBER, int &counter){
        while(n > 0){
            if(n % 10 == NUMBER) counter++;
            n /= 10;
        }
    }

 int main(){

    ifstream fi("skaitm.in");
    int NUMBER, TIMES; fi >> NUMBER >> TIMES;
    fi.close();
    int i = 1, counter = 0;
    while(true){
        checkNumber(i, NUMBER, counter);
        if(counter == TIMES) break;
        i++;
    }

    ofstream fo("skaitm.out");
    fo << i;
    fo.close();

 return 0; }
