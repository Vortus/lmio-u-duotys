/**
 * Sprendimo idėja:
 *
 * Iš užduotyje pateiktos griuvimo sąlygos galima pastebėti, kad senį
 * sudarys lygiai tiek dalių, kiek yra skirtingų spindulių. Esant
 * kelioms dalims su vienodu spinduliu reikia panaudoti tą, kurios
 * aukštis didžiausias.
 *
 * Tad kiekvienam galimam spinduliui (jų yra 10000) priskiriame didžiausią
 * pamatytą aukštį. Jei koks nors spindulys yra nepanaudotas, laikome, kad
 * jam priskirtas aukštis 0. Galutinis atsakymas bus šių aukščių suma.
 *
 * Autorius: Artūras Lapinskas
 */

#include      <algorithm>
#include      <iostream>
#include      <cassert>
using namespace std;

const int MAKS_SPINDULYS = 10000;

// Sukuriame masyvą, kuris kiekvienam spinduliui priskirs aukščiausios
// dalies aukštį.
// Kadangi masyvas apibrėžtas globaliai, jo papildomai išvalyti nereikia.
int geriausi_auksciai[MAKS_SPINDULYS + 1];

int main(int argc, char **argv) {
	// Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis
	// iš failo.
	// freopen("sniego-senis-jau.in", "r", stdin);
	// freopen("sniego-senis-jau.out", "w", stdout);

	// Nusiskaitome duomenis.
	int cilindru_kiekis;
	cin >> cilindru_kiekis;

	for (int i = 0; i < cilindru_kiekis; i += 1) {
		int aukstis, spindulys;
		cin >> aukstis >> spindulys;
		// Pačių duomenų saugoti nereikia, kadangi galutiniam atsakymui
		// svarbūs tik cilindrai kurių aukštis didžiausias.
		geriausi_auksciai[spindulys] = max(geriausi_auksciai[spindulys], aukstis);
	}

	// Randame galutinį atsakymą – tai didžiausių aukščių kiekvienam
	// spinduliui suma.
	int ruzultatas = 0;
	for (int i = 1; i <= MAKS_SPINDULYS; i += 1) {
		ruzultatas += geriausi_auksciai[i];
	}

	// Atspausdiname rezultatus.
	cout << ruzultatas << endl;

	return 0;
}
