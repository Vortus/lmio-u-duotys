#include <algorithm>
#include <vector>
using namespace std;

int kiekItikins(int N, int E0, int U[], int E[])
{
	int i, j, K = 0;
	int EE = E0;
	vector< pair<int, int> > geri, blogi;
	for (i = 0; i<N; i++)
		if (E[i] >= U[i])
			geri.push_back(make_pair(U[i], E[i]));
		else
			blogi.push_back(make_pair(E[i], U[i]));
	sort(geri.begin(), geri.end());
	for (i = 0; i < geri.size(); i++)
		if (geri[i].first <= EE)
		{
			EE += geri[i].second - geri[i].first;
			K++;
		}
	sort(blogi.begin(), blogi.end());
	vector<int> kE, kE2;
	kE.push_back(EE);
	for (i = blogi.size() - 1; i >= 0; i--)
	{
		kE2 = kE;
		for (j = 0; j < kE2.size()-1; j++)
			if (kE2[j] >= blogi[i].second)
				kE[j+1] = max(kE[j+1], kE2[j] - blogi[i].second + blogi[i].first);
		if (kE2[j] >= blogi[i].second)
			kE.push_back(kE2[j] - blogi[i].second + blogi[i].first);
	}
	return K + kE.size()-1;
}