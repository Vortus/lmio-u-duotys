#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#define MAXMN 100100
using namespace std;

    struct Line{
        int x1, y1, x2, y2;
        bool operator <(const Line &l) const{ // sortui
            return max(y1, y2) > max(l.y1, l.y2);
        }
    };

    int M, N, i; // juostu skaicius, verzliu skaicius
    vector<int> throwPositions(MAXMN); //metimu vietos
    vector<Line> transporters(MAXMN); // perkelejai

    void readFile(string p){
        ifstream fi(p); fi >> M >> N;
        for(i = 0; i < M; i++)
            fi >> transporters[i].x1 >> transporters[i].y1 >>
                transporters[i].x2 >> transporters[i].y2;
        for(i = 0; i < N; i++)
            fi >> throwPositions[i];
        fi.close();
        sort(transporters.begin(), transporters.begin() + M);
    }

 int main(){

    readFile("verzles.txt");

 return 0; }
