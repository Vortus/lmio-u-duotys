 /**
 * Sprendimo aprašymas:
 *
 * Uždavinys sprendžiamas algoritmu panašiu į Paiešką platyn (BFS):
 *
 * - Kiekvienai žemėlapio pozicijai saugoma, koks geriausias kelias iki jos yra
 *   rastas (saugomas gyvybių skaičius + ėjimų skaičius). Iš pradžių tai žinoma 
 *   tik pradinei pozicijai (K gyvybių + 0 ėjimų).
 *
 * - Nagrinėjama pozicijų žemėlapyje eilė, kuri iš pradžių susideda tik iš
 *   pradinės žaidėjo pozicijos.
 *
 * - Iš kiekvienos nagrinėjamos pozicijos bandomos keturios įmanomos kryptys.
 *   Patikrinamos sienos, spąstai. Jeigu į naują poziciją rastas geresnis kelias
 *   už kol kas žinomą -- ši pozicija įtraukiama į eilės galą tolimesniam
 *   nagrinėjimui.
 *
 * - Jei nagrinėjama pozicija yra labirinto krašte, tai sprendimas įsimenamas.
 *
 * - Paieška nutraukiama, kai eilėje nebėra jokių pozicijų. Geriausias rastas
 *   sprendimas išspausdinamas.
 * 
 * Taigi skirtumas toks, kad šiuo atveju į tą patį labirinto langelį galima
 * sugrįžti kelis kartus, jeigu randamas geresnis kelias (kadangi geresnis
 * nelygu trumpesnis šiame uždavinyje).
 *
 * Kad įsitikinti sprendimo teisingumu, pakanka įsitikinti šiomis savybėmis:
 * 1. Algoritmas ras trumpiausią išėjimą su K gyvybių (jei toks yra).
 * 2. Ten, kur nepavys nueiti su K gyvybių, bus ieškoma su K - 1 gyvybių.
 * 3. Ten, kur nepavys nueiti su K - 1 gyvybių, bus ieškoma su K - 2 gyvybių.
 * Ir taip toliau.
 *
 * Autorius: Linas Petrauskas
 */

#include <cstdio>
#include <utility>
#include <vector>

using namespace std;

const int MAXN = 1000; // Maksimali kraštinė.
const int MAXK = 10;   // Maksimalus gyvybių skaičius.

// Būsena aprašoma kaip gyvybių ir atstumo pora, tačiau patogumo dėlei
// apibrėžiame ir palyginimo operatorių.
//
// Pastebėkite:
//   (3 gybybės, 5 ėjimai) > (2 gyvybės, 3 ėjimai)
//   (3 gyvybės, 5 ėjimai) > (3 gyvybės, 7 ėjimai)
//   (0 gyvybių, 5 ėjimai) < (0 gyvybių, 0 ėjimų)
struct State {
    State() : lives(0), distance(0) {}
    State(int lives, int distance) : lives(lives), distance(distance) {}

    bool operator<(const State& other) const {
        if (lives != other.lives)
            return lives < other.lives;
        return distance > other.distance;
    }

    int lives;     // Likusių gyvybių kiekis.
    int distance;  // Nueitas kelias.
};

// Pozicija aprašoma kaip dviejų sveikųjų skaičių pora (x ir y).
struct Position {
    Position() : x(0), y(0) {}
    Position(int x, int y) : x(x), y(y) {}
    int x;
    int y;
};

// Pagalbinė struktūra saugoti labirinto žemėlapiui ir patikrinti įvairioms
// savybėms.
struct Maze {
    Maze(int width, int height) : width(width), height(height) {}

    void read_from_input() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                scanf("%c", &map[y][x]);
                if (map[y][x] == 'P') {
                    start = Position(x, y);
                }
            }
            scanf("\n");
        }
    }

    bool is_border(const Position& p) const {
        return (p.y == 0 || p.y == height - 1 || p.x == 0 || p.x == width - 1);
    }

    bool is_wall(const Position& p) const {
        return map[p.y][p.x] == '#';
    }

    bool is_trap(const Position& p) const {
        return map[p.y][p.x] == '!';
    }

    Position start;

  private:
    int width;
    int height;
    char map[MAXN][MAXN];
};

State best_state[MAXN][MAXN];

int main() {
    freopen("spastai-vyr.in", "r", stdin);
    freopen("spastai-vyr.out", "w", stdout);

    int N, M, K;
    scanf("%d%d%d\n", &N, &M, &K);

    Maze maze(N, M);
    maze.read_from_input();

    best_state[maze.start.x][maze.start.y] = State(K, 0);

    vector<Position> queue;
    queue.push_back(maze.start);

    // Geriausio kelio paieška.
    State answer;
    for (int q = 0; q < queue.size(); q++) {
        const Position p = queue[q];
        if (maze.is_border(p)) {
            if (answer < best_state[p.x][p.y]) {
                answer = best_state[p.x][p.y];
            }
        } else {
            for (int dx = -1; dx <= 1; dx++)
                for (int dy = -1; dy <= 1; dy++)
                    if ((dx == 0) ^ (dy == 0)) {
                        Position new_p = Position(p.x + dx, p.y + dy);
                        if (maze.is_wall(new_p))
                            continue;
                        State& s = best_state[p.x][p.y];
                        State new_state = State(s.lives, s.distance + 1);
                        if (maze.is_trap(new_p))
                            new_state.lives--;
                        if (best_state[new_p.x][new_p.y] < new_state) {
                            best_state[new_p.x][new_p.y] = new_state;
                            queue.push_back(new_p);
                        }
                    }
        }
    }

    printf("%d %d\n", answer.lives, answer.distance);

    return 0;
}
