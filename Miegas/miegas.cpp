#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int timer[4], newTimer[4];
    int buttonC;
    int buttons[10];

    void readFile(){
        ifstream fi("miegas.in");
        for(int i = 0; i < 4; i++)
            fi >> timer[i];
        fi >> buttonC;
        for(int i = 0; i < buttonC; i++)
            fi >> buttons[i];
        fi.close();
    }

    void subtractMinute(int &mins){
        int dayms = 24 * 60;
        mins--;
        if(mins < 0) mins += dayms;
    }

    void fillminArray(int mins){
        int m = mins % 60;
        newTimer[3] = m % 10;
        m /= 10;
        newTimer[2] = m;
        int h = mins / 60;
        newTimer[1] = h % 10;
        h /= 10;
        newTimer[0] = h;
    }

    bool exists(int n){
        for(int i = 0; i < buttonC; i++)
            if(buttons[i] == n) return true;
        return false;
    }

 int main(){

    readFile();
    int allm = (timer[0] * 10 + timer[1]) * 60 + // val
    (timer[2] * 10 + timer[3]); // min

    while(true){
        fillminArray(allm);

        bool b = true;

        for(int i = 0; i < 4; i++){
           if(!exists(newTimer[i])){ b = false; break;}
        }

        if(b) break;
        else subtractMinute(allm);
    }

    ofstream fo("miegas.out");
    for(int i = 0; i < 4; i++)
        fo << newTimer[i] << " ";
    fo.close();


 return 0; }
