// 25-oji Lietuvos mokinių informatikos olimpiada.
// 2014 m. Kovo 27-30 d.
//
// Uždavinys "Varlės".
//

/**
 * Sprendimo idėja:
 *
 * Šiame uždavinyje galima pastebėti du dalykus:
 *
 *   1. Varlės yra vienoje krūvoje, jei tarp jų nėra tarpų;
 *   2. Mažinti tarpų kiekį galime tik judindami šonines varles į būrio „vidų“ (po
 *      kiekvieno tokio pajudinimo tarpų kiekis sumažėja vienetu).
 *
 * Žinant šią informaciją, nesunku pastebėti, kad uždavinio atsakymas bus bendras
 * tarpų kiekis.
 *
 * Autorius: Artūras Lapinskas
 */

#include "varles.h"
#include <algorithm>

using namespace std;

int minimalusSuoliuSkaicius(int n, int pozicijos[])
{
    // Randame varles stovinčias grupės šonuose
    const int pirmaVarle = *min_element(pozicijos, pozicijos + n);
    const int paskutineVarle = *max_element(pozicijos, pozicijos + n);
    // Apskaičiuojame tarpų kiekį
    return paskutineVarle - pirmaVarle - n + 1;
}
