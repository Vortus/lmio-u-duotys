#include <cstdio>
#include <iostream>
#include <queue>
using namespace std;
typedef pair <int, int> pii;
#define y first
#define x second

const int MAXN = 3000 + 5;      //Maksimalus eilučių skaičius
const int MAXM = 3000 + 5;      //Maksimalus stulpelių skaičius
const int MAXD = 4;             //Ėjimų krypčių skaičius
const pii dir[] = {pii(0, -1), pii(1, 0), pii(0, 1), pii(-1, 0)};       //Ėjimai į kairę, žemyn, į dešinę, į viršų

int D, T, N, M;
char g[MAXN][MAXM];             //Lino kambario schema
int lnDst[MAXN][MAXM];          //Lino kelionės trukmė skaičiuojant nuo šaldytuvo
int ktDst[MAXN][MAXM];          //Katino kelionės trukmė skaičiuojant nuo šaldytuvo

//Ar galima eiti į langelį (c.y, c.x)?
inline bool isValid(pii c) {
    return (c.y >= 0 && c.x >= 0 && c.y < N && c.x < M && g[c.y][c.x] != '#');
}

//Simuliuojame katino ėjimus
pii simKatinas(pii st, int dst[MAXN][MAXM]) {
    dst[st.y][st.x] = 1;
    int i = 0;
    do {
        pii n = pii(st.y + dir[i].y, st.x + dir[i].x);      //Katinas bando paeiti į (n.y, n.x)
        while (!isValid(n)) {                               //Kol paeiti negali, katinas sukasi
            i = (i + 1) % MAXD;
            n = pii(st.y + dir[i].y, st.x + dir[i].x);
        }
        dst[n.y][n.x] = dst[st.y][st.x] + 1;
        st = n;
    }
    while (g[st.y][st.x] != 'P');                           //Einame iki spintos
    return st;
}

//Linas ieško katino - paieškos į plotį algoritmas
void bfsLinas(pii st, int dst[MAXN][MAXM]) {
    queue <pii> qs;
    qs.push(st);
    dst[st.y][st.x] = 1;
    while (!qs.empty()) {
        st = qs.front();
        qs.pop();
        for (int i = 0; i < MAXD; i++) {
            pii n = pii(st.y + dir[i].y, st.x + dir[i].x);
            if (isValid(n) && dst[n.y][n.x] == 0) {
                dst[n.y][n.x] = dst[st.y][st.x] + 1;
                qs.push(n);
            }
        }
    }
}

int main() {
    //Skaitome duomenis, pažymime, kur šaldytuvas
    freopen("katinas-vyr.in", "r", stdin);
    freopen("katinas-vyr.out", "w", stdout);
    cin >> D >> T >> N >> M;
    pii sal = pii(0, 0);
    for (int i = 0; i < N; i++) {
        cin >> g[i];
        for (int j = 0; j < M; j++)
            if (g[i][j] == 'S')
                sal = pii(i, j);
    }

    //Skaičiuojame Lino ir katino kelionių trukmes
    bfsLinas(sal, lnDst);
    pii sp = simKatinas(sal, ktDst);

    //Skaičiuojame ir spausdiname atsakymą
    int ans = max(0, D - max(0, T + lnDst[sp.y][sp.x] - ktDst[sp.y][sp.x]));
    cout << ans << '\n';
    return 0;
}