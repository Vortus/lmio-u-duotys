#include <algorithm>
#include <cstdio>
#define rep(i,n) for(i=0;i<n;++i)
#define maxN 1001
using namespace std;

int u[maxN][maxN], i, j, k, g[maxN], v[maxN], ok, N;

int main(){
    freopen("kinas-vyr.in", "r", stdin);
    freopen("kinas-vyr.out", "w", stdout);
    ok=1;
    scanf("%i", &N);
    rep(i,N) g[i]=0;
    rep(j,N) if (ok){
        rep(i,N) scanf("%i", &u[j][i]);
        sort(&u[j][0], &u[j][N]);
        k=N-1;
        for (i=N-1; i>=0; --i) {
            if (u[j][k]&&u[j][k]<g[i]) v[i]=0;
            else {
                v[i]=u[j][k];
                g[i]=v[i];
                --k;
            }
        }
        if (k>=0&&u[j][k]) ok=0;
        rep(i,N){
            u[j][i]=v[i];
            g[i]=max(g[i],v[i])-j-2;
        }
    }
    if (ok){
        printf ("TAIP\n");
        rep(j,N) {
            rep(i,N-1) printf("%i ", u[j][i]);
            printf ("%i\n", u[j][N-1]);
        }
    } else printf ("NE\n");
}
