/*
TASK:BURTAI
LANG:C++
*/
#include <cstdio>
#include <queue>
#include <algorithm>
#include <map>
using namespace std;

#define MAXN 1010
#define NL 26
#define SZ (MAXN + NL + 2)

#define SOURCE 0
#define SINK 1
#define LETTER(x) ((x)+2)
#define NAME(x) ((x)+NL+2)

typedef map<int, int> mii;

int n;
mii edge[SZ];

void read() {
	char buf[MAXN];
	scanf("%s", buf);
	for (n = 0; buf[n]; ++n)
		++edge[SOURCE][LETTER(buf[n]-'A')];
	for (int i = 0; i < n; ++i) {
		scanf(" %s", buf);
		for (int j = 0; buf[j]; ++j)
			++edge[LETTER(buf[j]-'A')][NAME(i)];
		++edge[NAME(i)][SINK];
	}
}

bool path() {
	int from[SZ];
	fill(from, from+SZ, -1);
	from[SOURCE] = SOURCE;
	queue<int> q;
	q.push(SOURCE);
	while (from[SINK] < 0 && !q.empty()) {
		int x = q.front(); q.pop();
		for (mii::iterator it = edge[x].begin(); it != edge[x].end(); ++it)
			if (from[it->first] < 0) {
				from[it->first] = x;
				q.push(it->first);
			}
	}
	if (from[SINK] < 0)
		return 0;
	for (int t = SINK; t != SOURCE; t = from[t]) {
		++edge[t][from[t]];
		if (--edge[from[t]][t] == 0)
			edge[from[t]].erase(t);
	}
	return 1;
}

int maxflow() {
	int c = 0;
	while (path())
		++c;
	return c;
}

int main() {
	freopen("burtai.in", "r", stdin);
	freopen("burtai.out", "w", stdout);
	read();
	printf("%d\n", maxflow());
}
