#include <stdio.h>

    typedef struct pozicija  { int x, y, v, p; } pozicija;
    typedef struct vektorius { int x, y; } vektorius;

    const int AKUTES[7][4] = {  
        { 0, 0, 0, 0 }, 
        { 2, 4, 5, 3 }, { 1, 3, 6, 4 }, { 1, 5, 6, 2 },
        { 1, 2, 6, 5 }, { 1, 4, 6, 3 }, { 2, 3, 5, 4 } };

pozicija paritus(pozicija sena, int ejimo_nr)
{
    const vektorius EJIMAI[4] = {
        { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 0 } };

    pozicija nauja;
    int k = 0;

    nauja.x = sena.x + EJIMAI[ejimo_nr].x;
    nauja.y = sena.y + EJIMAI[ejimo_nr].y;
    
    while (AKUTES[sena.v][k] != sena.p) k++;
    nauja.v = AKUTES[sena.v][(k + ejimo_nr) % 4];
    
    switch (ejimo_nr) {
        case 0 : nauja.p = (7 - sena.v);
             break;
        case 2 : nauja.p = sena.v;
             break;
        default: nauja.p = sena.p;
    }
    return nauja;
}
    
#define MAXDYDIS  110

    int skaiciai[MAXDYDIS][MAXDYDIS];
    int atstumas[MAXDYDIS][MAXDYDIS][7];

    pozicija eile[MAXDYDIS * MAXDYDIS * 6];

void ieskok(pozicija p)
{
    int k, pradzia = 0, pabaiga = 0;
    
    eile[pabaiga++] = p;
    atstumas[p.x][p.y][p.p] = 0;

    while (pradzia < pabaiga) {
        pozicija d = eile[pradzia++];
        for (k = 0; k < 4; k++) {
            pozicija t = paritus(d, k);
            if (skaiciai[t.x][t.y] == t.v)
                if (atstumas[t.x][t.y][t.p] > atstumas[d.x][d.y][d.p] + 1) {
                    atstumas[t.x][t.y][t.p] = atstumas[d.x][d.y][d.p] + 1;
                    eile[pabaiga++] = t;
                }
        }       
    }
}

int main(void)
{
    int n, m, i, j, k, ats;
    pozicija a, b;

    freopen("KAULIUKAS.IN", "r", stdin);
    freopen("KAULIUKAS.OUT", "w", stdout);

    scanf("%d%d", &n, &m);
    scanf("%d%d%d%d", &a.x, &a.y, &b.x, &b.y);

    for (j = 1; j <= m; j++)
        for (i = 1; i <= n; i++)
            scanf("%d", &skaiciai[i][j]);

#define BEGALINIS 1000000000
    
    for (i = 1; i <= n; i++)
        for (j = 1; j <= m; j++)
            for (k = 1; k <= 6; k++)
                atstumas[i][j][k] = BEGALINIS;
    
    for (i = 1; i <= 6; i++) {
        a.v = i;
        if (skaiciai[a.x][a.y] == a.v)
            for (j = 0; j < 4; j++) {
                a.p = AKUTES[i][j];
                ieskok(a);
            }
    }

    ats = BEGALINIS;
    for (b.p = 1; b.p <= 6; b.p++)
        if (ats > atstumas[b.x][b.y][b.p])
            ats = atstumas[b.x][b.y][b.p];
    printf("%d\n", ats);
    
    return 0;
}

