#include <algorithm>
#include <cstdio>
#include <vector>
#include <cassert>

using namespace std;

#define FIN "melagis-jau.in"
#define FOUT "melagis-jau.out"
#define MAXN 5000
#define SK_K 10000

int spej[MAXN][10]; 
int skaiciai[SK_K][4]; 
int plius[MAXN], minus[MAXN]; 

//Surašykime į duomenu struktūrą visus galimus mėnulio sugalvotus skaičius
int galimi() {
	int i = 0;
	for (int x1 = 1; x1 <= 9; x1++) { 
	for (int x2 = x1 + 1; x2 <= 9; x2++) { 
	for (int x3 = x2 + 1; x3 <= 9; x3++) { 
	for (int x4 = x3 + 1; x4 <= 9; x4++) { 
		int sk[] = {x1, x2, x3, x4};
		do {
			for (int j = 0; j < 4; j++)
				skaiciai[i][j] = sk[j];
			i++;	
		} while (next_permutation(sk, sk + 4));
	}	
	}
	}
	}
	assert(i == 9 * 8 * 7 * 6);
	return i;
}

int check(int dyd, int n) {
	int failAt = -1, p, m;//nulis, jei nėra prieštaravimų
	//bando visus skaičius, kuriuos mėnulis galėjo sugalvoti
	bool failed;
	for (int x = 0; x < dyd; x++) {
		//su visais spėjimais, kurie buvo atlikti
		failed = false;
		for (int i = 0; i < n; i++) {
			//testuoja spėjimą
			p = m = 0;
			for (int z = 0; z < 4; z++) {
				if (spej[i][skaiciai[x][z]] != -1) {
					if (spej[i][skaiciai[x][z]] == z) p++;
					else m++;
				} 
			}
			if (plius[i] != p || minus[i] != m) {
				failed = true;
				//jei spėjimas prieštarauja prielaidai, kad mėnulis naudojo šį skaičių 
				if (failAt == -1 || failAt < i + 1) {
					failAt = i + 1;
				}
				break;
			}
		}
		if (!failed) {
			return 0;
		}	
	}
	return failAt;	
}

int main() {
	freopen(FIN, "r", stdin);
	freopen(FOUT, "w", stdout);
	int n;
	scanf("%d", &n);	
	int sp, p, m;
	//skaitome spėjimus iš failo
	for (int i = 0; i < n; i++) {
		scanf("%d %d %d\n", &sp, &p, &m);
		//išskaidome spėjamą skaičių skaitmenimis
		fill(spej[i], spej[i] + 10, -1);
		for (int j = 3; j >= 0; j--) {
			spej[i][sp % 10] = j;
			sp /= 10;
		}
		plius[i] = p;
		minus[i] = m;		
	}
	int dyd = galimi();
	printf("%d\n", check(dyd, n));
	return 0;
}
