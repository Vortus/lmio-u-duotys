{
TASK: KLAUSIMYNAS
LANG: PASCAL
}
program test;

  const
    MAX_N = 10002;
    DUOM_BYLA = 'klausimynas.in';
    ATS_BYLA = 'klausimynas.out';
	
  type
    persokimas = record
      a, { klausimas, pagal kurį yra formuojama sąlyga }
      b, { klausimus nuo b iki c reikia praleisti, jei sąlyga patenkinta }
      c : integer;
    end;
	
  var
    m: integer;
    persokimai: array [1 .. MAX_N] of persokimas;
    
  function dubliuojasi(nr: integer): boolean;
  var
    i: integer;
  begin
    for i := 1 to m do
      if (nr <> i) and (persokimai[i].a = persokimai[nr].a) and
        (persokimai[i].b = persokimai[nr].b) and 
        (persokimai[i].c = persokimai[nr].c) then
      begin
        dubliuojasi := true; exit;
      end;
    dubliuojasi := false;
  end;
    
  procedure itraukti_klausima(klausimoNr: integer);
  var
    i: integer;
  begin
    for i := 1 to m do
      with persokimai[i] do
      begin
        if klausimoNr < a then
        begin
          inc(a); inc(b); inc(c);
        end else begin
          { Kadangi pirma sąlyga nepatenkinta, esu tikras, kad:
           klausimoNr >= a }
          if klausimoNr < b then
          begin
            inc(b); inc(c);
          end;
        end;
      end;
    
    { Skylančiu peršokimus nagrinėjam atskiru ciklu.
    Taip daroma todėl, kad naujai susidariusieji peršokimai gali 
    dubliuotis su jau egzistuojančiais ir tai reikės tikrinti }
    i := 1;
    while i <= m do
    begin
      with persokimai[i] do
      begin
        if (klausimoNr >= b) and (klausimoNr < c) then
        begin
          { b <= klausimoNr < c: peršokimą reikia perskelti į du }
              
          inc(m);
          persokimai[m].a := a;
          persokimai[m].b := klausimoNr + 2;
          persokimai[m].c := c + 1;
          if dubliuojasi(m) then dec(m);
              
          c := klausimoNr;
          if dubliuojasi(i) then
          begin
            persokimai[i] := persokimai[m];
            dec(i); dec(m);
          end;
              
        end;
      end;
      inc(i);
    end;
  end;

  procedure sprendimas;
  var
    i, klausimoNr,
    k: integer;
    f: text;
  begin
    assign(f, DUOM_BYLA); reset(f);
    readln(f, i, m, k);
    for i := 1 to m do
      with persokimai[i] do
        readln(f, a, b, c);
    
    for i := 1 to k do
    begin
      readln(f, klausimoNr);
      itraukti_klausima(klausimoNr);
    end;
    close(f);
  end;
    
  procedure atsakymas;
  var
    f: text;
  begin	  
    assign(f, ATS_BYLA); rewrite(f);
    writeln(f, m);
    close(f);
  end;

begin
   sprendimas;
   atsakymas;
end.
