#include <iostream>
#include <fstream>

using namespace std;

int M, N[100000];

void ReadFile(const char *file_name) {
    ifstream GET(file_name);
        GET >> M;
        for (int i = 0; i < M; i++) {
            GET >> N[i];
        }
    GET.close();
}

void WriteFile(const char *file_name) {
    ofstream PUT(file_name);
        int sk = 0;
        int remind = 0;
        for (int i = 0; i < M; i++) {
            N[M-i-1] += remind;
            remind = 0;
            if (N[M-i-1] > i) { remind = N[M-i-1] - i; N[M-i-1] = 0; }
            sk++;
        }
        if (remind != 0) {
            sk++;
        }

        cout << sk << endl;
        if (remind != 0) {
            cout << remind << endl;
        }
        for (int i = 0; i < M; i++) {
            cout << N[i] << endl;
        }
    PUT.close();
}

int main()
{
    ReadFile("faktorialai-jau.in");
    N[M-2] += 1;
    WriteFile("faktorialai-jau.out");
    return 0;
}
