#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

    int N;
    int arr[1000][1000];
    vector<int> numbers;

    void readFile(){
        ifstream fi("antimagiskas.in");
        fi >> N;
        for(int i = 0; i < N; i++)
            for(int j = 0; j < N; j++)
                fi >> arr[i][j];
        fi.close();
    }

    void getHorizontal(){
        for(int i = 0; i < N; i++){
            int tmp = 0;
            for(int j = 0; j < N; j++)
                tmp += arr[i][j];
            numbers.push_back(tmp);
        }
    }

    void getVertical(){
        for(int i = 0; i < N; i++){
            int tmp = 0;
            for(int j = 0; j < N; j++)
                tmp += arr[j][i];
            numbers.push_back(tmp);
        }
    }

    void getDiagonal(){
        int tmp = 0, tmpt = 0;
        for(int i = 0; i < N; i++){
            tmp += arr[i][i];
            tmpt += arr[i][N - i - 1];
        }
        numbers.push_back(tmp);
        numbers.push_back(tmpt);

    }

    bool checkSequence(int &a, int &b){
        a = numbers.at(0);
        b = numbers.at(numbers.size() - 1);
        for(int i = 0; i < numbers.size() - 1; i++){
            if(numbers.at(i) + 1 != numbers.at(i + 1))
                return false;
        }
        return true;
    }

 int main(){

    readFile();

    getHorizontal();
    getVertical();
    getDiagonal();

    sort(numbers.begin(), numbers.end());

    int a, b;
    ofstream fo("antimagiskas.out");

    if(checkSequence(a, b)){
        fo << a << " " << b;
    }else fo << 0;

    fo.close();

 return 0; }
