#include <cstdio>
#include <iostream>
#include <algorithm>
#include <climits>

using namespace std;

const int MAX_BULVIU = 3001;  // Maksimalus bulvių kiekis pagal sąlygą
const int begalybe = INT_MAX; // Begalybė apibrėžiama kaip didžiausias skaičius, kurį dar gali talpinti tipas int

int main() {
    freopen("dovana.in", "r", stdin);
    freopen("dovana.out", "w", stdout);
    
    int M, N;                 // M - visų turimų bulvių kiekis, N - atrinktinų bulvių kiekis
    int bulves[MAX_BULVIU];   // Masyve bulves saugosime bulvių svorius
    
    // Nuskaitome duomenis
    cin >> M >> N;
    for (int i = 0; i < M; i++)
        cin >> bulves[i];
    
    // Rikiuojame naudodami standartinės C++ bibliotekos funkciją sort
    sort(bulves, bulves + M);
    
    // Pakanka išbandyti visus iš eilės einančius bulvių rinkinius (pagal įrodymą detaliame sprendimo aprašyme):
    int skirtumas = INT_MAX;
    int pradzia = 0;
    for(int i = 0; i + (N-1) < M; i++)
      if( bulves[i + N - 1] - bulves[i] < skirtumas )   // Jei sunkiausios ir lengviausios bulvių skirtumas mažesnis už jau rastą,
        skirtumas = bulves[i + N - 1] - bulves[i],      // išsisaugome ir šį skirtumą, ir paties rinkinio vietą.
        pradzia = i;
    
    
    // Kai jau žinome, kur prasideda posekis, išvedame jį.
    for(int i = 0; i < N; i++)
      cout << bulves[pradzia + i] << " ";
    cout << endl;
    
    return 0;
}
