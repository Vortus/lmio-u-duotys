// 25-oji Lietuvos mokinių informatikos olimpiada.
// 2014 m. Kovo 27-30 d.
//
// Uždavinys "Ūkininkas"

/**
 * Sprendimo idėja:
 *
 * Šiame uždavinyje reikia rasti kiek vandens bus išpilta iš paskutinio
 * kibirėlio. Maksimaliai kibirėliuose gali tilpti:
 *
 * (T^1 - T^0) + (T^2 - T^1) + ... + (T^N - T^(N - 1))
 * (T^1 + T^2 + ... + T^N) - (T^0 + T^1 + ... + T^(N - 1))
 * ((T^(N + 1) - 1) / (T - 1) - 1) - (T^N - 1) / (T - 1)
 * (T^(N + 1) - T^N - 1 + 1) / (T - 1) - 1
 * (T^N * (T - 1)) / (T - 1) - 1
 * T^N - 1
 *
 * Tad, jei paimsime V mod T^N gausime kiek vandens yra kibirėliuose
 * supylus V vandens. Visas vanduo, kuris neišliko kibirėliuose, buvo
 * išpiltas.
 *
 * Autorius: Artūras Lapinskas
 */

#include "ukininkas.h"

long long ispiltasVanduo(int N, int T, long long V)
{
    // Randame T^N
    long long p = 1;
    for (int i = 0; i < N; i += 1)
    {
        p *= T;
    }
    // Apskaičiuojame likutį
    return V - V % p;
}
