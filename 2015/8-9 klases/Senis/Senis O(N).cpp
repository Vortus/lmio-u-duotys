#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;
    #define MAXR 10010

    vector<int> buffer(MAXR, -1);
    int N, i, a, b, maxrr = -1;

    void readFile(){
        ifstream fi("senis.in"); fi >> N;
        for(i = 0; i < N; i++){
            fi >> a >> b; buffer[b] = max(buffer[b], a);
            if(b > maxrr) maxrr = b;
        }
        fi.close();
    }

 int main(){

    readFile(); int sum = 0;
    for(i = 0; i <= maxrr; i++)
        if(buffer[i] != -1) sum += buffer[i];
    cout << sum;

 return 0; }
