#include "gyvenimas.h"
#include <cstdio>
using namespace std;

const int MAX_N = 100;
const int MAX_M = 100;
int tl[MAX_N][MAX_M];
int lenta[MAX_N][MAX_M];

int gyviKaimynai(int N, int M, int i, int j, int v[MAX_N][MAX_M]) {
	//Grąžina, kiek gyvų kaimynų turi langelis (i, j)
	int cnt = 0;
	for (int di = -1; di <= 1; di++)
		for (int dj = -1; dj <= 1; dj++)
			if (di != 0 || dj != 0) {
				int ki = i + di, kj = j + dj;
				cnt += (ki >= 0 && ki < N && kj >= 0 && kj < M && v[ki][kj] == 1);
			}
	return cnt;
}

int gyvenimoZaidimas(int N, int M, int C) {
	//Užpildom lenta pradinėmis būsenomis
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			lenta[i][j] = busena(i + 1, j + 1);

	//Simuliuojam gyvenimą
	for (int c = 0; c < C; c++) {
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++) {
				int gk = gyviKaimynai(N, M, i, j, lenta);
				if (lenta[i][j] == 0 && gk == 3) tl[i][j] = 1;
				else if (lenta[i][j] == 1 && (gk < 2 || gk > 3)) tl[i][j] = 0;
				else tl[i][j] = lenta[i][j];
			}
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				lenta[i][j] = tl[i][j];
	}

	//Skaičiuojam gyvus langelius
	int ans = 0;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			ans += lenta[i][j];
	return ans;
}