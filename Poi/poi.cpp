#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Participant{
        bool filip;
        int points, ID, done;
    } participants[2002];

    int exercisePoints[2002];
    int participantsC, exercises, filipID;
    int board[2002][2002];

    void sortByID(Participant &a, Participant &b){
        if(a.ID > b.ID) swap(a, b);
    }

    void sortByDone(Participant &a, Participant &b){
        if(a.done < b.done) swap(a, b);
        if(a.done == b.done) sortByID(a, b);
    }

    void sortByPoints(){
        for(int i = 0; i < participantsC; i++){
            for(int j = i + 1; j < participantsC; j++){
                if(participants[i].points < participants[j].points){ // jei taskai
                    swap(participants[i], participants[j]);
                }else if(participants[i].points == participants[j].points){ // jei reikia pagal atlikta
                 sortByDone(participants[i], participants[j]);
                }
            }
        }
    }

    void readFile(){ // failo nuskaitymas

        ifstream fi("poi.in");
        fi >> participantsC >> exercises >> filipID;

        ////ivedam boarda
        for(int i = 0; i < participantsC; i++)
            for(int j = 0; j < exercises; j++)
                fi >> board[i][j];
        ////
        fi.close();

        //// surast uzdaviniu taskus
        for(int i = 0; i < exercises; i++){
            int c = 0;
            for(int j = 0; j < participantsC; j++)
                if(board[j][i] == 0) c++;
            exercisePoints[i] = c;
        }
        /////

        ////dalyviu taskai
        for(int i = 0; i < participantsC; i++){
            int c = 0, cc = 0;
            for(int j = 0; j < exercises; j++){
                if(board[i][j] == 1){ cc++; c += exercisePoints[j]; }
            }

            participants[i].points = c;
            participants[i].ID = i;
            participants[i].done = cc;

            if(i == filipID - 1) participants[i].filip = true;
            else participants[i].filip = false;
        }
        ////

    }

 int main(){

    readFile();
    sortByPoints();

    ofstream fo("poi.out");
    for(int i = 0; i < participantsC; i++){
        if(participants[i].filip){
            fo << participants[i].points << " " << i + 1;
            break;
        }
    }
    fo.close();


 return 0; }
