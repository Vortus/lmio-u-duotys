/*
Sudėtingumas O(N(log N + log(max{y_i} + L))).
*/

#include <cstdio>
#include <algorithm>

using namespace std;

#define INFTY 1000000005
#define MAX_N 500001

typedef pair<int, int> pii;

pii jelly[MAX_N];
int N, K, L;

int possible(int Y) {
        for(int s = 0, e = 0, curr = 0; e < N; ++e) {
		while(s < N && jelly[s].first <= Y*2) {
                        if(jelly[s].second <= Y*2) ++curr;
                        ++s;
		}
		if(curr <= K) return 1;
                if(jelly[e].second <= Y*2 && jelly[e].first <= L-Y*2) --curr;
                while(s < N && jelly[s].first <= jelly[e].first+Y*4-1) {
                        if(jelly[s].second <= Y*2) ++curr;
                        ++s;
                }
                if(curr <= K) return 1;
        }
        return 0;
}

int main() {
	freopen("besmegeniai-vyr.in", "r", stdin);
	freopen("besmegeniai-vyr.out", "w", stdout);
        scanf("%d%d%d", &L, &N, &K);
        for(int i = 0; i < N; ++i)
                scanf("%d%d", &jelly[i].first, &jelly[i].second);
        sort(jelly, jelly+N);
        int l = 0, h = INFTY;
        while(l < h) {
                int c = (h+l)/2+1;
                if(possible(c)) l = c;
                else h = c-1;
        }
        printf("%d\n", h);
        return 0;
}

