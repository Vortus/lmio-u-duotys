#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int toMins(int h, int m){
        return h * 60 + m;
    }

    int getInterval(int sh, int sm, int eh, int em,
                    int sx, int sy, int ex, int ey){
        int a = toMins(sh, sm);
        int b = toMins(sx, sy);
        int aa = a > b ? a : b;
        int c = toMins(eh, em);
        if(b >= a && b >= c) return 0;
        int d = toMins(ex, ey);
        int bb = c < d ? c : d;
        return bb - aa;
    }

 int main(){

    int sh, sm, eh, em, N;
    ifstream fi("internetas.in");
    fi >> sh >> sm >> eh >> em >> N;
    int allMins = toMins(eh, em) - toMins(sh, sm);
    for(int i = 0; i < N; i++){
        int a, b, c, d; fi >> a >> b >> c >> d;
        allMins -= getInterval(sh, sm, eh, em, a, b, c, d);
    }
    fi.close();

    cout << allMins;

 return 0; }
