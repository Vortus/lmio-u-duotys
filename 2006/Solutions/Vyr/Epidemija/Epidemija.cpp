#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stack>
using namespace std;

    #define MAXN 1010
    #define MAXK 1010
    #define MAXP 500000

    struct Bird{
        int ID; bool explored;
        vector<int> connections;
    };

    int N, K, P; // pauksciu, uzsikretusiu pauksciu, inputas kontaktu
    vector<Bird> birds(MAXN);
    vector<int> birdGroups; // grupes

    void readFile(){
        ifstream fi("t/epidemija.g06.in");
        fi >> N >> K >> P;
        for(int i = 0; i < P; i++){
            int a, b; fi >> a >> b;
            birds[a].ID = a; birds[b].ID = b;
            birds[a].connections.push_back(b);
            birds[b].connections.push_back(a);
        }
        fi.close();
    }

    void DFS_Visit(int bird){
        if(birds[bird].explored) return;
        int visitedCount = 1;
        birds[bird].explored = true;
        stack<int> s; s.push(bird);
        while(!s.empty()){
            bird = s.top(); s.pop();
            for(int i = 0; i < birds[bird].connections.size(); i++){
                int lookto = birds[bird].connections[i]; //
                if(!birds[lookto].explored){
                    s.push(lookto);
                    birds[lookto].explored = true;
                    visitedCount++;
                }
            }
        }
        if(visitedCount != 0) birdGroups.push_back(visitedCount); // del viso pikto
    }

 int main(){

    readFile();
    //pravisitina visus
    for(int i = 1; i <= N; i++)
        DFS_Visit(i);
    ////
    int minsev = MAXP, maxsev = 0;
    sort(birdGroups.begin(), birdGroups.end());

    //maxs
    for(int i = 0; i < min(K, (int)birdGroups.size()); i++)
        maxsev += birdGroups[(int)birdGroups.size() - i - 1];
    //min
    for(int i = 0; i < birdGroups.size(); i++){
        int j = i, tmp = 0;
        while(tmp < K){
            tmp += birdGroups[j];
            j++;
        }
        if(tmp < minsev) minsev = tmp;
    }

    cout << minsev << " " << maxsev;

 return 0; }
