/**
 * Sprendimo idėja:
 *
 * Šį uždavinį galima spręsti pasinaudojus dinaminiu programavimu. T.y. masyve
 * `min_laikas[i]` laikysime informaciją apie tai per kiek minučių sugebame nušokuoti nuo pirminės
 * pozicijos iki lelijos, kurios numeris yra `i`. Šią reikšmę galima apskaičiuoti
 * pagal formulę: min_laikas[i] = musiu_kiekis[i] + min_{i - suolio_ilgis <= j < i} min_laikas[j].
 *
 * Autorius: Artūras Lapinskas
 */

#include      <algorithm>
#include      <iostream>
#include      <deque>
using namespace std;

const int MAX_N = 1000001;

// Nusako kiek musyčių sukasi virš tam tikros lelijos.
int musiu_sk[MAX_N];
// Nusako per kiek laiko varlė sugebės nušokuoti nuo pirminės pozicijos ant
// tam tikros lelijos.
long long min_laikas[MAX_N];

int main() {
	// Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis
	// iš failo.
	// freopen("varle.in", "r", stdin);
	// freopen("varle.out", "w", stdout);

	// Nusiskaitome duomenis.
	int leliju_kiekis, suolio_ilgis;
	cin >> leliju_kiekis >> suolio_ilgis;

	for (int i = 1; i < leliju_kiekis; i += 1) {
		cin >> musiu_sk[i];
	}

	// Pradinėje pozicijoje ir ant kranto (krantas modeliuojamas kaip papildoma
	// lelija) musių nėra.
	musiu_sk[0] = 0;
	musiu_sk[leliju_kiekis] = 0;

	// Šioje struktūroje laikysime griežtai didėjančią `min_laikas` reikšmių seką.
	// Vardan paprastumo saugosime ne pačias reikšmes, o jų indeksus masyve `min_laikas`.
	deque<int> didejanti_seka;

	// Mes jau esame ant lelijos, kurios indeksas 0.
	min_laikas[0] = 0;
	didejanti_seka.push_back(0);

	for (int lelija = 1; lelija <= leliju_kiekis; lelija += 1) {
		// Nušokus ant lelijos būtinai teks suvalgyti visas muses.
		min_laikas[lelija] = musiu_sk[lelija];

		// Ant šios lelijos šoksime nuo lelijos, kurią pasiekėme greičiausiai.
		min_laikas[lelija] += min_laikas[didejanti_seka.front()];

		// Įdedame `min_laikas[lelija]` reikšmę į `didejanti_seka` struktūrą.
		// Prieš įdėjimą reikia pašalinti visas reikšmes, kurios yra didesnės nei
		// `min_laikas[lelija]`, kadangi mums reikia išlaikyti didėjančią seką.
		while (!didejanti_seka.empty() && min_laikas[didejanti_seka.back()] >= min_laikas[lelija]) {
			didejanti_seka.pop_back();
		}
		didejanti_seka.push_back(lelija);

		// Mums rūpi tik paskutinės `suolio_ilgis` reikšmės.
		if (didejanti_seka.front() == lelija - suolio_ilgis) {
			didejanti_seka.pop_front();
		}
	}

	cout << min_laikas[leliju_kiekis] << endl;

	return 0;
}
