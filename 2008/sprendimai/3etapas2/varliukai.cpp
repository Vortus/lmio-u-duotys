/*
TASK: VARLIUKAI
LANG: C++
*/
 #include <bitset>
 #include <fstream>
 #include <queue>
 #include <iostream>
 
 using namespace std;
 const int MAX_N=3000;
 char grafas[MAX_N][MAX_N];
 
 char* inp;
 char* outp;
 
 
 int N = 0;
 int P = 0;
 
 bool skaityk(){
   N = 0;
   P = 0; 	 
   ifstream  fin(inp);
   fin >> N;
   if (N==1) { 
	   P=1; // Pagal salyga galima tik antra eilute 1 0
	   fin.close();
	   return false;
	   }
   memset( grafas, 0, MAX_N * MAX_N );
     
   for(int i=0; i<N; i++){
     int kiek;
	 fin >> kiek; 
	 for(int j=0; j<kiek; j++) {
	   int ka;
	   fin >> ka;
	   ka--; // numeruojame nuo 0 !
	    // cout << i << " " << ka << endl;
	   if( grafas[ka][i] == 1 ){ fin.close(); return false; } // Mato vienas kita
	      else { grafas[i][ka] = 1; grafas[ka][i]=-1; }
	 }  
   }     
   fin.close();
   return true;
 }


 void skaiciuok(){

   int svoriai[MAX_N];
   for(int i=0; i<N; i++) svoriai[i] = MAX_N;
	  
   while (true) {
      int nuo=0;
     // cout << nuo << endl;
      
	  while ( (svoriai[nuo] != MAX_N) && ( nuo < N ) ) nuo++;
	  if ( nuo == N ) break; // visos virsunes isnagrinetos
      // dirbame su jungia komponente
	  queue<int> eile;
	  if (!eile.empty()) cout << " OOOO no" << endl;
	  // Komponentes apdorojimas:
	  svoriai[nuo]=0;
	  eile.push( nuo );
               	  int did=0, maz=0;

	  while ( ! eile.empty() ){
	     
	     int nagr = eile.front();
	//	 cout << " pirmas elementas eileje " << nagr << endl;
		 for(int j=0; j<N; j++) {
		   if( grafas[nagr][j] != 0 ) {
		   		
				int naujas_svoris = svoriai[nagr] + grafas[nagr][j];
				
				if( svoriai[j] == MAX_N ) {
				   svoriai[j] = naujas_svoris; 
				   eile.push( j );  		
				}
  			    else if ( svoriai[j] != naujas_svoris) { // Nekorektiskumas
       	   	   	   P=0; 
   	   	   	   	   return;
				}
		   }//end -> if( grafas[nagr][j]...]
                       		 }//end -> for(pagal visus kaimynus]
	            if(svoriai[eile.front()] > did) did = 	svoriai[eile.front()];
   	            if  (svoriai[eile.front()] < maz) maz = svoriai[eile.front()];	    		  	  
                            eile.pop();  
      }//end -> while ( ! eile.empty() ...]
      
      // skaiciuojame poziciju skaiciu komponentei
	  
//	  for(int i=0; i<N; i++) {
//	      if ( (svoriai[i] != MAX_N)) {
  //             if  (svoriai[i] > did) did = svoriai[i];	    
//	           if  (svoriai[i] < maz) maz = svoriai[i];	    		  	     
//	  }	     
 //  }
	  int p1 = did - maz + 1;
	  if ( P < p1  ) P = p1;	  	    
 
   }// end ->    while ( yra nenagrinetu varliuku ) ...]
 
 } 

 void rasyk(){
   ofstream  fout(outp);
   fout << P;
   fout.close();
 }
 
 int main(int argc, char* argv[]){
 
   inp="varliukai.in";
   outp="varliukai.out";	   
   if ( skaityk() )   {   skaiciuok(); }
   rasyk();
  
 }

