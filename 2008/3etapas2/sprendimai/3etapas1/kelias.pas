{ Lino sprendimas uždaviniui PAPRASČIAUSIAS KELIAS. (Komentarai utf-8)
  Sprendimas iškarpymo būdu:
  Pirmiausia iškerpame /./ katalogus, o tuomet - */../,
  kur * - bet koks katalogas (gali būti tuščias).
  Iškarpymo tvarka yra svarbi. Jei pirmiausia iškirpinėtume */../
  katalogus, tai turėtume problemų su atvejais /x/./../
}

    var p, l   : integer;
        kelias : ansistring; { kad paprastai sutalpintume 255 + 1 simbolį.
                               Tas vienas yra papildomas / simbolis. }

begin
    assign(input,  'kelias.in');  reset(input);
    assign(output, 'kelias.out'); rewrite(output);
    readln(kelias);

    { dėl bendrumo pridedame papildomą brūkšnį, kad galėtume drąsiai ieškoti /./ }
    kelias := kelias + '/';

    { čia reikia mažyčio komentaro, mielas skaitytojau.
      Svarbu nepamiršti apgaubti tašką pasviro brūkšnio
      simboliais, t. y. ieškoti /./, o ne /., kadangi šitaip rastume ir /..,
      ir - kas yra dar baisiau - visai nekaltus katalogus, tokius kaip
      /.slapta/, /..kazkas/, ar tiesiog /.../ (taip, tai galimas katalogo vardas) }

    p := pos('/./', kelias);
    while p > 0 do
    begin
        delete(kelias, p, 2);
        p := pos('/./', kelias);
    end;

    { toliau pateiktas algoritmas nebūtų teisingas, jei funkcija pos()
      grąžintų ne pačią kairiausią eilutės vietą. Norėdami gauti teisingą
      kelią, turime būti tikri, jog kelyje /koks/nors/kelias/../.. pirmiausia
      aptiksime ir iškirpsime /kelias/.., o ne /../.. }

    p := pos('/../', kelias);
    while p > 0 do
    begin
        { l - kiek simbolių iškirpsime }
        l := 3;
        if p > 1 then { jei /../ nėra kelio pradžia }
            repeat
                dec(p);
                inc(l);
            until kelias[p] = '/';
        delete(kelias, p, l);
        p := pos('/../', kelias);
    end;

    { nukerpame /, kurį pridėjome. Tačiau gali būti, jog tai vienintelis likęs
      / simbolis - tuomet jį paliekame }
    if (length(kelias) > 1) then
        delete(kelias, length(kelias), 1);

    writeln(kelias);
end.
