{
TASK: VARLIUKAI
LANG: PASCAL
}

program bar;
  const
    fin = 'varliukai.in';
    fout= 'varliukai.out';
    maxn = 3000;

    NO_EDGE = 0;
    FW_EDGE = 1;
    BK_EDGE = 2;
  type
    Tmatrix = array [0 .. maxn, 0 .. maxn] of byte;
    Tarray  = array [0 .. maxn] of integer;
  var
    m: Tmatrix;
    f1, f2: text;
    best, n, i, j, k, cnt, ilgis: integer;
    klaida: boolean;

    inQueue, queue, dist: Tarray;
    head, tail: integer;

function find (a: integer): integer;
  var
    vertex, i: integer;
    ok: boolean;
    leftmost, rightmost: integer;
begin
  inQueue [a] := 1;
  dist [a] := 0;

  head := 2;
  tail := 1;
  queue[tail] := a;
  ok := true;

  leftmost := 0;
  rightmost := 0;
  while (head <> tail) and (ok) do begin
    vertex := queue [tail];
    for i := 1 to n do begin
      if (m [vertex, i] = FW_EDGE) then begin { desinen }
        if (inQueue [i] = 0) then begin
          queue [head] := i;
          inc (head);
          inQueue [i] := 1;
          dist [i] := dist [vertex] + 1;
          if dist [i] > rightmost then rightmost := dist [i];
        end
        else begin
          ok := ok and (dist [i] = dist [vertex] + 1);
        end;
      end
      else if (m [vertex, i] = BK_EDGE) then begin { kairen }
        if (inQueue [i] = 0) then begin
          queue [head] := i;
          inc (head);
          inQueue [i] := 1;
          dist [i] := dist [vertex] - 1;
          if dist [i] < leftmost then leftmost := dist [i];
        end
        else begin
          ok := ok and (dist [i] = dist [vertex] - 1);
        end;
      end;
    end;

    inc (tail);
  end;

  if (ok)
    then find := abs (leftmost) + abs (rightmost) + 1
    else find := -1;
end;

begin
  assign (f1, fin);
  assign (f2, fout);
  reset (f1);
  rewrite (f2);
  fillchar (m, sizeof (m), NO_EDGE);
  fillchar (inqueue, sizeof (inqueue), 0);
  readln (f1, n);
  klaida := false;
  for i := 1 to n do
    begin
      read (f1, cnt);
      for j := 1 to cnt do
        begin
          read (f1, k);
          klaida := klaida or ((m [k, i] <> 0) and (k = m [k, i]));
          m [i, k] := FW_EDGE;
          m [k, i] := BK_EDGE;
        end;
      readln (f1);
    end;
  { kol yra neisnagrinetu virsuniu }

  best := 0;
  k := 1;
  while ((klaida = false) and (k <= n)) do begin
    if inQueue [k] = 0 then begin
      ilgis := find (k);
      if ilgis = -1 then klaida := true
      else if ilgis > best then best := ilgis;
    end;
    inc (k);
  end;
  
  if (klaida = true)
    then writeln (f2, '0')
    else writeln (f2, best);

  close (f1);
  close (f2);
end.
