#include <cstdio>
#include <vector>
#define MAXN 100000
#define TASK "tiltai"
using namespace std;

vector<int> edges[MAXN + 1];
bool visited[MAXN + 1];

void dfs(int v)
{
	if (!visited[v])
	{
		visited[v] = true;
		for (int i = 0; i < edges[v].size(); ++i)
		{
			dfs(edges[v][i]);
		}
	}
}

int main()
{
	freopen(TASK".in", "r", stdin);
	freopen(TASK".out", "w", stdout);
	int N, M;
	scanf("%d%d", &N, &M);
	for (int i = 0; i < N; ++i)
	{
		int x, y;
		scanf("%d%d", &x, &y);
	}
	for (int i = 0; i < M; ++i)
	{
		int a, b;
		scanf("%d%d", &a, &b);
		edges[a].push_back(b);
		edges[b].push_back(a);
	}
	int components = 0;
	for (int i = 1; i <= N; ++i)
	{
		if (!visited[i])
		{
			++components;
			dfs(i);
		}
	}
	printf("%d\n", M - (N - components));
	return 0;
}
