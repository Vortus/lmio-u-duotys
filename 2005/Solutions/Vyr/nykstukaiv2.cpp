#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <queue>
using namespace std;

    struct Vertex{
        int headID;
        int COST = -1;
        vector<int> link;
    };

    int N, S, P; //aukstu, nikskt, viso k
    int EXIT_ROOM; // isejimo kambaris
    int t, COST_DOOR, COST_STAIRS; // limitas, duris, laiptai
    int playerNr[3000];
    Vertex V[200000]; // visos sajungos

    inline int flatID(int floor, int ID){
        return (floor - 1) * 50 + ID;
    }

    inline bool sameFloor(int a, int b){
        return (a - 1) / 50 == (b - 1) / 50;
    }

    void readFile(){
        int tmp; ifstream fi("nykstukaiv.g16.dat");
        fi >> N >> S >> P;
        for(int i = 1; i <= N; i++)
            fi >> tmp;
        int a, b, c, d; fi >> a >> b; // exitromas
        EXIT_ROOM = flatID(a, b);
        V[EXIT_ROOM].headID = EXIT_ROOM;
        fi >> t >> COST_DOOR >> COST_STAIRS; // visi costai
        for(int i = 1; i <= S; i++){ // nikstuku pozicijos
            fi >> a >> b; playerNr[i] = flatID(a, b);
        }
        for(int i = 0; i < P; i++){ // visi linkai
            fi >> a >> b >> c >> d;
            int fl = flatID(a, b);
            int fl2 = flatID(c, d);
            V[fl].headID = fl;
            V[fl].link.push_back(fl2);
            V[fl2].headID = fl2;
            V[fl2].link.push_back(fl);
        }

        fi.close();
    }

    void exploreBFS(int v){
        queue<int> q; q.push(v);
        while(!q.empty()){
            v = q.front();
            q.pop();
            for(int i = 0; i < V[v].link.size(); i++){
                int j = V[v].link[i];
                int c = !sameFloor(v, j) ? COST_STAIRS : COST_DOOR;
                if(V[j].COST == -1){
                    V[j].COST = V[v].COST + c;
                    q.push(j);
                }
            }
        }
    }

 int main(){

    readFile();
    V[EXIT_ROOM].COST = COST_DOOR;
    exploreBFS(EXIT_ROOM);

    ofstream fo("NAMAS.out");
    int result = 0;
    for(int i = 1; i <= S; i++)
        if(V[playerNr[i]].COST <= t && V[playerNr[i]].COST != -1) result++;
    fo << result << endl;
    for(int i = 1; i <= S; i++)
        if(V[playerNr[i]].COST <= t && V[playerNr[i]].COST != -1) fo << i << endl;
    fo.close();

 return 0; }
