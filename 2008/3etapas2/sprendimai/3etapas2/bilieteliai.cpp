/*
TASK: BILIETELIAI
LANG: C++
*/
/* Autorius: Linas
 * Sprendimo sudėtingumas - O(NM)
 */

#include <cstdio>
#include <vector>

using namespace std;

void bfs(int v, int n, int p[], int d[], vector<int> adj[])
{
    const int infinity = 1000000;
    for (int i = 1; i <= n; i++) {
        d[i] = infinity;
        p[i] = 0;
    }
    vector<int> queue;
    queue.push_back(v);
    d[v] = 0;
    for (int q = 0; q < queue.size(); q++) {
        v = queue[q];
        for (int i = 0; i < adj[v].size(); i++) {
            int u = adj[v][i];
            if (d[u] == infinity) {
                d[u] = d[v] + 1;
                p[u] = v;
                queue.push_back(u);
            }
        }
    }
}

int main()
{
    freopen("bilieteliai.in", "r", stdin);
    freopen("bilieteliai.out", "w", stdout);
    int n, m, a, b;
    scanf("%d %d %d %d", &n, &m, &a, &b);

    /* viršūnės numeruojamos taip:
     * stotelės nuo 1 iki m, autobusai - nuo m + 1 iki m + n
     */
    vector<int> adj[n + m + 1];

    for (int stop = 1; stop <= m; stop++) {
        int numb, bus;
        scanf("%d", &numb);
        for (int i = 0; i < numb; i++) {
            scanf("%d", &bus);
            adj[stop].push_back(m + bus);
            adj[m + bus].push_back(stop);
        }
    }

    int p[n + m + 1], d[n + m + 1];
    bfs(b, n + m + 1, p, d, adj);

    printf("%d\n", d[a] / 2);
    for (int i = a, isstop = 1; p[i] != 0; i = p[i], isstop = !isstop)
        printf(isstop ? "%d " : "%d\n", isstop ? i : i - m);

    return 0;
}
