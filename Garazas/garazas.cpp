#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

    struct Car{
        int index;
        int weight;
    } cars[3000];

    struct Place{
        int price, occupying = -1;
    } places[3000];

    int placeC, carC, money = 0; // vietu masinu kiekiai

    vector<Car> carQueue;
    vector<int> moveQueue;

    void readFile(){

        ifstream fi("garazas.in");
        fi >> placeC >> carC;

        for(int i = 0; i < placeC; i++){
            fi >> places[i].price;
        }

        for(int i = 0; i < carC; i++){
            fi >> cars[i].weight;
            cars[i].index = i;
            //cout << cars[i].weight << " " << i << endl;
        }

        while(!fi.eof()){
            int j; fi >> j;
            moveQueue.push_back(j);
        }

        fi.close();

    }

    int canFlush(){
        for(int i = 0; i < placeC; i++){
            if(places[i].occupying == -1) return i;
        }
        return -1;
    }

    int getIndex(int n){
        for(int i = 0; i < placeC; i++){
            if(places[i].occupying == n) return i;
        }
        return -1;
    }

    void calculate(){
        for(int i = 0; i < moveQueue.size(); i++){
            int ci = abs(moveQueue[i]) - 1;
            bool flushCar = moveQueue[i] > 0 ? true : false; // ar imesti automobili

            int cf = canFlush();

            if(flushCar){ // jei telpa
                if(cf != -1){
                    places[cf].occupying = ci;
                    money += cars[ci].weight * places[cf].price;
                }else{ // jei negali
                    carQueue.push_back(cars[ci]);
                }
            }else{
                int k = getIndex(ci);
                if(k != -1) {
                    places[k].occupying = -1;
                }
            }

            cf = canFlush();
            if(cf != -1 && carQueue.size() != 0){
                   places[cf].occupying = carQueue[0].index;
                   money += carQueue[0].weight * places[cf].price;
                   carQueue.erase(carQueue.begin());
            }
        }

    }

 int main(){

    readFile();
    calculate();

    ofstream fo("garazas.out");
    fo << money;
    fo.close();

 return 0; }
