#include <cstdio>
using namespace std;

typedef long long ll;

const int Maxn = 100005;
const int Maxm = 1000005;

int n;
int x, y, k;
ll dx[Maxm], dy[Maxm];
ll curx, deltax, cury, deltay;

int main()
{
	freopen("cechas.in", "r", stdin);
	freopen("cechas.out", "w", stdout);
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d %d %d", &x, &y, &k);
        deltax -= k; deltay -= k;
        dx[x] += 2LL * k; dy[y] += 2LL * k;
        curx += ll(x) * ll(k); cury += ll(y) * ll(k);
    }
    int xv, yv;
    for (xv = 0; xv < Maxm; xv++) {
        deltax += dx[xv];
        if (deltax >= 0) break;
        curx += deltax;
    }
    for (yv = 0; yv < Maxm; yv++) {
        deltay += dy[yv];
        if (deltay >= 0) break;
        cury += deltay;
    }
    printf("%d %d\n%lld\n", xv, yv, curx + cury);
    return 0;
}
