program Giles;

    const MAXT = 10000;
          MAXP = 15;
          
    var gile : array [1..MAXT, 1..MAXP] of boolean;
        ejimai : array [1..MAXT] of integer;
        P, T, N : longint;

procedure pradiniai_duomenys;
var f : text; 
    raide : char;
    tk, pk, k : longint;
begin
    assign(f, 'GILES.IN'); reset(f);
    readln(f, P, T, N);
    for k := 1 to T do begin
        read(f, raide);
        if raide = 'K' then ejimai[k] := -1
        else if raide = 'D' then ejimai[k] := 1
        else ejimai[k] := 0;
    end;
    readln(f);
    for k := 1 to N do begin
        readln(f, tk, pk);
        gile[tk, pk] := true;
    end;
    close(f);
end;
    
procedure rezultatai(ats : longint);
var f : text;
begin
    assign(f, 'GILES.OUT'); rewrite(f);
    writeln(f, ats);
    close(f);
end;

    var
        laikas, pozicija, atsakymas : longint;
        
begin
    pradiniai_duomenys;
    
    pozicija  := (P + 1) div 2;
    atsakymas := 0;
    
    for laikas := 1 to T do begin
        inc(pozicija, ejimai[laikas]);
        if gile[laikas, pozicija]
            then inc(atsakymas);
    end;

    rezultatai(atsakymas);    
end.
