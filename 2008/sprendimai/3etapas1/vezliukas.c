#include <stdio.h>
#include <stdlib.h>
#define MAX_N 255
#define STULPELIU 2000 //   
#define EILUCIU 2000
#define BALTA 0
#define JUODA 1

char ekranas[STULPELIU][EILUCIU];
int vX = STULPELIU / 2;
int vY = EILUCIU / 2;


void vykdykCikla(int* kiek, int dX, int dY){
      int j;
      for(j=1;j<=(*kiek);j++) { 
          vX+=dX; 
          vY+=dY; 
          ekranas[vX][vY]= JUODA; 
      } 
      *kiek = 1; // nuvalome ciklo kint.
      return;
     }

void skaitykIrSpalvink(char* byla){
  int kiek_simb=0;
  char komandos[256];
  int i=0;
  int j;
  int ciklas = 1; 

  memset(ekranas, BALTA, STULPELIU * EILUCIU);
  
  FILE* fin = fopen(byla,"rt");
  fscanf(fin,"%d",&kiek_simb);
  fgets(komandos, kiek_simb + 1 ,fin);   // praleisti EOLN 
  fgets(komandos, kiek_simb + 1 ,fin);    
  fclose(fin);

  // skaitome ir vykdome spalvinimo komandas 
  while(i < kiek_simb){
       switch ( komandos[i] ){
         case 'C' : 
              i++;
              ciklas = komandos[i] - '0'; 
              break;    
         case 'S' :  vykdykCikla(&ciklas,0,-1);    break; 
         case 'P' :  vykdykCikla(&ciklas,0,1);     break; 
         case 'V' :  vykdykCikla(&ciklas,-1,0);    break; 
         case 'R' :  vykdykCikla(&ciklas,1,0);     break; 
       }                  
       i++;              
  }
}

void skaiciuokIrRasyk(char* byla){
     int i,j, kiek=0;
     // sumuojame visus juodus pikselius
     for(i=0;i<STULPELIU;i++)
        for(j=0;j<EILUCIU;j++)
          kiek+=ekranas[i][j];
  
     FILE* fout = fopen(byla,"wt");
     fprintf(fout,"%d",kiek);
     fclose(fout);
     return;
     }

int main(int argc, char *argv[])
{
  skaitykIrSpalvink("vezliukas.in");
  skaiciuokIrRasyk("vezliukas.out");
  
  return 0;
}
