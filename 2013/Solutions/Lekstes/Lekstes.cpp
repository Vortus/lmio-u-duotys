#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

    #define MAXN 200100
    typedef pair<int, int> Plate; // lekste
    vector<Plate> plates; // visos lekstes
    int N; // viso leksciu

    void readFile(string p){
        ifstream fi(p); fi >> N; int a, b;
        for(int i = 0; i < N; i++){
            fi >> a >> b; plates.push_back(Plate(a, b));
            if(plates[i].first < plates[i].second)
                swap(plates[i].first, plates[i].second);
        }
        sort(plates.begin(), plates.end());
        fi.close();
    }

 int main(){

    readFile("lekstes.txt");

    /////longest increasing subsequence - reiki ieskot
//    vector<int> L(MAXN); int maxk;
//    for(int k = 0; k < N; k++){
//        maxk = 0;
//        for(int m = 0; m < k; m++){
//            if(plates[k].second >= plates[m].second &&
//               L[m] > maxk) maxk = L[m];
//        }
//        L[k] = maxk + 1;
//    }
//    maxk = -1;
//    //////
//    for(int i = 0; i < MAXN; i++) if(L[i] > maxk) maxk = L[i];
//    cout << maxk;

 return 0; }
