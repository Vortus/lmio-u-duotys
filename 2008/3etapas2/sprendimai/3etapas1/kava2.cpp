/*
TASK: kava
LANG: C++
*/

#include <cstdio>
#include <algorithm>
using namespace std;

#define MAXN 104
#define B 10010

int n, a[MAXN], b, c;
int p[B][MAXN];

int main() {
	freopen("kava.in", "r", stdin);
	freopen("kava.out", "w", stdout);
	scanf("%d%d%d", &b, &c, &n);
	for (int i = 1; i <= n; ++i)
		scanf("%d", &a[i]);
	fill(p[0], p[B], -1);
	p[0][0] = 0;
	for (int i = 1; i <= n; ++i)
		for (int x = b; x >= 0; --x)
			for (int y = c; y >= 0; --y)
				if (p[x][y] < 0 && y && x >= a[i] && p[x-a[i]][y-1] >= 0)
					p[x][y] = i;
	if (p[b][c] < 0)
		printf("0\n");
	else {
		while (c) {
			printf("%d\n", p[b][c]);
			b -= a[p[b][c]];
			--c;
		}
	}
	return 0;
}
