/*
TASK:LINOBACI
LANG:C++
*/
#include <cstdio>
int main() {
    freopen("linobaci.in", "r", stdin);
    freopen("linobaci.out", "w", stdout);
    int n, a, l = 0, sum = 0;
    scanf("%d%d", &n, &sum);
    if (sum == 1)
        for (l = 1; l < n; l++) {
            scanf("%d", &a);
            if (a > sum)
                break;
            else
                sum += sum >= 1000000000 ? 0 : a;
        }
    printf("%d\n", l);
    return 0;
}
