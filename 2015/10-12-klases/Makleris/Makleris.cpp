#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;

    #define MAXBITS 100010
    typedef unsigned long long ULL;

    vector<ULL> BITSUMS(MAXBITS); // sumos
    vector<ULL> BITS; // pitstopai
    vector<ULL> BESTR; // geriausios antenos tarp pitstpu
    vector<ULL> BITDIFF;
    ULL N, L, C, T, tmp; // antenu kiekis, visas kelias, antenos vienas metras

    void readFile(string s){
        ifstream fi(s);
        fi >> N >> L >> C >> T;
        BITS.push_back(0); // 0 pitstopas
        while(!fi.eof()){ fi >> tmp; BITS.push_back(tmp); }
        sort(BITS.begin(), BITS.end());
    }

    void getBESTR(){ // geriausios tarp antenas
        for(tmp = 1; tmp <= N; tmp++){
            ULL j = BITS[tmp] - BITS[tmp - 1];
            BITDIFF.push_back(j);
            BESTR.push_back(j / 2);
            BESTR.push_back(j / 2 + j % 2);
        }
        sort(BITDIFF.begin(), BITDIFF.end());
        sort(BESTR.begin(), BESTR.end());
        BITDIFF.push_back((ULL)INFINITY);
    }

    void getSUMS(){
        BITSUMS[N - 1] = 0;
        for(int tmp = N - 2; tmp >= 0; tmp--)
            BITSUMS[tmp] = BITSUMS[tmp + 1] + BITDIFF[tmp];
    }


 int main(){

    readFile("makleris.in");
    getBESTR();
    getSUMS();

    ULL result = L * T; ULL j = 0;
    for(tmp = 0; tmp < BESTR.size(); tmp++){
        ULL R = BESTR[tmp];
        while(2 * R > BITDIFF[j]) j++;
        ULL c = R * C + (BITSUMS[j] - (N - j - 1) * 2 * R) * T;
        if(c < result) result = c;
    }

    cout << result;

 return 0; }
