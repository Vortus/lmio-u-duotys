#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <stdlib.h>

using namespace std;

    struct Rule{
        string to;
        string groups[4][2];
    }rules[1002];

    struct Link{
        string group[4];
    }links[1002];

    int ruleCount, N;

    void splitRuleString(string s, int index){
        string tmps;
        vector<string> tmp;

        ////susplitinam i grupes
        for(int i = 0; i < s.length(); i++){
            if(s[i] == '.'){
                tmp.push_back(tmps);
                tmps = "";
                i++;
            }
            tmps += s[i];
        }
        tmp.push_back(tmps);
        ///////

        ////tikrinam intervalus
        for(int i = 0; i < tmp.size(); i++){
            int j = tmp.at(i).find("-");
            if(j == -1){ // jei nerado intervalo
                rules[index].groups[i][0] = tmp.at(i);
                rules[index].groups[i][1] = tmp.at(i);
            }else{
                rules[index].groups[i][0] = tmp.at(i).substr(0, j);
                tmp.at(i) = tmp.at(i).substr(j + 1);
                rules[index].groups[i][1] = tmp.at(i);
            }
        }
        /////
    }

    void splitLinkString(string s, int index){
        ////pirmas
        int i = s.find(".");
        links[index].group[0] = s.substr(0, i);
        s = s.substr(i + 1);
        ////

        ////ketvirtas
        i = s.find(".");
        links[index].group[1] = s.substr(0, i);
        s = s.substr(i + 1);
        ////

        ////trecias
        i = s.find(".");
        links[index].group[2] = s.substr(0, i);
        s = s.substr(i + 1);
        ////

        ////ketvirtas
        links[index].group[3] = s;
        ////

    }

    bool fitsIn(Rule r, Link l){
        for(int i = 0; i < 4; i++){
            int a = atoi(l.group[i].c_str());
            int b = atoi(r.groups[i][0].c_str());
            int c = atoi(r.groups[i][1].c_str());
            if(a < b || a > c) return false;
        }
        return true;
    }

    void readFile(){
        ifstream fi("marsrutizatorius.in");
        fi >> ruleCount >> N;

        for(int i = 0; i < ruleCount; i++){
            string a, b; fi >> a >> b;
            rules[i].to = b;
            splitRuleString(a, i);
        }

        for(int i = 0; i < N; i++){
            string a; fi >> a;
            splitLinkString(a, i);
        }

        fi.close();
    }

 int main(){

	readFile();

	ofstream fo("marsrutizatorius.out");
    for(int i = 0; i < N; i++){
        for(int j = 0; j < ruleCount; j++){
            bool b = fitsIn(rules[j], links[i]);
            if(b){
                fo << rules[j].to << endl;
                break;
            }
        }
    }
	fo.close();

 return 0; }
