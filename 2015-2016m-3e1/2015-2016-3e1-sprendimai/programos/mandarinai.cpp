/**
 * Sprendimo idėja:
 *
 * Greitai gendančius mandarinus reikia bandyti valgyti kiek įmanoma anksčiau.
 * Tad sugrupuojame mandarinus pagal jų sugedimo dieną (nuo mažiausios iki
 * didžiausios) ir valgome iš eilės.
 *
 * Autorius: Linas Petrauskas
 */

#include <cstdio>
#include <algorithm>

using namespace std;

const int MAX_D = 1000;

int sugenda[MAX_D + 1];

int main() {
	// Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis
	// iš failo.
	// freopen("mandarinai-jau.in", "r", stdin);
	// freopen("mandarinai-jau.out", "w", stdout);

	// Nusiskaitome duomenis ir sugrupuojame mandarinus pagal jų sugedimo dieną.
	int N, K;

	scanf("%d%d", &N, &K);

	for (int i = 0; i < N; i++) {
		int d;
		scanf("%d", &d);
		sugenda[d]++;
	}

	int suma = 0;

	// Simuliuojame valgymą
	for (int diena = 1, valgyti_is_dienos = 1; diena <= MAX_D; diena += 1) {
		int suvalgem = 0;
		// Peršokame sugedusius.
		valgyti_is_dienos = max(diena, valgyti_is_dienos);

		// Valgom kiek galime
		while (valgyti_is_dienos <= MAX_D && suvalgem < K) {
			const int suvalgyti = min(sugenda[valgyti_is_dienos], K - suvalgem);
			sugenda[valgyti_is_dienos] -= suvalgyti;
			suvalgem += suvalgyti;

			if (sugenda[valgyti_is_dienos] == 0) {
				valgyti_is_dienos += 1;
			}
		}

		suma += suvalgem;
	}

	printf("%d\n", suma);
}
