#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

    #define FILE "zaidimas1.txt"
    #define MAXN 50010
    #define INF 2000000000
    int N; // lygiu kiekis
    typedef pair<int, int> Level;
    vector<int> levels[MAXN]; // lygiai

    void readFile(){
        int where, a, b;
        ifstream fi(FILE); fi >> N;
        bool first = true;
        for(int i = 1; i < N; i++){
            fi >> a >> b;
            for(int j = a; j <= b; j++)
                levels[i].push_back(j);
        }
        fi.close();
    }

 int main(){

    readFile();
    for(int i = 1; i <= N; i++)
        cout << levels[i].size() << " ";
 return 0; }
