#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

    double M, K;

    void readFile(){
        ifstream fi("t/avys.s01.in");
        fi >> M >> K;
        K = pow(2, K);
        fi.close();
    }

 int main(){

    readFile();
    int i = 1, j = K, pos = M;
    while(i != j){
        pos = j - (pos - i);
        int jj = (i + j - 1) / 2;
        if(pos <= jj) j = jj;
        else i = jj + 1;
    }

    cout << pos;

 return 0; }
