#include <cstdio>

using namespace std;

#define MAXN 1005
#define INF 1000000000

int poliai[MAXN];

int main() {
	freopen("varle-jau.in", "r", stdin);
	freopen("varle-jau.out", "w", stdout);
	int n, d;
	scanf("%d %d", &n, &d);
	for (int i = 0; i < n; i++) {
		scanf(" %d", &poliai[i]);
	}
	int best_h, best_p;
	for (int pr = 0; pr < d; pr++) {
		int maz = poliai[pr];
		int lyg = 0, nelyg = 0; 
		for (int t = pr; t < n; t += d) {
			if ((t + 1) % 2 == 0) lyg++;
			else nelyg++;
			int aukstis = poliai[t] + ((t + 1) % 2 == 0 ? ((-2) * lyg + 1 * nelyg) : (1 * lyg + (-2) * nelyg)); //suskaičiuoti dabartinį polio aukštį
			if (aukstis < maz) maz = aukstis;
		}	
		if (pr == 0 || best_h < maz) {
		   best_h = maz;
		   best_p = pr + 1;
		}
	}
	printf("%d %d\n", best_p, best_h);
	return 0;
}
