/*
TASK: sustojimas
LANG: C++
*/
#include <cstdio>
#include <cstdlib>
#include <string>
#include <map>
#include <set>
#include <vector>
using namespace std;

struct proc {
	string name;
	vector<string> calls;
	bool analysed;
	int ops;
	bool recursive;
};

map <string, proc> procs;

void read() {
	char name[200];
	int ll;
	scanf("%d", &ll);
	while (scanf(" PROC %s", name) == 1 && ll) {
		--ll;
		proc np;
		np.name = name;
		np.analysed = 0;
		while (scanf(" CALL %s", name) == 1) {
			np.calls.push_back(name);
			--ll;
		}
		scanf(" END");
		--ll;
		procs[np.name] = np;
	}
}

set<string> called;

void analyse(string name) {
	proc &p = procs[name];
	called.insert(name);
	p.ops = 1;
	p.recursive = 0;
	for (int i = 0; i < p.calls.size(); ++i) {
		if (called.count(p.calls[i])) {
			p.recursive = 1;
			break;
		}
		++p.ops;
		proc &cp = procs[p.calls[i]];
		if (!cp.analysed) {
			analyse(p.calls[i]);
		}
		p.ops += cp.ops;
		if (cp.recursive) {
			p.recursive = 1;
			break;
		}
	}
	if (!p.recursive)
		++p.ops;
	p.analysed = 1;
	called.erase(name);
}

int main() {
	freopen("sustojimas.in", "r", stdin);
	freopen("sustojimas.out", "w", stdout);
	read();
	analyse("MAIN");
	printf("%s\n%d\n", procs["MAIN"].recursive ? "NEBAIGIA" : "BAIGIA", procs["MAIN"].ops);
	return 0;
}
