/*
TASK:EPID
LANG:C
*/

#include <stdio.h>
#include <stdlib.h>

/* Konstantos */
#define MAX_N 1000
#define MAX_RIBA (MAX_N + 1) * MAX_N / 2

/* Globalios strukturos*/

char poros[MAX_N][MAX_N]; /* Grafas */

/* Populiacijos �eimos */
int seimuSkaicius;
int seimos[MAX_N];
char patikrintiPauksciai[MAX_N];

/* Sumos */
int  sumuAibe[ MAX_RIBA ];
int  sumuAibesKopija[ MAX_RIBA ];

int N; /* Pauk��i� skai�ius */
int K; /* U�sikr�tusi� pauk��i� skai�ius*/

/* Inicializacija */
void inicializacija(char* str)
{
  FILE* fin;
  int i,j,poruSkaicius,a,b;
  fin = fopen(str,"r");
  
  fscanf(fin,"%d %d",&N,&K); 
  fscanf(fin,"%d",&poruSkaicius);
  for(i=0;i<N;i++){
     patikrintiPauksciai[i]=0; 
     for(j=0;j<N;j++)
      {
      if (i != j) poros[i][j]=0;                      
           else  poros[i][j]=1;                       
      }
    }
  for(i=1;i<=poruSkaicius;i++)
   {
   fscanf(fin,"%d %d",&a,&b);
   poros[a-1][b-1]=1;
   poros[b-1][a-1]=1;
   poros[a-1][a-1] =0;
   poros[b-1][b-1] =0;
   }
   
  fclose(fin);   
  
  for(i=0;i<MAX_RIBA;i++){
   sumuAibe[ i ] = 0;
   sumuAibesKopija[ i ] = 0;                                                    
  }
}
/* Randame �eim� parametrus, panaudodami DFS   */
int igp(int nuo){
   int skaicius=0, i ;
 
   patikrintiPauksciai[nuo]=1;
   if (poros[nuo][nuo]){ return -1;}
   else{
    for(i=1;i<N;i++)
     if ((poros[nuo][i]==1) && (patikrintiPauksciai[i]==0)){
       skaicius = skaicius + 1 + igp(i);        
     } 
   }  
   patikrintiPauksciai[nuo]=2;

   return skaicius;
}
int palygink (void* a, void * b)
{
  return ( *(int*)a - *(int*)b );
}
void randameSeimuParametrus(){
 int i,kiek;
 seimuSkaicius=0;
 for(i=0;i<N;i++){
   if (patikrintiPauksciai[i] == 0) {
   kiek=igp(i);                
   if (kiek > 0) {
            seimos[seimuSkaicius]=kiek + 1;                     
            seimuSkaicius++;
            }
      else if ( kiek == -1 ) {
            seimos[seimuSkaicius]=1;                     
            seimuSkaicius++;
           }      
    }         
 }    
 qsort(seimos,seimuSkaicius,sizeof(int),palygink);   
}


int main(int argc, char* argv[])
{
 
 int D,i,j; 
 inicializacija("EPID.IN"); 
 randameSeimuParametrus();
  
 /* Did�iausias:*/
 D = 0; 
 for(i = seimuSkaicius-1; ( i >= 0 ) && ( i >= ( seimuSkaicius - K ) );i--)
 {
     D = D + seimos[i] ;
 }    

 FILE* fout;
 fout = fopen("EPID.OUT","wt1");
 fprintf(fout,"%d",D);
 close(fout);
   
    
    
}
