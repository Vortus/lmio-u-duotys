#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

    struct Ski{
        int ID, length;
    } skiArr[2001];

    bool cmpr(const Ski &a, const Ski &b){
        return a.length < b.length;
    }

    int needPairs, N, minSum = 0;
    int lengthDiffBuffer[1001][2001];
    bool chosenSkis[1001][2001];

    void readFile(){
        ifstream fi("slides.in");
        Ski tmp; fi >> needPairs >> N;
        for(int i = 1; i <= N; i++){
            int j; fi >> j;
            tmp.ID = i; tmp.length = j;
            skiArr[i] = tmp;
        }
        fi.close();
      sort(skiArr, skiArr + N + 1, cmpr);
    }

    int minF(bool &b, int a, int c){
        if(a < c) b = false;
        else b = true;
        return min(a, c);
    }

 int main(){

    readFile();

    for(int i = 1; i <= needPairs; i++){
        lengthDiffBuffer[i][i * 2] =
        lengthDiffBuffer[i - 1][i * 2 - 2] + (skiArr[i * 2].length - skiArr[i * 2 - 1].length);
        chosenSkis[i][i * 2] = true;

        for(int j = i * 2 + 1; j <= (N - (needPairs - i) * 2); j++){
            bool b;
            lengthDiffBuffer[i][j] =
            minF(b, lengthDiffBuffer[i][j - 1],
            lengthDiffBuffer[i - 1][j - 2] + (skiArr[j].length - skiArr[j - 1].length));
            chosenSkis[i][j] = b;
        }
    }

    ofstream fo("slides.out");
    fo << lengthDiffBuffer[needPairs][N] << endl;
    while (needPairs != 0) {
        if (chosenSkis[needPairs][N]) {
            fo << skiArr[N - 1].ID << " " << skiArr[N].ID << endl;
            N -= 2; needPairs--;
        }else N--;
    }
    fo.close();

 return 0; }
