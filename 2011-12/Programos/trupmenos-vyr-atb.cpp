#include <vector>
#include <iostream>
#include <cstdio>
#define ll long long
#define pb push_back
using namespace std;

typedef vector<ll> vl;

vl skaiciuot(ll p, ll q, ll k){
    vl res;
    if (k==1){
        if (!(q%p)) res.pb(q/p);
    } else {
        int mm=q/p+1, m;
        for (m=k*q/p; m>=mm; --m){
            vl x=skaiciuot(p*m-q,m*q,k-1);
            if (x.size()) {
                x.pb(m);
                return x;
            }
        }
    }
    return res;
}

int main(){
    freopen("trupmenos-vyr.in", "r", stdin);
    freopen("trupmenos-vyr.out", "w", stdout);
    vl res;
    ll p, q, k=0, i;
    cin >> p >> q;
    while (!res.size()){
        ++k;
        res=skaiciuot(p,q,k);
    }
    cout << k << endl;
    for (i=0; i<k; ++i) cout << res[i] << " ";
    cout << endl;
}
