#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#define NUM 1000000

using namespace std;

struct Branch {
	int x, y, z;
};

bool operator<(const Branch& a, const Branch& b) {
	return ((a.x < b.x) || ((a.x == b.x)  && (a.y < b.y)) || ((a.x == b.x)  && (a.y == b.y) && (a.z < b.z)));
}

bool operator==(const Branch& a, const Branch& b) {
	return ((a.x == b.x)  && (a.y == b.y) && (a.z == b.z));
}

int main() {
	freopen("maleja-jau.in", "r", stdin);
	freopen("maleja-jau.out", "w", stdout);

	Branch s;
	int N;
	vector<Branch> v;

	// input data
	cin >> N;
	for(int i = 0; i < N; i++) {
		cin >> s.x >> s.y >> s.z;
		v.push_back(s);
	}

	// solve
	std::sort(v.begin(), v.end());
	int M = std::unique(v.begin(), v.end()) - v.begin();
	cout << N - M + 1 << endl;

	return 0;
}