/*
TASK: diktavimas
LANG: C++
*/

#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <vector>
using namespace std;

#define LNS 1005
#define LNSK LNS+10
#define LOTS 0x3fffffff
#define LOTSLL 0x3fffffffffffffffll
#define MAXNODES 501000

struct node {
	int cost;
	int link_s;
	char link_n;
	char edge;
};

node nodes[MAXNODES];
int nn;

char S[LNS];
long long cost[LNS];
int ln[LNS];

void make_trie(int id, const vector<int> &nr) {
//	printf("node %d: link_s %d, link_n %hhd, edge %hhd\n", id, nodes[id].link_s, nodes[id].link_n, nodes[id].edge);
//	for (int i = 0; i < nr.size(); ++i)
//		printf(" %d", nr[i]);
//	printf("\n");
	vector<int> nrs[10];
	for (int i = 0; i < nr.size(); ++i)
		if (isdigit(S[nr[i]]))
			nrs[S[nr[i]]-'0'].push_back(nr[i]+1);
	nodes[id].link_s = nn;
	nodes[id].link_n = 0;
	for (int i = 0; i < 10; ++i)
		if (nrs[i].size()) {
			nodes[nn].cost = -1;
			nodes[nn].edge = i;
			++nodes[id].link_n;
			++nn;
		}
	for (int i = 0, j = nodes[id].link_s; i < 10; ++i)
		if (nrs[i].size())
			make_trie(j++, nrs[i]);
}

int getedge(int id, int e) {
	for (int i = nodes[id].link_s, j = 0; j < nodes[id].link_n; ++i, ++j)
		if (nodes[i].edge == e)
			return i;
	return -1;
}

int main() {
	freopen("diktavimas.in", "r", stdin);
	freopen("diktavimas.out", "w", stdout);
	nodes[0].cost = -1;
	nodes[0].edge = -1;
	nn = 1;
	//===== skaitymas ======
	char buff[LNSK];
	fgets(S, LNS, stdin);
	vector<int> all;
	for (int i = 0; isdigit(S[i]); ++i)
		all.push_back(i);
	make_trie(0, all);
	fgets(buff, LNSK, stdin);
	int n;
	sscanf(buff, "%d", &n);
	for (int i = 0; i < n; ++i) {
		fgets(buff, LNSK, stdin);
		int x = 0;
		int j = 0;
		for (; isdigit(buff[j]); ++j) {
			x = getedge(x, buff[j]-'0');
			if (x < 0)
				break;
		}
		int k;
		sscanf(buff+j, "%d", &k);
		if (x >= 0 && (nodes[x].cost < 0 || nodes[x].cost > k))
			nodes[x].cost = k;
	}
	
	//===== skai�iavimas ======
	int len = strlen(S);
	while (!isdigit(S[len-1]))
		--len;
	cost[len] = 0;
	ln[len] = 0;
	for (int i = len-1; i >= 0; --i) {
		cost[i] = LOTSLL;
		int x = 0;
		for (int j = i; j < len; ++j) {
			x = getedge(x, S[j]-'0');
			if (x < 0)
				break;
			if (nodes[x].cost >= 0 && cost[j+1] + nodes[x].cost < cost[i]) {
				cost[i] = cost[j+1] + nodes[x].cost;
				ln[i] = j-i+1;
			}
		}
	}
	
	//======== ra�ymas =======
	if (cost[0] == LOTSLL) {
		printf("Hmmm.. neradau ne vieno varianto...\n");
		return 1;
	}

	for (int i = 0; i < len; i += ln[i]) {
		if (i)
			printf("-");
		printf("%.*s", ln[i], S+i);
	}
	printf("\n");
	
	return 0;
}
