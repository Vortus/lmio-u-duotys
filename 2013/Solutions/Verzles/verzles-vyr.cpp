#include <cstdio>
#include <algorithm>
#include <map>

using namespace std;

#define INFTY 1000000002
#define MAX_M 1000000

typedef long long LL;
typedef pair<int,int> pii;
typedef pair<LL,pii> pLLpii;

pLLpii belt[MAX_M];
map<int,int> rend;

int M, N;

void addBelt(int i, LL &dx, LL &dy) { // Nuskaitoma juosta
    int x1, y1, x2, y2;
    scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
    if(!dx && !dy) {
        dx = x2-x1;
        dy = y2-y1;
        if(dx < 0) {
            dx = -dx;
            dy = -dy;
        }
    }
    LL coeff = dx*y1-dy*x1; // Juostos komponent� statmens kryptimi
    belt[i] = pLLpii(coeff, pii(x1, x2));
}

void useBelt(pii b) { // Juosta panaudojama konstruojant intervalus
    int l = min(b.first, b.second),
        r = max(b.first, b.second),
        d = b.second;
    if(r-l <= 1) return;

    int targ = rend.lower_bound(d)->second,
        tl = rend.lower_bound(l)->second;

    rend.erase(rend.lower_bound(l), rend.lower_bound(r));
    if(targ == INFTY) rend.insert(pii(r-1, d));
    else rend.insert(pii(r-1, targ));
    rend.insert(pii(l, tl));
}

int main() {
    freopen("verzles-vyr.in", "r", stdin);
    freopen("verzles-vyr.out", "w", stdout);

    scanf("%d%d", &M, &N);

    LL dx = 0, dy = 0;
    for(int i = 0; i < M; ++i) addBelt(i, dx, dy);
    sort(belt, belt+M); // Rikiuojama statmena juostoms kryptimi
    
    // Medis rend talpina interval� de�iniuosius galus.
    // Jei (r, t) ir (r', t') yra gretimi elementai, t.y. jei r > r' ir
    // n�ra elemento (r", t"), kad r > r" > r', tai yra intervalas (r', r],
    // ant kurio nukritusios ver�l�s atsiduria krep�yje t.
    // I�imtis: jei t = INFTY, tai ant (r', r] nukritusios juostos toliau
    // kris vertikaliai.
    
    rend.insert(pii(INFTY,INFTY)); // Pirmas intervalas - visos ver�l�s krenta vertikaliai
    for(int i = 0; i < M; ++i)
        useBelt(belt[i].second);
        
    for(int j = 0; j < N; ++j) {
        int pipe, bucket;
        scanf("%d", &pipe);
        bucket = rend.lower_bound(pipe)->second; // Metame ver�l�
        if(bucket == INFTY) bucket = pipe;
        printf("%d\n", bucket);
    }
    
    return 0;
}
