#include <cstdio>
#include <iostream>
#define MOD 1000000007
#define TASK "medis-vyr"

using namespace std;

typedef long long ll;

// Apskaičiuoja 1 + base + base^2 + ... + base^d per log(d) laiką.
ll count(ll base, int d) {
    if (d == 0)
        return 1;
    if (d % 2 == 0)
        return (1 + base * count(base, d - 1)) % MOD;
    return (count((base * base) % MOD, d / 2) * (1 + base)) % MOD;
}

ll pow(ll base, int exp) {
    if (exp == 0)
        return 1;
    if (exp & 1)
        return (base * pow(base, exp - 1)) % MOD;
    return pow((base * base) % MOD, exp / 2);
}

int main() {
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
	ll N, R, D;
    cin >> N >> R >> D;
    ll totalR = pow(R, D);
    ll totalZ = (count(R, D - 1) * (N - R)) % MOD;
    printf("%lld %lld\n", totalR, totalZ);
    return 0;
}
