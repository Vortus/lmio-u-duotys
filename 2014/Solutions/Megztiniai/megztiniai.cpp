#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

 int main(){

    int N, capacity, result = 0;
    ifstream fi("megztiniai.in");
    fi >> N >> capacity;
    for(int i = 0; i < N; i++){
        double j; fi >> j;
        result += ceil(j / capacity);
    }
    fi.close();

    ofstream fo("megztiniai.out");
    fo << result;
    fo.close();

 return 0; }
