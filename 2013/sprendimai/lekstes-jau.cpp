#include <cstdio>
#include <algorithm>

using namespace std;

#define MAX_N 1000

typedef pair<int,int> pii;
pii plate[MAX_N];
int best[MAX_N], n, ans;

int main() {
    freopen("lekstes-jau.in", "r", stdin);
    freopen("lekstes-jau.out", "w", stdout);
    
    scanf("%d", &n);
    for(int i = 0; i < n; ++i)
        scanf("%d%d", &plate[i].first, &plate[i].second);
    sort(plate, plate+n);
    
    ans = 0;
    for(int i = 0; i < n; ++i) {
        best[i] = 1;
        for(int j = 0; j < i; ++j)
            if(plate[j].second <= plate[i].second
               && best[i] < best[j]+1)
                best[i] = best[j]+1;
        if(ans < best[i]) ans = best[i];
    }
    
    printf("%d\n", ans);
    return 0;
}
