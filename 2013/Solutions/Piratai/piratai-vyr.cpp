#include <cstdio>
#include <algorithm>

using namespace std;

#define MAX_K 100000
#define IMPOSSIBLE "NENUPLAUKS"

typedef long long LL;
int t[MAX_K], v[MAX_K], must[MAX_K], N, K;
LL ans;

int main() {
    freopen("piratai-vyr.in", "r", stdin);
    freopen("piratai-vyr.out", "w", stdout);
    
    scanf("%d%d", &N, &K);
    for(int i = 0; i < K; ++i)
        scanf("%d%d", &t[i], &v[i]);
    must[K-1] = v[K-1]; // Prasidedant prognozei i, b�tina tur�ti bent must[i] pirat�
    for(int i = K-2; i >= 0; --i)
        must[i] = max(must[i+1], v[i]); 
    
    if(must[0] > N) {
        printf(IMPOSSIBLE);
        return 0;
    }
    
    ans = 0;
    for(int i = 0; i < K; ++i)
        while(t[i]) {
            int m = max(must[i], (v[i]+N+1)/2); // Girdome kuo ma�iau pirat�
            if(m == N) { // Prognoz�s metu pirat� kiekis nebema��ja
                ans += (LL) t[i]*N;
                t[i] = 0;
            } else { // Pirat� kiekis dar ma��ja
                ans += (LL) m;
                N = m;
                --t[i]; 
            }
        }
    printf("%lld", ans);
    return 0;
}
