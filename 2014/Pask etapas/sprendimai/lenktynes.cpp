#include "lenktynes.h"
#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

typedef pair<double, double> pdd;  // <startas, greitis>
typedef pair<int, double> pid;  // <begikas, laikas>

double kadaPavis(pdd pirmas, pdd antras) {
	if (pirmas.second <= antras.second)
		return -1;  // Nepavis niekada
	return (antras.first - pirmas.first) / (pirmas.second - antras.second);
}

double kadaFinisuos(pdd begikas, double finisas) {
	return (finisas - begikas.first) / begikas.second;
}

double kiekIlgiausiaiPirmavo(double finisas, int N, double startas[], double greitis[]) {
	vector<pdd> begikai;
	for (int i = 0; i < N; i++)
		begikai.push_back(pdd(startas[i], greitis[i]));
	sort(begikai.begin(), begikai.end());
	vector<pid> pirmauja;
	for (int i = N - 1; i >= 0; i--) {
		while (!pirmauja.empty()) {
			pid p = pirmauja.back();
			double pavis = kadaPavis(begikai[i], begikai[p.first]);
			if (pavis == -1)
				break;
			if (pavis <= p.second)
				pirmauja.pop_back();
			else {
				pirmauja.push_back(pid(i, pavis));
				break;
			}
		}
		if (pirmauja.empty())
			pirmauja.push_back(pid(i, 0));
	}
	double finisoLaikas;
	while (true) {
		finisoLaikas = kadaFinisuos(begikai[pirmauja.back().first], finisas);
		if (finisoLaikas < pirmauja.back().second)
			pirmauja.pop_back();
		else
			break;
	}
	pirmauja.push_back(pid(-1, finisoLaikas));
	double maxPirmavoLaiko = 0;
	for (int i = 0; i < pirmauja.size() - 1; i++) {
		double pirmavoLaiko = pirmauja[i + 1].second - pirmauja[i].second;
		if (pirmavoLaiko > maxPirmavoLaiko)
			maxPirmavoLaiko = pirmavoLaiko;
	}
	return maxPirmavoLaiko;
}
