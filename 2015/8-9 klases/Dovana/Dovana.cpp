#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
#define INF 2000000000
using namespace std;

    int M, N, i, j, minI, minJ, minS = INF;
    vector<int> potatoes;
    void readFile(string p){
        ifstream FI(p); FI >> M >> N;
        while(FI >> i) potatoes.push_back(i);
        FI.close();
        sort(potatoes.begin(), potatoes.end());
    }

 int main(){
    readFile("dovana.in");
    i = 0; j = N - 1;
    while(j < M){
        if(potatoes[j] - potatoes[i] < minS){
            minS = potatoes[j] - potatoes[i]; minI = i; minJ = j;
        }
        i++; j++;
    }

    ofstream FO("dovana.out");
    for(i = minI; i <= minJ; i++)
        FO << potatoes[i] << " ";
    FO.close();

 return 0; }
