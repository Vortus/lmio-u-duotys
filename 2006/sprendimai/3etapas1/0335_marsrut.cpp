/*

ideja:
    1) kiekvienas adresas susideda i� keturiu daliu;
    2) kiekviena taisykle saugome kaip keturis sveikuju skaiciu intervalus
       ir viena adresa, kuriuo reikia nukreipti ja tenkinancius adresus;
    3) kiekvienam ateinanciam adresui perbegame visas taisykles ir patikriname
       ar jam galima taikyti kiekviena i� taisykliu. rade pirmaja taisykle,
       kuria galima jam pritaikyti, toliau nebeie�kome (salygoje pasakyta, kad
       taisykles nesikerta);
    4) taisykle galima pritaikyti adresui, jei visos keturios adreso dalys
       patenka i atitinkamus taisykles sveikuju skaiciu intervalus;
    5) efektyvumo vardan, taisykles galima butu suru�iuoti, pagal pirmaji
       sveikuju skaiciu intervala. taip nereiketu ie�koti per visas taisykles.
       taciau testai yra pakankamai ma�i ir tai atlikti nera butina.

*/

#include <string>
#include <vector>
#include <fstream>
using namespace std;

const string inFilename = "MARSRUT.IN";
const string outFilename = "MARSRUT.OUT";

struct TAISYKLE
{
	int nuo[4];
	int iki[4];

	int adresas[4];
};

int main()
{
	int n, m;
	vector<TAISYKLE> taisykles;

	ifstream inFile(inFilename.c_str());
	inFile >> n >> m;
	for (int i = 0; i < n; ++i)
	{
		string adresai, adresas;
		inFile >> adresai >> adresas;

		TAISYKLE t;
		
		// nuskaitome adresu intervala
		for (int ii = 0; ii < (int) adresai.size(); ++ii)
		{
			if (adresai[ii] == '.')
				adresai[ii] = ' ';
		}

		char temp[4][8];
		sscanf(adresai.c_str(), "%s %s %s %s",
			&temp[0], &temp[1], &temp[2], &temp[3]);
		for (int ii = 0; ii < 4; ++ii)
		{
			t.nuo[ii] = -1;
			t.iki[ii] = -1;
			sscanf(temp[ii], "%d-%d", &t.nuo[ii], &t.iki[ii]);
			if (t.iki[ii] == -1)
				t.iki[ii] = t.nuo[ii];
		}

		// nuskaitome viena adresa
		sscanf(adresas.c_str(), "%d.%d.%d.%d",
			&t.adresas[0], &t.adresas[1], &t.adresas[2], &t.adresas[3]);

		taisykles.push_back(t);
	}

	// kiekvienam adresui surandame ji atitinkancia taisykle
	ofstream outFile(outFilename.c_str());
	for (int i = 0; i < m; ++i)
	{
		string adresas;
		inFile >> adresas;

		int adr[4] = {-1, -1, -1, -1};
		sscanf(adresas.c_str(), "%d.%d.%d.%d",
			&adr[0], &adr[1], &adr[2], &adr[3]);

		int taisykle = -1;
		for (int ii = 0; ii < (int) taisykles.size(); ++ii)
		{
			bool radom = true;

			for (int j = 0; j < 4; ++j)
			{
				if ((adr[j] < taisykles[ii].nuo[j]) || (adr[j] > taisykles[ii].iki[j]))
				{
					radom = false;
					break;
				}
			}

			if (radom)
			{
				taisykle = ii;
				break;
			}
		}

		outFile << taisykles[taisykle].adresas[0] << "."
				<< taisykles[taisykle].adresas[1] << "."
				<< taisykles[taisykle].adresas[2] << "."
				<< taisykles[taisykle].adresas[3] << endl;
	}
	inFile.close();
	outFile.close();

	return 0;
}
