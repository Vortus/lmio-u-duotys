#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

    #define MAXM 3010
    vector<int> potatoes(MAXM); // buviu svoriai
    int M, N; // bulviu kiekis, vaiku kiekis

    void readFile(string s){
        ifstream fi(s);
        fi >> M >> N; int tmp = 0;
        while(!fi.eof()) fi >> potatoes[tmp++];
        sort(potatoes.begin(), potatoes.begin() + M);
        fi.close();
    }

 int main(){

    readFile("dovana.in");
    int i = 0, j = N - 1, MAXS = INFINITY, bestI, bestJ, tmp;
    while(j < M){
        tmp = potatoes[j] - potatoes[i];
        if(tmp < MAXS){ bestI = i; bestJ = j;  MAXS = tmp; }
        i++; j++;
    }

    ofstream fo("dovana.out");
    for(i = bestI; i <= bestJ; i++)
        fo << potatoes[i] << " ";
    fo.close();

 return 0; }
