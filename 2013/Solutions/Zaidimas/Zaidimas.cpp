#include <iostream>
#include <iomanip>
#include <fstream>
#include <queue>
using namespace std;

    #define MAXL 50010
    #define INFINITY 2000000000

    struct Level{
        int S, B, ID;
        int minL, maxL;// minimum iki lygio maximum iki lygio
        void init(int id, int minl, int maxl){ ID = id; minL = minl; maxL = maxl; }
    };

    int N; // lygiu
    Level levels[MAXL];

    void readFile(string p){
        ifstream fi(p); fi >> N;
        for(int i = 1; i < N; i++){
            fi >> levels[i].S >> levels[i].B;
            levels[i].init(i, INFINITY, -1);
        }
        fi.close();
        levels[N].init(N, INFINITY, -1); // galutinis
    }

    void exploreBFS(Level *l){
        l -> init(1, 1, 1);
        queue<Level*> q; q.push(l);
        int from, to, i, id, where;
        while(!q.empty()){
            l = q.front(); q.pop();
            from = l -> S; to = l -> B; id = l -> ID;
            if(id != N){
                for(i = from; i <= to; i++){ // per visus taskus
                    where = min(i + id, N);
                    if((levels[where].minL) > (l -> minL) + 1){ // jei rado trumpesni kelia
                        levels[where].minL = (l -> minL) + 1;
                        q.push(&levels[where]);
                    }
                    if((levels[where].maxL) < (l -> maxL) + 1){ // jei rado trumpesni kelia
                        levels[where].maxL = (l -> maxL) + 1;
                        q.push(&levels[where]);
                    }
                }
            }
        }
    }

 int main(){

    readFile("zaidimas1.txt");
    exploreBFS(&levels[1]);
    cout << levels[N].minL << " " << levels[N].maxL;

 return 0; }
