#include "mozaika.h"
#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

static const int MAXN = 2001;
int lSuma[MAXN][MAXN];
int bSuma[MAXN][MAXN];

inline int imk(int arr[][MAXN], int x, int y) {
    return x >= 0 && y >= 0 ? arr[x][y] : 0;
}

int restauruok(int N, int M, int K, int L, char mozaika[][MAXN]) {
    if (N < K || M < L) return -1;
    int maz = -1;
    for (int p1 = 0; p1 < N; p1++) {
        for (int p2 = 0; p2 < M; p2++) {
            // Nutrupėjusių langelių suma
            lSuma[p1][p2] = imk(lSuma, p1-1, p2) + imk(lSuma, p1, p2-1) - imk(lSuma, p1-1, p2-1) + (mozaika[p1][p2] == '.');
            // Baltų langelių suma
            bSuma[p1][p2] = imk(bSuma, p1-1, p2) + imk(bSuma, p1, p2-1) - imk(bSuma, p1-1, p2-1) + (mozaika[p1][p2] == 'B');
            // Idėk jurą
            if (p1 >= K - 1 && p2 >= L - 1) {
                int lSumaSt = imk(lSuma, p1, p2) - imk(lSuma, p1-K, p2) - imk(lSuma, p1, p2-L) + imk(lSuma, p1-K, p2-L);
                int bSumaSt = imk(bSuma, p1, p2) - imk(bSuma, p1-K, p2) - imk(bSuma, p1, p2-L) + imk(bSuma, p1-K, p2-L);
                if (bSumaSt == 0 && (maz == -1 || lSumaSt < maz)) maz = lSumaSt; 
            }
        }
    }
   
    return maz;
}
