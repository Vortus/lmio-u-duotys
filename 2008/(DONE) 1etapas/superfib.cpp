#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    long memo[1000];

 int main(){

    memo[0] = 1; memo[1] = 1; memo[2] = 1;

    ifstream fi("super.in");
    long N; fi >> N;
    fi.close();

    for(int i = 3; i < N; i++)
        memo[i] = memo[i - 1] + memo[i - 2] + memo[i - 3];

    ofstream fo("super.out");
    fo << memo[N - 1];
    fo.close();

 return 0; }
