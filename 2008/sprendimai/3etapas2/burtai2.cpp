/*
TASK:BURTAI
LANG:C++
*/
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>

using namespace std;

    const int let = 'Z' - 'A' + 1,
              maxv = 1000 + let + 2;
    int f[maxv][maxv], c[maxv][maxv];
    int source, sink;

inline int letter(char c) 
{
    return 1 + c - 'A';
}

inline int word(int i)
{
    return 1 + let + i;
}

void readData()
{
    string s;
    cin >> s;
    source = 0;
    sink = 1 + s.length() + let;
    for (int i = 0; i < s.length(); i++) {
        c[letter(s[i])][sink]++;
        string v;
        cin >> v;
        c[source][word(i)] = 1;
        for (int j = 0; j < v.length(); j++)
            c[word(i)][letter(v[j])] = 1;
    }
}

void dfs(int u, int vi[], int p[])
{
    vi[u] = 1;
    for (int v = source; !vi[sink] && v <= sink; v++)
        if (c[u][v] - f[u][v] > 0 && !vi[v]) {
            p[v] = u;
            dfs(v, vi, p);
        }
}

bool findPath()
{
    int vi[sink + 1], p[sink + 1];
    for (int i = source; i <= sink; i++) {
        vi[i] = 0;
        p[i] = -1;
    }
    dfs(source, vi, p);
    if (!vi[sink])
        return false;
    for (int v = sink; p[v] != -1; v = p[v]) {
        f[p[v]][v]++;
        f[v][p[v]]--;
    }
    return true;
}

int main()
{
    freopen("burtai.in", "r", stdin);
    freopen("burtai.out", "w", stdout);
    readData();
    int flow = 0;
    while (findPath()) 
        flow++;
    cout << flow << endl;
    return 0;
}
