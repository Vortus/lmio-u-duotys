/*
TASK:KONTROLINIS
LANG:C++
*/
#include <cstdlib>
#include <iostream>
#include <Fstream>
#include <string>
using namespace std;

// Neneigiam� sveik�j� skai�i� klas� su  reikalingais metodais 
class Skaicius{
      public:
                   
      static const int MAXLEN = 0x10000; // U�tenktinai didelis skai�ius masyvo r��iui
      
      char  reiksme[MAXLEN];
      int ilgis;
      int sistema; // sistema <= 10 
      // Inicializatorius:                          
      Skaicius(int n, int sistema){
         this->ilgis=0;
         this->sistema = sistema;
         for (int i=0;i<MAXLEN;i++)  reiksme[ i ] = 0;             
         int nn = n;    
         do { 
           reiksme[ilgis++] = nn % sistema;
           nn /= sistema;
         } while (nn>0);            
      } //       
      void padauginkIsSkaitmens(int skaitmuo){ // skaitmuo < sistema 
         int omenyje=0;  
         for(int i=0;i<ilgis;i++){
            int rez = reiksme[ i ] * skaitmuo + omenyje;
            reiksme[ i ] = rez  % sistema;             
            omenyje = rez / sistema;                      
         }  
         if ( omenyje > 0 )  reiksme[ ilgis++ ] = omenyje;                                 
      }
     
}; //  
 
// Pagalbin�s funkcijos:
              
void spausdinkDesimtaini(ostream& iKur, Skaicius& sk){
     
     for(int i=(sk.ilgis-1); i>=0; i--)
     {
       iKur << char(sk.reiksme[i] + '0');                               
     }              
}
// Pagalbiniai masyvai:
const string skaitmenys81[8] = {"0","1","10","11","100","101","110","111"};
const int nuliuSkaicius81[8] = {0,1,1,2,1,2,2,3};
const string skaitmenys8[8] = {"000","001","010","011","100","101","110","111"};
const int nuliuSkaicius8[8] = {0,1,1,2,1,2,2,3};

int  spausdinkDvejetaini(ostream& iKur, Skaicius& sk){ // Skaicius u�ra�ytas 8-tain�je, 
     int vienetu = 0;
     iKur << skaitmenys81[ sk.reiksme[ sk.ilgis - 1 ] ];
     vienetu += nuliuSkaicius81[ sk.reiksme[ sk.ilgis - 1 ] ];
     for(int i=(sk.ilgis-2); i>=0; i--)
     {
       iKur << skaitmenys8[ sk.reiksme[i] ];                
       vienetu += nuliuSkaicius8[ sk.reiksme[ i ] ];                      
     }              
     return vienetu;
}
//------------------------------------
// Kontrolinis darbas:
int uzduotis1(ostream& iKur, int n){
     //5^N
     Skaicius sk(1,10);
     for(int i=1;i<=n;i++)
       sk.padauginkIsSkaitmens(5);
     spausdinkDesimtaini(iKur,sk);
     iKur << "\n";   
     return sk.ilgis - 1;
}
int uzduotis4(ostream& iKur, int n){
     //5^N
     Skaicius sk(1,8);
     for(int i=1;i<=n;i++)
       sk.padauginkIsSkaitmens(5);
     int vienetu = spausdinkDvejetaini(iKur,sk);
     iKur << "\n";   
    //                     cout <<" Vienetu " << vienetu << "\n";
     return vienetu;
}

void uzduotis5(ostream& iKur, int vienetu){
     
     Skaicius sk(1,10);
     for(int i=1;i <= vienetu ;i++)
       sk.padauginkIsSkaitmens(2);
     // Koreguojame paskutini skaitmeni
     sk.reiksme[0]--;     
     spausdinkDesimtaini(iKur,sk);
     iKur << "\n";
}
//

int main(int argc, char *argv[])
{
    // Nuskaitome pradinius duomenis.
    ifstream fin("KONTROLINIS.IN");
    int n;
    fin >> n; 
    fin.close();
    // Sprendimas:
    ofstream fout("KONTROLINIS.OUT");
                   
    int u2 = uzduotis1(fout, n);    
    
    fout << u2 << "\n";
    // Tre�ia u�duotis yra tas pats kaip ir pirma su n/2:
    uzduotis1(fout,n/2);
    // Ketvirta ir penkta u�duotys  susietos:
    uzduotis5 ( fout, uzduotis4(fout,n));
    
    fout.close();
    return EXIT_SUCCESS;
}
