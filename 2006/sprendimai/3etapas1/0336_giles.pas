program Giles;

    const MAXP = 15;
          MAXT = 10000;
          
    var gile : array [1..MAXT, 1..MAXP] of boolean;
        P, N, T : longint;
        
procedure pradiniai_duomenys;
var f : text; 
    tk, pk, k : longint;
begin
    assign(f, 'GILES.IN'); 
    reset(f);
    readln(f, P);
    readln(f, N);
    for k := 1 to N do
    begin
        readln(f, tk, pk);
        
        gile[tk, pk] := true;
        
        if tk > T 
            then T := tk;
    end;
    close(f);
end;
    
procedure rezultatai(ats : longint);
var f : text;
begin
    assign(f, 'GILES.OUT'); 
    rewrite(f);
    writeln(f, ats);
    close(f);
end;

function max(a, b, c : longint) : longint;
begin
    if (a > b) and (a > c) then max := a
    else if b > c then max := b
    else max := c
end;

    const MINUS_BEGALYBE = -1000000;

    var maksgiliu : array [0..MAXT, 0..MAXP + 1] of longint;
        laikas, pozicija, atsakymas : longint;
        
begin
    pradiniai_duomenys;
    
    { paruo�iama lentel� }
    for laikas := 1 to T do begin
        maksgiliu[laikas, 0] := MINUS_BEGALYBE;
        maksgiliu[laikas, P + 1] := MINUS_BEGALYBE;
    end;
    for pozicija := 0 to P + 1 do
        maksgiliu[0, pozicija] := MINUS_BEGALYBE;
    maksgiliu[0, (P + 1) div 2] := 0;
    
    for laikas := 1 to T do
        for pozicija := 1 to P do
        begin
            maksgiliu[laikas, pozicija] := max(
                maksgiliu[laikas - 1, pozicija - 1],
                maksgiliu[laikas - 1, pozicija],
                maksgiliu[laikas - 1, pozicija + 1]);
            if gile[laikas, pozicija]
                then inc(maksgiliu[laikas, pozicija]);
        end;

    { atsakymas - did�iausias vir�utin�s lentel�s eilut�s skai�ius }   
    atsakymas := 0;
    for pozicija := 1 to P do
        if maksgiliu[T, pozicija] > atsakymas
            then atsakymas := maksgiliu[T, pozicija];

    rezultatai(atsakymas);    
end.
