#include <iostream>
#include <iomanip>
#include <fstream>
#include <queue>
using namespace std;

    /*
    Vietoj pairu aisku galejo buti naudojamas struktas
    bet kadangi dirbant kaip ir su dvimaicu masyvu ir zemelapiu
    atrodo geriau tinkam pairas.
    */

    ////Kad patogiau pairus butu
    #define y first
    #define x second
    typedef pair<int, int> Point;// y, x
    const Point Dir[] = { Point(0, -1), Point(1, 0), Point(0, 1), Point(-1, 0) };
    const int MAXW = 3010, MAXH = 3010, MAXD = 4;
    char map[3010][3010];// mapas
    int pBuffer[MAXH][MAXW]; // lino
    int cBuffer[MAXH][MAXW];//kates
    int width, height, D, T;
    Point startPoint; // saldytuvo pozicija

    //inline pagreitina funkcijos veikima jei ji daug kartu iskvieciama
    inline bool isValid(Point p){
        return p.x >= 0 && p.y >= 0 &&
         p.x < width && p.y < height &&
         map[p.y][p.x] != '#';
    }

    Point exploreCat(Point start, int buffer[MAXH][MAXW]){
        buffer[start.y][start.x] = 1;
        int i = 0;
        do{
            Point p = Point(start.y + Dir[i].y, start.x + Dir[i].x); //bando judet i p taska
            while(!isValid(p)){
                i++; i %= MAXD;
                p = Point(start.y + Dir[i].y, start.x + Dir[i].x);
            }
            buffer[p.y][p.x] = buffer[start.y][start.x] + 1; // nusinesa reiksme ir prideda vienu judesiu daugiau
            start = p;
        }while(map[start.y][start.x] != 'P');
        return start;
    }

    //isiesko visa zemelapi su ju atstumais nuo pradzios tasko linui
    void bfsExploreP(Point start, int buffer[MAXH][MAXW]){
        queue<Point> q; // eile
        q.push(start);
        buffer[start.y][start.x] = 1;
        while(!q.empty()){
            start = q.front();
            q.pop();
            for(int i = 0; i < MAXD; i++){ // persuka per visus ejimus ir sudeda i queue kurie teisingi
                Point p = Point(start.y + Dir[i].y, start.x + Dir[i].x);
                if(isValid(p) && buffer[p.y][p.x] == 0) {// jei tinka ir nera praeksplorintas
                    q.push(p);
                    buffer[p.y][p.x] = buffer[start.y][start.x] + 1; // keliaujiam toliau
                }
            }
        }

    }

    void readFile(){
        ifstream fi("katinas.in");
        fi >> D >> T >> height >> width;
        fi.ignore(80, '\n');
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                char c; fi.get(c);
                if(c == 'S') { startPoint = Point(i, j); }
                map[i][j] = c;
            }
            fi.ignore();
        }
        fi.close();
    }


 int main(){

    readFile();
    bfsExploreP(startPoint, pBuffer); // praziuri visa mapa ir suraso atstumus
    Point ce = exploreCat(startPoint, cBuffer); // kates baigiamoji pozicija

    ofstream fo("katinas.out");
    fo << max(0, D - max(0, T + pBuffer[ce.y][ce.x] - cBuffer[ce.y][ce.x]));
    fo.close();

 return 0; }
