#include "vedybos.h"
#include <algorithm>
using namespace std;

//Realizuokite �i� funkcij�.
int maziausiasKoeficientas(int N, int V[]) {
    for (int i = 1; i < N; i++)
        V[i] = max(V[i-1]-1, V[i]);
    for (int i = N-2; i >= 0; i--)
        V[i] = max(V[i+1]-1, V[i]);
    int ats = 2000000000;
    for (int i = 0; i < N; i++)
        ats = min (ats, V[i]);
    return ats;
}
