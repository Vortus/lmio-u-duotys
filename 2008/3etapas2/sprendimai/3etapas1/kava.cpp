/* Lino sprendimas uždaviniui "Kava". (Komentarai utf-8)
 * Sprendimas dinaminiu programavimu.
 */

#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

#define MAXN 100
#define MAXB 10000

    struct misinys {
        int  b;         // surinktas skaičius "b". Tai kava <b in x>.
        bool pan[MAXN]; // ar panaudotas i-asis pakelis?
        misinys() {     // mišinio inicializavimas
            b = 0;
            for (int i = 0; i < MAXN; i++)
                pan[i] = false;
        }
    };

    struct misiniai {
    private:
        bool sud[MAXB + 1]; // ar sudarytas mišinys b? Naudojamas dėl efektyvumo.
    public:
        vector<misinys> m;  // patys mišiniai
        misiniai() {        // mišinių inicializavimas
            for (int i = 0; i <= MAXB; i++)
                sud[i] = false;
        }
        bool sudarytas(int b) { // ar sudarytas mišinys "b"?
            if (b < 0 || b > MAXB)
                return true;    // apsimetam, kad sudarytas, idant nepridėtume tokių
            return sud[b];
        }
        void pridek(misinys& x) { 
            m.push_back(x);
            sud[x.b] = true;
        }
    };

int main()
{
    freopen("kava.in", "r", stdin);
    freopen("kava.out", "w", stdout);

    int n, b, c;
    int a[MAXN];

    cin >> b >> c >> n;
    for (int i = 0; i < n; i++) 
        cin >> a[i];

    misiniai turimi;            // turimi mišiniai
    misinys  tuscias;
    turimi.pridek(tuscias);     // pridedamas tuščias
    for (int k = 1; k <= c; k++) {
        /* Ieškosime, kokius mišinius galime sudaryti panaudodami
         * lygiai k pakelių. Remsimės jau sudarytais mišiniais iš
         * k - 1 pakelių.
         */
        misiniai nauji;

        // nagrinėjam visus turimus mišinius
        for (int i = 0; i < turimi.m.size(); i++)
            // ir visus kavos pakelius
            for (int j = 0; j < n; j++)
                if (!turimi.m[i].pan[j] && !nauji.sudarytas(turimi.m[i].b + a[j]))
                {
                    misinys x = turimi.m[i];
                    x.b += a[j];
                    x.pan[j] = true;
                    nauji.pridek(x);
                }

        turimi = nauji;
    }

    // ar sudarėme mišinį, kurio reikėjo?
    for (int i = 0; i < turimi.m.size(); i++)
        if (turimi.m[i].b == b) {
            // jei sudarėme, tai išspausdiname panaudotų pakelių numerius
            for (int j = 0; j < n; j++)
                if (turimi.m[i].pan[j])
                    cout << (j + 1) << endl;
            return 0; // galime nutraukti darbą
        }

    // jei mišinio <b in c> nepavyko sudaryti, spausdinamas nulis
    cout << 0 << endl;

    return 0;
}
