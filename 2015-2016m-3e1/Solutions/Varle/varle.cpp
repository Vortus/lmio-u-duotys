#include <iostream>
#include <iomanip>
#include <fstream>
#include <deque>
using namespace std;

    #define MAXN 1000010
    #define FILE "varle.txt"
    #define FILEOUT "varleOUT.txt"
    int M[MAXN]; // muses
    long long MINL[MAXN]; // min lelijos
    int N, L; // leliju kiekis, suolio ilgis

    void readFile(){
        ifstream FI(FILE); FI >> N >> L;
        for(int i = 1; i <= N; i++) FI >> M[i];
        FI.close();
        M[0] = 0;  M[N] = 0; MINL[0] = 0;
    }

 int main(){

    readFile();

    deque<int> deq; deq.push_back(0);
    for(int i = 1; i <= N; i++){
        MINL[i] = M[i] + MINL[deq.front()];
        while(!deq.empty() && MINL[deq.back()] >= MINL[i])
            deq.pop_back();
        deq.push_back(i);
        if(deq.front() == i - L)
            deq.pop_front();
    }

    cout << MINL[N];

 return 0; }
