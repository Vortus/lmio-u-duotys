#include <cstdio>

int main() {
    freopen("beglys-jau.in", "r", stdin);
    freopen("beglys-jau.out", "w", stdout);
    
    int n;
    scanf("%d", &n);
    
    int i = 1;
    char c, turn = 'R';
    scanf(" %c", &c);    

    while(i < n) {
        scanf("%c", &c);
        if(c == turn) break; // Grupuot� c valdo du miestus paeiliui
        turn = c;
        ++i;
    }
    
    printf("%c\n", turn);
    return 0;
}
