/*
TASK: AKADEMIJA
LANG: C++
*/

#include <iostream>
using namespace std;

const int BEGALYBE = 1000000000;

/*
1 �ingsnis. Kiekvienoje lentel�s eilut�je surandame minimali� reik�m� ir
j� atimame i� kiekvieno tos eilut�s elemento. Taip kiekvienoje lentel�s
eilut�je gauname bent po vien� nul�.
*/
void SukuriameNulius(int n, int **lentele)
{
	/*
	eilut�s
	*/
	for (int i = 0; i < n; ++i)
	{
		int minReiksme = lentele[i][0];
		for (int ii = 1; ii < n; ++ii)
		{
			if (lentele[i][ii] < minReiksme)
				minReiksme = lentele[i][ii];
		}

		for (int ii = 0; ii < n; ++ii)
			lentele[i][ii] -= minReiksme;
	}

	/*
	stulpeliai
	*/
	for (int ii = 0; ii < n; ++ii)
	{
		int minReiksme = lentele[0][ii];
		for (int i = 1; i < n; ++i)
		{
			if (lentele[i][ii] < minReiksme)
				minReiksme = lentele[i][ii];
		}

		for (int i = 0; i < n; ++i)
			lentele[i][ii] -= minReiksme;
	}
}

/*
2 �ingsnis. Lentel�je pa�ymime tuos nulius, kurie yra vieninteliai savo
eilut�je arba stulpelyje. Pa�ym�dami nul�, kuris yra vienintelis savo
stulpelyje, u�dengiame lentel�s eilut�, kuriai jis priklauso. O pa�ym�dami
nul�, kuris yra vienintelis savo eilut�je, u�dengiame lentel�s stulpel�,
kuriam jis priklauso. Toliau tikrindami, ar nuliai yra vieninteliai savo
eilut�je ar stulpelyje, u�dengt� nuli� (esan�i� u�dengtoje eilut�je ar
stulpelyje) neskai�iuojame. Pa�ym�dami nul� atliekame atitinkamos u�duoties
priskyrim� darbininkui. M�s� tikslas � atlikti lygiai N priskyrim�.
*/
int ZymimeNulius(int n, int **lentele, int *eil, int *stulp,
				 int *eil_nuliu, int *stulp_nuliu)
{
	int priskyrimu = 0;

	memset(eil_nuliu, 0, sizeof(int) * n);
	memset(stulp_nuliu, 0, sizeof(int) * n);
	
	/*
	suskai�iuojame, kiek nuli� yra kiekvienoje eilut�je ir stulpelyje
	*/
	for (int i = 0; i < n; ++i)
	{
		for (int ii = 0; ii < n; ++ii)
		{
			if (lentele[i][ii] == 0)
			{
				++eil_nuliu[i];
				++stulp_nuliu[ii];
			}
		}
	}

	bool pakeista = true;
	while (pakeista)
	{
		pakeista = false;
		/*
		kiekvienai eilutei, kurioje yra tik vienas nulis..
		*/
		for (int j = 0; j < n; ++j)
		{
			if ((eil[j] == 0) && (eil_nuliu[j] == 1))
			{
				bool radom = false;
				for (int ii = 0; (ii < n) && !radom; ++ii)
				{
					if (lentele[j][ii] == 0)
					{
						for (int i = 0; i < n; ++i)
						{
							if (lentele[i][ii] == 0)
							{
								--eil_nuliu[i];
								--stulp_nuliu[ii];
								lentele[i][ii] = -1;
							}
						}
						++priskyrimu;
						stulp[ii] = 1;
						lentele[j][ii] = -2;
						radom = true;
					}
				}
				pakeista = true;
			}
		}
		/*
		kiekviename stulpelyje, kuriame yra tik vienas nulis..
		*/
		for (int j = 0; j < n; ++j)
		{
			if ((stulp[j] == 0) && (stulp_nuliu[j] == 1))
			{
				bool radom = false;
				for (int i = 0; (i < n) && !radom; ++i)
				{
					if (lentele[i][j] == 0)
					{
						for (int ii = 0; ii < n; ++ii)
						{
							if (lentele[i][ii] == 0)
							{
								--eil_nuliu[i];
								--stulp_nuliu[ii];
								lentele[i][ii] = -1;
							}
						}
						++priskyrimu;
						eil[i] = 1;
						lentele[i][j] = -2;
						radom = true;
					}
				}
				pakeista = true;
			}
		}
	}

	return priskyrimu;
}

/*
4 �ingsnis. Pa�ymime bet kur� neu�dengt� nul�. Kartu u�dengiame jo
eilut� ir stulpel�.
*/
bool YraNeuzdengtuNuliu(int n, int **lentele, int *eil, int *stulp,
						int *eil_nuliu, int *stulp_nuliu)
{
	bool radom = false;

	for (int i = 0; (i < n) && !radom; ++i)
	{
		if (eil_nuliu[i] > 0)
		{
			for (int ii = 0; (ii < n) && !radom; ++ii)
			{
				if (lentele[i][ii] == 0)
				{
					for (int j = 0; j < n; ++j)
					{
						if (lentele[i][j] == 0)
						{
							--eil_nuliu[i];
							--stulp_nuliu[j];
							lentele[i][j] = -1;
						}
					}
					for (int j = 0; j < n; ++j)
					{
						if (lentele[j][ii] == 0)
						{
							--eil_nuliu[j];
							--stulp_nuliu[ii];
							lentele[j][ii] = -1;
						}
					}
					eil[i] = 1;
					stulp[ii] = 1;
					lentele[i][ii] = -2;
					radom = true;
				}
			}
		}
	}

	return radom;
}

/*
5.1 �ingsnis. Ie�kome minimalaus skai�iaus linij�, eilu�i� arba stulpeli�,
kuriomis galima u�dengti visus nulius lentel�je, tokiu b�du:
1) pa�ymime eilutes, kuriose n�ra nei vieno pa�ym�to nulio;
2) pa�ymime stulpelius, kurie turi nuli� pa�ym�tose eilut�se;
3) pa�ymime eilutes, kuriose yra pa�ym�t� nuli� i� pa�ym�t� stulpeli�.
Paskutinius du �ingsnius kartojame tol, kol atliekame nors vien� pakeitim�
�ym�jimuose. Tada u�dengiame pa�ym�tuosius lentel�s stulpelius ir
nepa�ym�t�sias lentel�s eilutes.
*/
void UzdengiameNulius(int n, int **lentele, int *eil, int *stulp)
{
	memset(eil, 0, sizeof(int) * n);
	memset(stulp, 0, sizeof(int) * n);

	// 1) �ingsnelis
	for (int i = 0; i < n; ++i)
	{
		bool radom = false;
		for (int ii = 0; (ii < n) && !radom; ++ii)
		{
			if (lentele[i][ii] == -2)
				radom = true;
		}

		if (!radom)
			eil[i] = 1;
	}

	bool pakeista = true;
	while (pakeista)
	{
		pakeista = false;
		// 2) �ingsnelis
		for (int i = 0; i < n; ++i)
		{
			if (eil[i] == 1)
			{
				for (int ii = 0; ii < n; ++ii)
				{
					if ((stulp[ii] == 0) && (lentele[i][ii] <= 0))
					{
						stulp[ii] = 1;
						pakeista = true;
					}
				}
			}
		}
		// 3) �ingsnelis
		for (int ii = 0; ii < n; ++ii)
		{
			if (stulp[ii] == 1)
			{
				for (int i = 0; i < n; ++i)
				{
					if ((eil[i] == 0) && (lentele[i][ii] == -2))
					{
						eil[i] = 1;
						pakeista = true;
					}
				}
			}
		}
	}

	for (int i = 0; i < n; ++i)
		eil[i] = 1 - eil[i];
}

/*
5.2 �ingsnis. Surandame minimali� neu�dengt� reik�m� lentel�je ir j� atimame
i� vis� neu�dengt� lentel�s reik�mi� bei pridedame prie vis� lentel�s
reik�mi�, ties kuriomis kertasi eilut�s ir stulpelius dengian�ios linijos.
Atidengiame visas lentel�s eilutes ir stulpelius.
*/
void SukuriameNaujusNulius(int n, int **lentele, int *eil, int *stulp)
{
	int minReiksme = BEGALYBE;
	/*
	surandame ma�iausi� neu�dengt� reik�m�
	*/
	for (int i = 0; i < n; ++i)
	{
		if (eil[i] == 1)
			continue;

		for (int ii = 0; ii < n; ++ii)
		{
			if ((stulp[ii] == 0) && (lentele[i][ii] < minReiksme))
				minReiksme = lentele[i][ii];
		}
	}

	/*
	atimame j� i� kiekvienos neu�dengtos reik�m�s ir pridedame prie kiekvienos
	dukart u�dengtos reik�m�s
	*/
	for (int i = 0; i < n; ++i)
	{
		if (eil[i] == 0)
		{
			for (int ii = 0; ii < n; ++ii)
			{
				if (stulp[ii] == 0)
					lentele[i][ii] -= minReiksme;
			}
		}
		else
		{
			for (int ii = 0; ii < n; ++ii)
			{
				if (stulp[ii] == 1)
				{
					lentele[i][ii] += minReiksme;
				}
			}
		}
	}
}

void Vengriskas(int n, int **lentele)
{
	int *eil = new int[n];
	int *stulp = new int[n];
	int *eil_nuliu = new int[n];
	int *stulp_nuliu = new int[n];
	bool baigta = false;
	bool buvoNeuzdengtuNuliu = false;
	int priskyrimu = 0;
	
	SukuriameNulius(n, lentele);
	memset(eil, 0, sizeof(int) * n);
	memset(stulp, 0, sizeof(int) * n);
	while (!baigta)
	{
		priskyrimu += ZymimeNulius(n, lentele, eil, stulp, eil_nuliu, stulp_nuliu);
		if (priskyrimu < n)
		{
			if (YraNeuzdengtuNuliu(n, lentele, eil, stulp, eil_nuliu, stulp_nuliu))
			{
				++priskyrimu;
				buvoNeuzdengtuNuliu = true;
			}
			else
			{
				if (buvoNeuzdengtuNuliu)
				{
					UzdengiameNulius(n, lentele, eil, stulp);
					buvoNeuzdengtuNuliu = false;
				}

				for (int i = 0; i < n; ++i)
				{
					for (int ii = 0; ii < n; ++ii)
					{
						if (lentele[i][ii] < 0)
							lentele[i][ii] = 0;
					}
				}
				SukuriameNaujusNulius(n, lentele, eil, stulp);
				memset(eil, 0, sizeof(int) * n);
				memset(stulp, 0, sizeof(int) * n);
				priskyrimu = 0;
			}
		}
		else
		{
			baigta = true;
		}
	}

	delete[] eil;
	delete[] stulp;
	delete[] eil_nuliu;
	delete[] stulp_nuliu;
}

int main()
{
	freopen("AKADEMIJA.IN", "r", stdin);
	freopen("AKADEMIJA.OUT", "w", stdout);

	int n, p;
	scanf("%d %d", &n, &p);
	int maxReiksme = 0;
	
	int **spejimai = new int*[n];
	for (int i = 0; i < n; ++i)
	{
		spejimai[i] = new int[n];
		memset(spejimai[i], 0, sizeof(int) * n);
	}
	for (int j = 0; j < p; ++j)
	{
		int i, ii;

		scanf("%d %d", &i, &ii);
		++spejimai[i - 1][ii - 1];
		if (spejimai[i - 1][ii - 1] > maxReiksme)
			maxReiksme = spejimai[i - 1][ii - 1];
	}

	/*
	surandame lentel�je maksimali� reik�m� ir i� jos atimame kiekvien�
	lentel�s reik�m�
	*/
	for (int i = 0; i < n; ++i)
		for (int ii = 0; ii < n; ++ii)
			spejimai[i][ii] = maxReiksme - spejimai[i][ii];

	Vengriskas(n, spejimai);

	for (int i = 0; i < n; ++i)
	{
		for (int ii = 0; ii < n; ++ii)
			if (spejimai[i][ii] == -2)
				cout << ii + 1 << endl;
	}
	for (int i = 0; i < n; ++i)
		delete[] spejimai[i];
	delete[] spejimai;

	return 0;
}
