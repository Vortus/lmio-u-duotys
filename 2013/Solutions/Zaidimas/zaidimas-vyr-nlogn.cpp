#include <climits>
#include <cstdio>
#include <vector>

using namespace std;

template<class AggFunc>
struct Tree {
    AggFunc func;
    int identity;
    vector<vector<int> > levels;
    Tree(AggFunc func, int identity): func(func), identity(identity) {}
    
    void allocate(int size) {
        for (int lsize = 1; ; lsize *= 2) {
            levels.push_back(vector<int>(lsize, 0));
            if (lsize >= size)
                break;
        }
    }
    
    void set(int pos, int value) {
        levels.back()[pos] = value;
        for (int i = levels.size() - 2; i >= 0; i--) {
            pos /= 2;
            levels[i][pos] = func(levels[i+1][2*pos],
                                  levels[i+1][2*pos+1]);
        }
    }
    
    int get(int low, int high) {
        return get(low, high, 0, 0);
    }
    
    int get(int low, int high, int i, int level) {
        int n_levels = levels.size(),
            holds_from = (1 << (n_levels - level - 1)) * i,
            holds_to = (1 << (n_levels - level - 1)) * (i + 1) - 1;
        if (low > holds_to || high < holds_from)
            return identity;
        if (low <= holds_from && holds_to <= high)
            return levels[level][i];
        return func(get(low, high, 2*i, level+1),
                    get(low, high, 2*i+1, level+1));
    }
};

int N;
vector<int> S, B;

struct MinFunctor { int operator()(int a, int b) { return min(a, b); } };
struct MaxFunctor { int operator()(int a, int b) { return max(a, b); } };

Tree<MinFunctor> Min(MinFunctor(), INT_MAX);
Tree<MaxFunctor> Max(MaxFunctor(), INT_MIN);

int main() {
    freopen("zaidimas-vyr.in", "r", stdin);
    freopen("zaidimas-vyr.out", "w", stdout);
    scanf("%d", &N);
    N--;
    S.resize(N); B.resize(N);
    for (int i = 0; i < N; i++)
        scanf("%d%d", &S[i], &B[i]);
    Min.allocate(N + 1);
    Max.allocate(N + 1);
    Min.set(N, 1);
    Max.set(N, 1);
    for (int i = N-1; i >= 0; i--) {
        int low = min(i + S[i], N),
            high = min(i + B[i], N);
        Min.set(i, Min.get(low, high) + 1);
        Max.set(i, Max.get(low, high) + 1);
    }
    printf("%d %d\n", Min.get(0, 0), Max.get(0, 0));
    return 0;
}
