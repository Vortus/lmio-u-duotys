#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    string number;

    void readFile(){
        ifstream fi("skaicius.txt");
        fi >> number;
        fi.close();
    }

 int main(){

    readFile();
    int i = 0, j = number.length() - 1;
    while(i < j){
        swap(number[i], number[j]);
        i++; j--;
    }
    size_t start = number.find_first_of("123456789");
    number = number.substr(start);
    ofstream fo("skaicius.txt");
    fo << number;
    fo.close();

 return 0; }
