program KiberJuster;

uses
  math;

const
  MaxN = 500;

type
   TAK = record // Adres� knygel�s �ra�as
     pasp: word; // Kiek reikia klavi�� paspaudim� norint pasiekti �ra��
     vardas: string;
   end;

var
  n: word; // Adres� knygel�s �ra�� skai�ius
  ak: array [1 .. MaxN] of TAK;

function rinkti(c: char): word;
begin
  if c in ['A', 'D', 'G', 'J', 'M', 'P', 'T', 'W'] then
    rinkti := 1
  else
    if c in ['B', 'E', 'H', 'K', 'N', 'Q', 'U', 'X'] then
      rinkti := 2
    else
      if c in ['C', 'F', 'I', 'L', 'O', 'R', 'V', 'Y'] then
        rinkti := 3
      else
        if c in ['S', 'Z'] then
          rinkti := 4
        else begin
          writeln ('Nerastas simbolis');
          halt;
        end;

end;


{ Paskai�iuoja, koki� dal� vardo S2 reikia surinkti ir, kiek tai
u�trunka, jei ankstesni u�ra�� knygel�s �ra�as yra S1.
}
function surinkti(s1, s2: string): word;
var
  res, i: word;
begin
  res := 0;
  for i := 1 to length(s2) do
  begin
    res := res + rinkti(s2[i]);
    if (length(s1) < i) or (s1[i] <> s2[i]) then
      break;
  end;
  surinkti := res;
end;

procedure Spresk;
var
  inf: text;
  i, j: integer;
  nn, ats: integer;
begin
  assign(inf, 'JUSTAS.IN'); reset(inf);
  readln(inf, n);
  for i := 1 to n do
  begin
    readln(inf, ak[i].vardas);
    if i = 1 then
      ak[i].pasp := 0 // i�kart aktyvuojamas �ra�as
    else
      ak[i].pasp := surinkti(ak[i-1].vardas, ak[i].vardas);
  end;
  close(inf);

  {Ie�kosim, gal yra �ra��, kuriuos geriau pasiekti naudojantis ir valdymo
  klavi�ais}
  ats := 1;
  for i := 2 to N do
  begin
    for j := 1 to N do
      if i <> j then
      begin
        nn := min(ak[i].pasp, ak[j].pasp + abs(i-j));
        if j < i then
          nn := min(nn, ak[j].pasp + j + (n-i))
        else
          nn := min(nn, ak[j].pasp + i + (n-j));

        ak[i].pasp := nn;
      end;
    if ak[i].pasp > ak[ats].pasp then
      ats := i;
  end;

  assign(inf, 'JUSTAS.OUT'); rewrite(inf);
  writeln(inf, ak[ats].vardas);
  writeln(inf, ak[ats].pasp);
  close(inf);
end;


begin
  Spresk;
end.
