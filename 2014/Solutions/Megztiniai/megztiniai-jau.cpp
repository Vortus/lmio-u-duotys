/**
 * Sprendimo aprašymas:
 *
 * Spręsdami šį uždavinį nagrinėsime kiekvienos spalvos megztinius atskirai.
 *
 * Norėdami sužinoti kiek kartu skalbyklė, kuri talpina M megztinių, bus
 * pripildyta pilnai galima pasitelkti sveikosios dalybos operatorių "/"
 * (arba Pascal kalbos operatoriumi "div") , t.y. jei norime išskalbti m
 * megztinių, tai skalbyklė bus pripildyta pilnai m / M kartų. Jei po šių,
 * pilnų skalbimų, dar lieka megztinių (dalybos liekana nėra lygi nuliui),
 * tenka atlikti dar vieną skalbimą. Pasižiūrėti ar atlikta dalyba baigsis
 * su liekana galima pasinaudoti C kalbos operatoriumi "%" (atitinka Pascal
 * kalbos operatorių "mod")
 *
 * Autorius: Vytautas Gruslys
 */

#include      <iostream>
#include      <cassert>
#include      <cstdio>
#define endl '\n'

using namespace std;

int main(int argc, char **argv) {
    // Atkomentuokite šias eilutes, jei norite skaityti pradinius duomenis iš
	// failo.
	// freopen("megztiniai-jau.in", "r", stdin);
	// freopen("megztiniai-jau.out", "w", stdout);

	int N;       // Spalvų skaičius
	int M;       // Skaičius nurodantis kiek megztinių telpą į skalbyklę
	int ats = 0; // Uždavinio atsakymas

	// Nuskaitome pradinius duomenis
	cin >> N >> M;

	for (int i = 0; i < N; i += 1) {
		// nuskaitome kiek yra i'tosios spalvos megztinių
		int m;
		cin >> m;

		// apskaičiuojame kiek kartų skalbyklė bus pripildyta pilnai
		ats += m / M;

		// jei po pilnų skalbimų dar lieka megztinių, teks atlikti dar vieną
		// skalbimą
		if (m % M != 0) {
			ats += 1;
		}
	}

	// atspausdiname atsakymą
	cout << ats << endl;

	return 0;
}
