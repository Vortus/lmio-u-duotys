#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int buffer[256];

    void refresh(){
        for(int i = 0; i < 256; i++)
            buffer[i] = 0;
    }

 int main(){

    ifstream fi("puslapiavimasjau.in");
    int N, pageSize, maxs = -1; fi >> N >> pageSize;
    fi.ignore(80, '\n');
    while(!fi.eof()){
        refresh();
        for(int i = 0; i < pageSize; i++){
            if(fi.eof()) break;
            char c; fi.get(c); fi.ignore(80, '\n');
            buffer[c]++;
        }
        int c = 0;
        for(int i = 1; i < 256; i++)
            if(buffer[i] != 0) c++;
        if(c > maxs) maxs = c;
    }

    fi.close();

    ofstream fo("puslapiavimasjau.out");
    fo << maxs;
    fo.close();

 return 0; }
