/*
TASK: BANDOMASIS
LANG: C
*/
#include <stdio.h>

int main() {
	int s, k, l = -1;
	
	freopen("bandomasis.in", "r", stdin);
	freopen("bandomasis.out", "w", stdout);
	scanf("%d %d", &k, &s);
	if ((s - k) % 19 == 0)
		l = (s - k) / 19;
	if (l < 0 || l > k)
		l = -1;
	printf("%d\n", l);
	return 0;
}
